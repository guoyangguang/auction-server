--don't change directly data without validation--
--psql -U postgres  数据库名 < dum.sql--
--pg_dump -h localhost -U pgsqldata -f /Users/gyg/app/auction_db_dump/dump3.sql auction_dev--


create table users (
--serial is same as auto_increment in mysql
    id serial primary key,
    email varchar(50),
    phone varchar(25),
    password_digest varchar(80),
--signin
    remember_created_at timestamp,
    sign_in_count integer default 0, 
    current_sign_in_at timestamp,
    last_sign_in_at timestamp,
    current_sign_in_ip varchar(20),
    last_sign_in_ip varchar(20),
    failed_attempts integer,
    unlock_token varchar(60),
    locked_at timestamp,
--invitation
    pin integer,
    invitation_token varchar(60),
    invitation_sent_at timestamp,
    invitation_accepted_at timestamp,
    invitation_limit integer,
    invited_by_id integer,
    invited_by_type varchar(10),
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table verifications (
    id serial primary key,
    phone varchar(25),
    verification_code integer,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table profiles (
    id serial primary key,
    user_id integer not null references users(id) unique,
    name varchar(50),
    gender integer not null default 1,
    location varchar(50),
    job integer not null default 1,
    img varchar(50),
    img_meta jsonb,
    about text,
    uploaded_at timestamp,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table accounts (
    id serial primary key,
    profile_id integer not null references profiles(id) unique,
    balance integer,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table account_orders (
    id serial primary key,
    account_id integer not null references accounts(id),
    amount integer,
    status integer not null default 1,
    pay_method integer,
    paid_at timestamp,
    refunded_at timestamp,
    transaction_id varchar(50) unique,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table roles (
    id serial primary key,
    name varchar(20),
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table assignments (
    profile_id integer not null references profiles(id),
    role_id integer not null references roles(id),
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table messages (
    id serial primary key,
    profile_id integer not null references profiles(id),
    body text,
    recipient_id integer not null references profiles(id),
    read boolean default 'f',
    read_at timestamp,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table addresses (
    id serial primary key,
    profile_id integer not null references profiles(id),
    recipient varchar(50),
    phone varchar(25),
    state integer,
    city integer,
    detail varchar(50),
    postcode varchar(15),
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table artists (
    id serial primary key,
    name varchar(50) not null,
    intro text not null,
    img varchar(50) not null,
    img_meta jsonb,
    uploaded_at timestamp,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table attorneys (
    id serial primary key,
    artist_id integer not null references artists(id),
    img varchar(50),
    img_meta jsonb,
    uploaded_at timestamp,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table items (
    id serial primary key,
    artist_id integer not null references artists(id),
    kind integer not null, 
    name varchar(50) not null,
    description text,
    size integer Array not null,
    usage varchar(200),
    produced_at timestamp not null,
    starting_price integer not null default 0,
    raising_price integer not null,
    deposit integer not null default 0,
    starting_bid timestamp not null,
    closing_bid timestamp not null,
    img varchar(50) not null,
    img_meta jsonb,
    uploaded_at timestamp,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);
create index item_name_index on items (name);

create table bids(
    id serial primary key,
    item_id integer not null references items(id),
    profile_id integer not null references profiles(id),
    price integer not null,
    status integer not null default 1,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table item_imgs (
    id serial primary key,
    item_id integer not null references items(id),
    description text,
    img varchar(50),
    img_meta jsonb,
    uploaded_at timestamp,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table comments (
    id serial primary key,
    profile_id integer not null references profiles(id),
    item_id integer not null references items(id),
    comment_id integer references comments(id),
    body varchar(255) not null,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table followship (
    following_id integer not null references profiles(id),
    followed_id integer not null references artists(id),
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table likes (
    profile_id integer not null references profiles(id),
    item_id integer not null references items(id),
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table orders (
    id serial primary key,
    profile_id integer not null references profiles(id),
    item_id integer not null references items(id),
    amount integer,
    status integer not null default 1,
    pay_method integer,
    paid_at timestamp,
    refunded_at timestamp,
    transaction_id varchar(50) unique,
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

-- item_id is unique in one particular order_id
--create table order_items (
--    id serial primary key,
--    order_id integer not null references orders(id),
--    item_id integer not null references items(id),
--    price integer,
--    quantity integer,
--    total integer,
--    created_at timestamp default current_timestamp,
--    updated_at timestamp,
--    deleted_at timestamp
--);

create table tags (
    id serial primary key,
    name varchar(20),
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table access_tokens (
    id serial primary key,
    profile_id integer not null references profiles(id),
    site integer,
    access_token varchar(60),
    expires_in integer,
    user_id varchar(25),
    created_at timestamp default current_timestamp,
    updated_at timestamp,
    deleted_at timestamp
);

create table sessions (
    session_id varchar(128) unique not null,
    atime timestamp not null default current_timestamp,
    data text
);
