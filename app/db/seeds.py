# -*- coding: utf-8 -*-

'''
add data
'''

from datetime import datetime, timedelta
from web import Storage
from faker import Faker

from ..model.user import User
from ..model.profile import Profile
from ..model.account import Account 
from ..model.role import Role 
from ..model.assignment import Assignment
from ..test.helper import truncate_db

fake = Faker()


if __name__ == '__main__':

    # truncate db before seeding data
    truncate_db()

    # admin 
    user = Storage(
        email='929891169@qq.com',
        phone='18909512215',
        password_digest='8285A4269uction'
    )
    user = User.signup(user)
    profile = Profile.create_default_profile(user)
    account = Account.create(profile, Storage(balance=0))
    role = Role.create(Storage(name='admin'))
    Assignment.has_role(profile, role)

    # general user
    user = Storage(
        email='929891172@qq.com',
        phone='18909512218',
        password_digest='3338179'
    )
    user = User.signup(user)
    profile = Profile.create_default_profile(user)
    account = Account.create(profile, Storage(balance=0))

    user = Storage(
        email='929891173@qq.com',
        phone='18909512219',
        password_digest='3338179'
    )
    user = User.signup(user)
    profile = Profile.create_default_profile(user)
    account = Account.create(profile, Storage(balance=0))
    print('data has been added')
