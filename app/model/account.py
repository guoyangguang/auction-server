# -*- coding: utf-8 -*-

'''
model account
'''

from wheezy.validation import Validator, rules
from ..config import conf
from ..box import utils


def be_unique_profile(profile_id):
    '''
    check if account is already created for profile
    '''
    accounts = conf.db.query(
        'select * from accounts where profile_id=$profileid and deleted_at is null',
        vars=dict(profileid=profile_id)
    )
    return True if len(accounts) == 0 else False


class Account(object):

    table = 'accounts'

    # validate balance even if balance is set by code
    validator = Validator({
        'profile_id': [
            rules.must(
                be_unique_profile,
                utils.invalid_msgs['unique'].format('account')
            )
        ],
        'balance': [
            rules.must(
                utils.is_positive_int_or_zero,
                message_template=utils.invalid_msgs['positive_int_or_zero']
            )
        ]
    })

    @classmethod
    def create(cls, profile, account):
        '''
        create account for the profile
        must validate account when creating except AuthSignupAPI.POST
        '''
        _id = conf.db.insert(
            'accounts',
            profile_id=profile.id,
            balance=int(account.balance)
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter account by _id
        '''
        accounts = conf.db.query(
            'select * from accounts where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(accounts) == 1:
            return accounts[0]

    @classmethod
    def filter_by_profile(cls, profile):
        '''
        filter account by profile 
        '''
        accounts = conf.db.query(
            'select * from accounts where profile_id=$profileid and deleted_at is null',
            vars=dict(profileid=profile.id)
        )
        if len(accounts) == 1:
            return accounts[0]
