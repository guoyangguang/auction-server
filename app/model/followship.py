# -*- coding: utf-8 -*-
'''
followship module
'''

from .profile import Profile
from .artist import Artist
from ..config.conf import db
from web import Storage
from datetime import datetime


class Followship(object):
    '''
    followship class
    '''

    table = 'followship'
    per_page = 15
 
    @classmethod
    def follow(cls, following, followed):
        '''
        follow
        '''
        # NOTE  check followeds before calling the method
        # to ensure primary key that is not deleted
        followship = Storage(
            following_id=following.id, followed_id=followed.id
        )
        val = db.insert(
            'followship',
            following_id=followship.following_id,
            followed_id=followship.followed_id
        )
        # val is None in this case
        return cls.get(following.id, followed.id)

    @classmethod
    def unfollow(cls, followship):
        '''
        unfollow
        '''
        followship.deleted_at = datetime.now()
        num = db.update(
            'followship',
            deleted_at=followship.deleted_at,
            where='following_id=$ing and followed_id=$ed and deleted_at is null',
            vars=dict(
                ing=followship.following_id, ed=followship.followed_id
            )
        )
        if num == 1:
            return followship

    @classmethod
    def get(cls, following_id, followed_id):
        '''
        filter followship by following_id, followed_id
        '''
        val = db.query(
            'select * from followship where following_id=$ing and \
followed_id=$ed and deleted_at is null',
            vars=dict(ing=following_id, ed=followed_id)
        )
        if len(val) == 1:
            return val[0]

    @classmethod
    def followings(cls, profile):
        '''
        filter profile's followings
        '''
        val = db.query(
            'select followed_id from followship where following_id=$_id \
and deleted_at is null order by created_at desc',
            vars=dict(_id=profile.id)
        )
        ids = [followship.followed_id for followship in val]
        return Artist.filter_by_ids(ids)

    @classmethod
    def is_following(cls, profile, artist):
        val = db.query(
            'select followed_id from followship where following_id=$_id \
and deleted_at is null',
            vars=dict(_id=profile.id)
        )
        ids = [followship.followed_id for followship in val]
        if artist.id in ids:
            return True
        return False

    @classmethod
    def followeds(cls, artist, page):
        '''
        filter artist's followeds
        '''
        val = db.query(
            'select following_id from followship where followed_id=$_id \
and deleted_at is null order by created_at desc limit $limit offset $offset',
            vars=dict(
                _id=artist.id,
                limit=cls.per_page,
                offset=(page-1)*cls.per_page
            )
        )
        ids = [followship.following_id for followship in val]
        return Profile.filter_by_ids(ids)
