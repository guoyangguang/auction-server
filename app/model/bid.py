# -*- coding: utf-8 -*-

'''
model bid
'''

from datetime import datetime
from wheezy.validation import Validator, rules
from ..config import conf
from ..box import utils


class Bid(object):
    '''
    model bid
    '''
    table = 'bids'
    per_page = 15
    validator = Validator({
        'price': [
            rules.must(
                utils.is_positive_int,
                message_template=utils.invalid_msgs['positive_int']
            )
        ],
        'status':[
            rules.one_of(
                [1, 2, 3], # 1, 出局;  2, 领先; 3, 成交
                utils.invalid_msgs['one_of']
            )
        ]
    })

    @classmethod
    def create(cls, item, profile, bid):
        '''
        create an bid
        '''
        _id = conf.db.insert(
            cls.table,
            item_id=item.id,
            profile_id=profile.id,
            price=bid.price,
            status=bid.status
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter bid with id
        '''
        bids = conf.db.query(
            'select * from bids where id=$_id and deleted_at is null',
            vars={'_id': _id}
        )
        if len(bids) == 1:
            return bids[0]

    @classmethod
    def filter_by_item(cls, item, page):
        '''
        filter bids by item
        '''
        bids = conf.db.query(
            cls._select_bid_and_profile() +
            ' from bids inner join profiles on bids.profile_id=' +
            'profiles.id where bids.item_id=$item_id and' +
            ' bids.deleted_at is null order by bids.price DESC' +
            ' limit $limit offset $offset',
            vars=dict(
                item_id=item.id,
                limit=cls.per_page,
                offset=(page-1)*cls.per_page
            )
        )
        return list(bids)

    @classmethod
    def filter_by_status(cls, status):
        '''
        filter bids by status 
        '''
        bids = conf.db.query(
            'select * from bids where status=$_status and deleted_at is null',
            vars=dict(_status=status)
        )
        return list(bids)

    @classmethod
    def _select_bid_and_profile(cls):
        '''
        select bid and profile
        '''
        bid_cols = [
            'id', 'profile_id', 'item_id', 'price', 'status',
            'created_at', 'updated_at', 'deleted_at'
        ]
        profile_cols = [
            'id', 'user_id', 'name', 'gender', 'location',
            'job', 'img', 'about', 'uploaded_at', 'created_at',
            'updated_at', 'deleted_at'
        ]
        select_bid = ', '.join(
            ['bids.{0}'.format(col) for col in bid_cols]
        )
        select_profile = ', '.join(
            ['profiles.{0} as profiles_{1}'.format(col, col) for col in profile_cols]
        )
        return 'select ' + select_bid + ', ' + select_profile

    @classmethod
    def filter_max_bid_by_item(cls, item):
        '''
        filter max bid for the item
        '''
        bids = conf.db.query(
            'select * from bids where item_id=$itemid and deleted_at is null',
            vars=dict(itemid=item.id)
        )
        bids = list(bids)
        if bids:
            return max(bids, key=lambda bid: bid.price) 
        else:
            return None

    @classmethod
    def update_status(cls, bid, status):
        '''
        update the bid's status
        '''
        bid.status=status 
        bid.updated_at = datetime.now()
        num = conf.db.update(
            cls.table,
            status=bid.status,
            updated_at=bid.updated_at,
            where='id=$_id',
            vars=dict(_id=bid.id)
        )
        if num == 1:
            return bid

    @classmethod
    def update_status_from2to3_by_itemids(cls, item_ids):
        '''
        update the bids' status
        '''
        num = conf.db.update(
            cls.table,
            status=3,
            updated_at=datetime.now(),
            where='item_id in $_ids and status=2',
            vars=dict(_ids=item_ids)
        )
        return num 
