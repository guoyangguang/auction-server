# -*- coding: utf-8 -*-

'''
model artist
'''

import json
from datetime import datetime
from wheezy.validation import Validator, rules
from web import Storage
from ..config import conf
from ..box import utils


class Artist(object):
    '''
    model artist
    '''
    table = 'artists'
    per_page = 15
    versions = {'detail': 270, 'thumb': 160}
    validator = Validator({
        'name': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(1, 30, utils.invalid_msgs['length'].format(1, 30))
        ],
        'intro': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(
                100,
                500,
                utils.invalid_msgs['length'].format(100, 500)
            )
        ]
    })

    @classmethod
    def create(cls, artist):
        '''
        create an artist
        '''
        _id = conf.db.insert(
            cls.table,
            name=artist.name,
            intro=artist.intro,
            img=artist.img,
            img_meta=json.dumps(artist.img_meta),
            uploaded_at=datetime.now()
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def update(cls, artist, new):
        '''
        update the artist with new one
        '''
        now = datetime.now()
        artist.name = new.name
        artist.intro = new.intro
        artist.img = new.img
        artist.img_meta = json.dumps(new.img_meta) 
        artist.updated_at = now 
        artist.uploaded_at = now 
        num = conf.db.update(
            cls.table,
            name=artist.name,
            intro=artist.intro,
            img=artist.img,
            img_meta=artist.img_meta,
            updated_at=artist.updated_at,
            uploaded_at=artist.uploaded_at,
            where='id=$_id',
            vars={'_id': artist.id}
        )
        if num == 1:
            return artist 

    @classmethod
    def get(cls, _id):
        '''
        filter artist with id
        '''
        artists = conf.db.query(
            'select * from artists where id=$_id and deleted_at is null',
            vars={'_id': _id}
        )
        if len(artists) == 1:
            return artists[0]

    @classmethod
    def all(cls, page):
        '''
        filter all artists with page
        '''
        artists = conf.db.query(
            'select * from artists where deleted_at is null order by created_at desc \
limit $limit offset $offset',
            vars=dict(
                limit=cls.per_page, offset=(page-1)*cls.per_page
            )
        )
        return list(artists)

    @classmethod
    def filter_by_ids(cls, _ids):
        '''
        filter artists by a list of ids
        '''
        if len(_ids) == 0:
            return list()
        artists = conf.db.query(
            'select * from artists where id in $ids and deleted_at is null',
            vars=dict(ids=_ids)
        )
        return list(artists)
