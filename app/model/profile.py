# -*- coding: utf-8 -*-
'''
model profile
'''

from wheezy.validation import Validator, rules
from web import Storage
from datetime import datetime
import json
from ..config import conf
from ..box import utils


class Profile(object):
    '''
    model profile
    '''
    table = 'profiles'
    versions = {'detail': 160, 'thumb': 50}

    validator = Validator({
        'name': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(1, 30, utils.invalid_msgs['length'].format(1, 30))
        ],
        'gender': [rules.one_of(['1', '0'], utils.invalid_msgs['one_of'])],
        'location': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(2, 100, utils.invalid_msgs['length'].format(2, 100))
        ],
        'job': [rules.one_of(['1', '2', '3'], utils.invalid_msgs['one_of'])]
    })

    @classmethod
    def get(cls, _id):
        '''
        filter profile by id
        '''
        profiles = conf.db.query(
            'select * from profiles where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(profiles) == 1:
            return profiles[0]

    @classmethod
    def filter_by_ids(cls, ids):
        '''
        filter profiles by a list of ids
        '''
        if len(ids) == 0:
            return list()
        profiles = conf.db.query(
            'select * from profiles where id in $ids and deleted_at is null',
            vars=dict(ids=ids)
        )
        return list(profiles)

    @classmethod
    def filter_by_user(cls, user):
        '''
        filter profile by user
        '''
        profiles = conf.db.query(
            'select * from profiles where user_id=$_id and deleted_at is null',
            vars=dict(_id=user.id)
        )
        if len(profiles) == 1:
            return profiles[0]

    @classmethod
    def update(cls, profile, new):
        '''
        update profile general field with a new one
        '''
        profile.name = new.name
        profile.gender = int(new.gender)
        profile.location = new.location
        profile.job = int(new.job)
        profile.about = new.about
        profile.updated_at = datetime.now()
        num = conf.db.update(
            'profiles',
            name=profile.name,
            gender=profile.gender,
            location=profile.location,
            job=profile.job,
            about=profile.about,
            updated_at=profile.updated_at,
            where='id=$_id',
            vars=dict(_id=profile.id)
        )
        if num == 1:
            return profile

    @classmethod
    def update_img(cls, profile, img_name, img_meta):
        '''
        update img
        '''
        profile.img = img_name
        profile.img_meta = json.dumps(img_meta),
        profile.updated_at = datetime.now()
        profile.uploaded_at = datetime.now()
        num = conf.db.update(
            'profiles',
            img=profile.img,
            img_meta=profile.img_meta,
            updated_at=profile.updated_at,
            uploaded_at=profile.uploaded_at,
            where='id=$_id',
            vars=dict(_id=profile.id)
        )
        if num == 1:
            profile.img_meta = img_meta
            return profile

    @classmethod
    def create_default_profile(cls, user):
        '''
        create default profile for the user
        '''
        name = user.email.rsplit('@', 1)[0]
        profile = Storage(user_id=user.id, name=name, img=conf.DEFAULT_UPLOAD)
        _id = conf.db.insert(
            'profiles',
            user_id=profile.user_id,
            name=profile.name,
            img=profile.img
        )
        if _id:
            return cls.get(_id)
