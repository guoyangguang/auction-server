#-*- coding: utf-8 -*-

'''
orders model
'''

from datetime import datetime
from wheezy.validation import Validator, rules
from ..config import conf
from ..box import utils

class Order(object):
    '''
    order class
    '''
    table = 'orders'
    per_page = 15

    validator = Validator({
        'amount': [
            rules.must(
                utils.is_positive_int,
                message_template=utils.invalid_msgs['positive_int']
            )
        ]
    })

    @classmethod
    def get(cls, _id): 
        '''
        filter order by _id
        '''
        orders = conf.db.query(
            'select * from orders where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(orders) == 1:
            return orders[0]

    @classmethod
    def create(cls, profile, item, new_order):
        '''
        create an order for the profile and the item.
        '''
        _id = conf.db.insert(
            cls.table,
            profile_id=profile.id,
            item_id=item.id,
            amount=new_order.amount,
            status=1
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def paid(cls, order):
        '''
        update the order's status, pay_method, paid_at after it is paid.
        '''
        order.status = 2
        order.updated_at = datetime.now()
        num = conf.db.update(
            'orders',
            status=order.status,
            pay_method=order.pay_method,
            paid_at=order.paid_at,
            updated_at=order.updated_at,
            where='id=$_id',
            vars=dict(_id=order.id)
        )
        if num == 1:
            return order

    @classmethod
    def refund(cls, order):
        '''
        update the order's status, pay_method, paid_at after it is paid.
        '''
        order.status = 3 
        order.updated_at = datetime.now()
        num = conf.db.update(
            'orders',
            status=order.status,
            refunded_at=order.refunded_at,
            updated_at=order.updated_at,
            where='id=$_id',
            vars=dict(_id=order.id)
        )
        if num == 1:
            return order

    @classmethod
    def filter_by_profile(cls, profile, page):
        '''
        filter the orders by the profile
        '''
        orders = conf.db.query(
            'select * from orders where profile_id=$profileid and deleted_at \
is null order by created_at limit $limit offset $offset',
            vars=dict(
                profileid=profile.id,
                limit=cls.per_page,
                offset=(page-1)*cls.per_page
            )
        )
        return list(orders)
