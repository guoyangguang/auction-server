# -*- coding: utf-8 -*-
'''
model verification
'''

from wheezy.validation import Validator, rules
from datetime import datetime
from ..config import conf 
from ..box import utils


def is_unique_phone(phone):
    '''
    check if verification with same phone already exist 
    '''
    verifications = conf.db.query(
        'select * from verifications where phone=$phone and deleted_at is null',
        vars=dict(phone=phone)
    )
    return True if len(verifications) == 0 else False


class Verification(object):

    table = 'verifications'

    validator = Validator({
        'phone': [
            rules.must(
                utils.is_phone,
                message_template=utils.invalid_msgs['phone']
            )
            # rules.must(
            #     is_unique_phone,
            #     utils.invalid_msgs['unique'].format('phone')
            # )
        ]
    })

    @classmethod
    def create(cls, verification):
        '''
        create verification
        '''
        _id = conf.db.insert(
            'verifications',
            phone=verification.phone,
            verification_code=int(verification.verification_code)
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter by id
        '''
        verifications = conf.db.query(
            'select * from verifications where id=$_id and deleted_at is null', 
            vars=dict(_id=_id)
        )
        if len(verifications) == 1:
            return verifications[0]

    @classmethod
    def update_code(cls, verification):
        '''
        update verification code for the exist verification
        '''
        verification.updated_at = datetime.now()
        num = conf.db.update(
            'verifications',
            verification_code=int(verification.verification_code),
            updated_at=verification.updated_at,
            where='id=$_id',
            vars=dict(_id=verification.id)
        )
        if num == 1:
            return verification

    @classmethod
    def filter_by_phone(cls, phone):
        '''
        filter by phone
        '''
        verifications = conf.db.query(
            'select * from verifications where phone=$phone and deleted_at is null', 
            vars=dict(phone=phone)
        )
        return list(verifications)
