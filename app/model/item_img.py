# -*- coding: utf-8 -*-
'''
model item imgs
'''

from datetime import datetime
from wheezy.validation import Validator, rules
import json
from ..config import conf
from ..box import utils


class ItemImg(object):

    table = 'item_imgs'
    versions = {'thumb': 235, 'detail': 425}
    validator = Validator({
        'description': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(1, 50, utils.invalid_msgs['length'].format(1, 50))
        ]
    })

    @classmethod
    def create(cls, item, itemimg):
        '''
        create an item_img for item
        '''
        _id = conf.db.insert(
            'item_imgs',
            item_id=item.id,
            description=itemimg.description,
            img=itemimg.img,
            img_meta=json.dumps(itemimg.img_meta),
            uploaded_at=datetime.now()
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter item_img by id
        '''
        itemimgs = conf.db.query(
            'select * from item_imgs where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(itemimgs) == 1:
            return itemimgs[0]

    @classmethod
    def filter_by_item(cls, item):
        '''
        filter item_imgs by item
        '''
        itemimgs = conf.db.query(
            'select * from item_imgs where item_id=$item_id \
and deleted_at is null order by created_at',
            vars=dict(item_id=item.id)
        )
        return list(itemimgs)
