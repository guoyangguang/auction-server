# -*- coding: utf-8 -*-

'''
mode item
'''

from wheezy.validation import Validator, rules
from datetime import datetime
import json
from ..config.conf import db
from ..box import utils


class Item(object):
    '''
    model item
    '''

    table = 'items'
    per_page = 15
    versions = {'thumb': 235, 'detail': 425}

    validator = Validator({
        'kind': [
            rules.one_of(
                [str(ele) for ele in list(range(1, 9))],
                utils.invalid_msgs['one_of']
            )
        ],
        'name': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(1, 30, utils.invalid_msgs['length'].format(1, 30))
        ],
        'description': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(10, 500, utils.invalid_msgs['length'].format(10, 500))
        ],
        'length': [
            rules.must(
                utils.is_positive_int,
                message_template=utils.invalid_msgs['positive_int']
            )
        ],
        'width': [
            rules.must(
                utils.is_positive_int,
                message_template=utils.invalid_msgs['positive_int']
            )
        ],
        'usage': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(1, 50, utils.invalid_msgs['length'].format(1, 50))
        ],
        'produced_at': [
            rules.must(
                utils.is_valid_date,
                message_template=utils.invalid_msgs['date']
            )
        ],
        'starting_price': [
            rules.must(
                utils.is_positive_int_or_zero,
                message_template=utils.invalid_msgs['positive_int_or_zero']
            )
        ],
        'raising_price': [
            rules.must(
                utils.is_positive_int,
                message_template=utils.invalid_msgs['positive_int']
            )
        ],
        'deposit': [
            rules.must(
                utils.is_positive_int_or_zero,
                message_template=utils.invalid_msgs['positive_int_or_zero']
            )
        ],
        'starting_bid': [
            rules.must(
                utils.is_valid_datetime,
                message_template=utils.invalid_msgs['datetime']
            )
        ],
        'closing_bid': [
            rules.must(
                utils.is_valid_datetime,
                message_template=utils.invalid_msgs['datetime']
            )
        ]
    })

    @classmethod
    def create(cls, artist, item):
        '''
        create an item for artist
        '''
        _id = db.insert(
            'items',
            artist_id=artist.id,
            kind=int(item.kind),
            name=item.name,
            description=item.description,
            size=[int(item.length), int(item.width)],
            usage=item.usage,
            produced_at=datetime.strptime(item.produced_at, '%Y/%m/%d'),
            starting_price=int(item.starting_price) * 100,
            raising_price=int(item.raising_price) * 100,
            deposit=int(item.deposit) * 100,
            starting_bid=datetime.strptime(
                item.starting_bid, '%Y/%m/%d/%H:%M'
            ),
            closing_bid=datetime.strptime(
                item.closing_bid, '%Y/%m/%d/%H:%M'
            ),
            img=item.img,
            img_meta=json.dumps(item.img_meta),
            uploaded_at=datetime.now()
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter item by _id
        '''
        items = db.query(
            'select * from items where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(items) == 1:
            return items[0]

    @classmethod
    def all(cls, page):
        '''
        get all the items
        '''
        items = db.query(
            'select * from items where deleted_at is null \
order by created_at desc limit $limit offset $offset',
            vars=dict(
                limit=cls.per_page, offset=(page-1)*cls.per_page
            )
        )
        return list(items)

    @classmethod
    def filter_by_artist(cls, artist, page):
        '''
        filter items by artist 
        '''
        items = db.query(
            'select * from items where artist_id=$artistid and deleted_at \
is null order by created_at limit $limit offset $offset',
            vars=dict(
                artistid=artist.id,
                limit=cls.per_page,
                offset=(page-1)*cls.per_page
            )
        )
        return list(items)

    @classmethod
    def filter_closed_items(cls, now):
        '''
        filter closed items
        '''
        items = db.query(
            'select * from items where closing_bid <= $_now and deleted_at is null',
            vars=dict(_now=now)
        )
        return list(items)
