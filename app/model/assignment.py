# -*- coding: utf-8 -*-

'''
assignment model module
'''

from datetime import datetime
from web import Storage
from .role import Role
from ..config.conf import db


class Assignment(object):
    '''
    assignment model class
    '''

    table = 'assignments'

    @classmethod
    def get(cls, profile, role):
        '''
        filter assignment by profile and role
        '''
        assignments = db.query(
            'select * from assignments where profile_id=$pid' +
            ' and role_id=$rid and deleted_at is null',
            vars=dict(pid=profile.id, rid=role.id)
        )
        if len(assignments) == 1:
            return assignments[0]

    @classmethod
    def roles_of(cls, profile):
        '''
        filter profile's roles
        '''
        assignments = db.query(
            'select role_id from assignments where profile_id=$_id \
             and deleted_at is null',
            vars=dict(_id=profile.id)
        )
        ids = [assignment.role_id for assignment in assignments]
        return Role.filter_by_ids(ids)

    @classmethod
    def is_role(cls, profile, roles):
        '''
        check if profile has one of roles
        '''
        if_role = False
        for role in cls.roles_of(profile):
            for role_name in roles:
                if role.name == role_name:
                    if_role = True
                    break
        return if_role

    @classmethod
    def has_role(cls, profile, role):
        '''
        assign role to profile
        '''
        # NOTE check profile's roles before calling the method
        # to ensure primary key that is not deleted
        assignment = Storage(profile_id=profile.id, role_id=role.id)
        db.insert(
            'assignments',
            profile_id=assignment.profile_id,
            role_id=assignment.role_id
        )
        return cls.get(profile, role)

    @classmethod
    def has_no_role(cls, assignment):
        '''
        delete assignment to make profile has no role
        '''
        assignment.deleted_at = datetime.now()
        num = db.update(
            'assignments',
            deleted_at=assignment.deleted_at,
            where='profile_id=$uid and role_id=$rid and deleted_at is null',
            vars=dict(uid=assignment.profile_id, rid=assignment.role_id)
        )
        if num == 1:
            return assignment
