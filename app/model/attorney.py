# -*- coding: utf-8 -*-

'''
model attorney
'''

import json
from datetime import datetime
from wheezy.validation import Validator, rules
from ..config import conf
from ..box import utils


class Attorney(object):
    '''
    model attorney
    '''
    table = 'attorneys'
    versions = {'thumb': 235, 'detail': 425}

    @classmethod
    def create(cls, artist, attorney):
        '''
        create an attorney
        '''
        _id = conf.db.insert(
            cls.table,
            artist_id=artist.id,
            img=attorney.img,
            img_meta=json.dumps(attorney.img_meta),
            uploaded_at=datetime.now()
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter attorney with id
        '''
        attorneys = conf.db.query(
            'select * from attorneys where id=$_id and deleted_at is null',
            vars={'_id': _id}
        )
        if len(attorneys) == 1:
            return attorneys[0]

    @classmethod
    def filter_by_artist(cls, artist):
        '''
        filter by artist
        '''
        attorneys = conf.db.query(
            'select * from attorneys where artist_id=$artistid and deleted_at' +
            ' is null order by created_at',
            vars={'artistid': artist.id}
        )
        return list(attorneys)
