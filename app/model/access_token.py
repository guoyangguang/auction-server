# -*- coding: utf-8 -*-

'''
model access_token
'''

from wheezy.validation import Validator, rules
from datetime import datetime
from ..box import utils
from ..config import conf


class AccessToken(object):
    '''
    access token
    '''
    table = 'access_tokens'

    validator = Validator({
        'site': [
            rules.one_of(
                ['1', '2', '3'],
                utils.invalid_msgs['one_of']
            )
        ]
    })

    @classmethod
    def create(cls, profile, access_token):
        '''
        create oauth access_token for the profile.
        ensure profile_id and site as primary key
        '''
        _id = conf.db.insert(
            'access_tokens',
            profile_id=profile.id,
            site=int(access_token.site),
            access_token=access_token.access_token,
            expires_in=access_token.expires_in,
            user_id=access_token.user_id
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter accesss token by _id
        '''
        access_tokens = conf.db.query(
            'select * from access_tokens where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(access_tokens) == 1:
            return access_tokens[0]

    @classmethod
    def filter_by_profile_and_site(cls, profile, site):
        '''
        filter accesss token by profile and site
        '''
        access_tokens = conf.db.query(
            'select * from access_tokens where profile_id=$profileid' +
            ' and site=$site and deleted_at is null',
            vars=dict(profileid=profile.id, site=site)
        )
        if len(access_tokens) == 1:
            return access_tokens[0]

    @classmethod
    def filter_by_profile(cls, profile):
        '''
        filter accesss token by profile
        '''
        access_tokens = conf.db.query(
            'select * from access_tokens where profile_id=$profileid' +
            ' and deleted_at is null',
            vars=dict(profileid=profile.id)
        )
        return list(access_tokens)

    @classmethod
    def update_access_token(cls, access_token):
        '''
        update access_token for the access_token
        '''
        access_token.updated_at = datetime.now()
        num = conf.db.update(
            'access_tokens',
            updated_at=access_token.updated_at,
            access_token=access_token.access_token,
            expires_in=access_token.expires_in,
            user_id=access_token.user_id,
            where='id=$_id',
            vars=dict(_id=access_token.id)
        )
        if num == 1:
            return access_token
