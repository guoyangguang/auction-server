# -*- coding: utf-8 -*-
'''
model comment
'''

from wheezy.validation import Validator, rules
from ..config import conf
from ..box import utils


class Comment(object):
    '''
    model comment
    '''

    table = 'comments'
    per_page = 15

    comment_validator = Validator({
        'body': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(
                1, 100, utils.invalid_msgs['length'].format(1, 100)
            )
        ]
    })
    reply_validator = Validator({
        'comment_id': [
            rules.must(
                utils.is_positive_int,
                message_template=utils.invalid_msgs['positive_int']
            )
        ],
        'body': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(
                1, 100, utils.invalid_msgs['length'].format(1, 100)
            )
        ]
    })

    @classmethod
    def create(cls, profile, item, new, comment=None):
        '''
        profile create comment for an item
        '''
        if comment:
            _id = conf.db.insert(
                'comments',
                profile_id=profile.id,
                item_id=item.id,
                comment_id=comment.id,
                body=new.body
            )
        else:
            _id = conf.db.insert(
                'comments',
                profile_id=profile.id,
                item_id=item.id,
                body=new.body
            )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter comment by id
        '''
        comments = conf.db.query(
            'select * from comments where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(comments) == 1:
            return comments[0]

    @classmethod
    def filter_by_item(cls, item, page):
        '''
        filter all the comments of item plus profiles
        '''
        # TODO if there is a comment_id in one comment,
        # how client get the comment that has the comment_id?
        comments = conf.db.query(
            cls._select_comment_and_profile() +
            ' from comments inner join profiles on comments.profile_id=' +
            'profiles.id where comments.item_id=$item_id and' +
            ' comments.deleted_at is null order by comments.created_at' +
            ' limit $limit offset $offset',
            vars=dict(
                item_id=item.id,
                limit=cls.per_page,
                offset=(page-1)*cls.per_page
            )
        )
        return list(comments)

    @classmethod
    def _select_comment_and_profile(cls):
        '''
        select comment and profile
        '''
        comment_cols = [
            'id', 'profile_id', 'item_id', 'comment_id', 'body',
            'created_at', 'updated_at', 'deleted_at'
        ]
        profile_cols = [
            'id', 'user_id', 'name', 'gender', 'location',
            'job', 'img', 'about', 'uploaded_at', 'created_at',
            'updated_at', 'deleted_at'
        ]
        select_comment = ', '.join(
            ['comments.{0}'.format(col) for col in comment_cols]
        )
        select_profile = ', '.join(
            ['profiles.{0} as profiles_{1}'.format(col, col) for col in profile_cols]
        )
        return 'select ' + select_comment + ', ' + select_profile
