# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ..helper import truncate_db, signup
from ...model.followship import Followship
from ...model.user import User
from ...model.profile import Profile 
from ...model.artist import Artist
from ...model.role import Role
from ...model.assignment import Assignment


class FollowshipTest(unittest.TestCase):
   
    def setUp(self):
        user1 = Storage(
            email='user1@book.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user1 = User.signup(user1)
        self.profile1 = Profile.create_default_profile(self.user1)

        user2 = Storage(
            email='user2@book.com',
            phone='18612345679',
            password_digest='123password4'
        )
        self.user2 = User.signup(user2)
        self.profile2 = Profile.create_default_profile(self.user2)
        
        artist1 = Storage(
            name='artist1',
            intro='artist1 is a famous'*10,
            img='artist1.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist1 = Artist.create(artist1)
        artist2 = Storage(
            name='artist2',
            intro='artist2 is a famous'*10,
            img='artist2.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist2 = Artist.create(artist2)

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Followship.table, 'followship')

    def test_follow(self):
        self.assertEqual(len(Followship.followings(self.profile1)), 0)
        self.assertEqual(len(Followship.followeds(self.artist1, 1)), 0)
        self.assertEqual(len(Followship.followeds(self.artist2, 1)), 0)
        Followship.follow(self.profile1, self.artist1) 
        Followship.follow(self.profile1, self.artist2) 

        artists = Followship.followings(self.profile1)
        self.assertEqual(len(artists), 2)
        self.assertTrue(self.artist1 in  artists)
        self.assertTrue(self.artist2 in artists)

        profiles = Followship.followeds(self.artist1, 1)
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0], self.profile1)

        profiles = Followship.followeds(self.artist2, 1)
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0], self.profile1)

    def test_unfollow(self):
        followship = Followship.follow(self.profile1, self.artist1) 
        Followship.follow(self.profile1, self.artist2) 

        artists = Followship.followings(self.profile1)
        self.assertEqual(len(artists), 2)
        self.assertTrue(self.artist1 in artists)
        self.assertTrue(self.artist2 in artists)

        artists = Followship.followeds(self.artist1, 1)
        self.assertEqual(len(artists), 1)
        self.assertEqual(artists[0], self.profile1)
        
        followship = Followship.unfollow(followship)
        self.assertIsInstance(followship.deleted_at, datetime)
        artists = Followship.followings(self.profile1)
        self.assertEqual(len(artists), 1)
        self.assertEqual(artists[0], self.artist2)
        profiles = Followship.followeds(self.artist1, 1)
        self.assertEqual(len(profiles), 0)

    def test_get(self):
        followship = Followship.get(self.profile1.id, self.artist1.id)
        self.assertIsNone(followship)
        Followship.follow(self.profile1, self.artist1)
        followship = Followship.get(self.profile1.id, self.artist1.id)
        self.assertEqual(followship.following_id, self.profile1.id)
        self.assertEqual(followship.followed_id, self.artist1.id)
        self.assertIsInstance(followship.created_at, datetime)
        self.assertIsNone(followship.deleted_at)

    def test_followings(self):
        self.assertEqual(len(Followship.followings(self.profile1)), 0)
        Followship.follow(self.profile1, self.artist1) 
        Followship.follow(self.profile1, self.artist2) 
        artists = Followship.followings(self.profile1)
        self.assertEqual(len(artists), 2)
        self.assertTrue(self.artist1 in artists)
        self.assertTrue(self.artist2 in artists)

    def test_is_following(self):
        self.assertFalse(
            Followship.is_following(self.profile1, self.artist1)
        )
        Followship.follow(self.profile1, self.artist1) 
        self.assertTrue(
            Followship.is_following(self.profile1, self.artist1)
        )

    def test_followeds(self):
        self.assertEqual(len(Followship.followeds(self.artist1, 1)), 0)
        self.assertEqual(len(Followship.followeds(self.artist2, 1)), 0)
        Followship.follow(self.profile1, self.artist1) 
        Followship.follow(self.profile1, self.artist2) 

        profiles = Followship.followeds(self.artist1, 1)
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0], self.profile1)

        profiles = Followship.followeds(self.artist2, 1)
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0], self.profile1)


if __name__ == '__main__':
    unittest.main()
