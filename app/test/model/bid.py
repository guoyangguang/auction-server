# -*- coding: utf-8 -*-

from web import Storage
from datetime import datetime
import unittest
from ...model.artist import Artist
from ...model.item import Item
from ...model.bid import Bid 
from ...model.role import Role
from ...model.assignment import Assignment
from ..helper import truncate_db, signup
from ...box.utils import invalid_msgs


class BidTest(unittest.TestCase):

    def setUp(self):
        self.user, self.profile, self.account = signup()
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)
        self.item = Item.create(
            self.artist,
            Storage(
                kind='1',
                name='厚德载物',
                description='君子的品德应如大地般厚实可以载养万物。' +
                            '旧指道德高尚者能承担重大任务',
                length='150',
                width='60',
                usage='客厅 办公室',
                produced_at='2017/11/10',
                starting_price='99',
                raising_price='10',
                deposit='8',
                starting_bid='2017/11/12/09:00',
                closing_bid='2018/08/13/10:00',
                img='hdzw.jpg',
                img_meta=dict(
                    size=1024,
                    filetype='image/jpeg',
                    vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
                )
            )
        )
        self.bid = Storage(
            price=119,
            status=2
        )

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Bid.table, 'bids')

    def test_per_page(self):
        self.assertEqual(Bid.per_page, 15)

    def test_price_validation(self):
        errors = dict()
        self.assertTrue(Bid.validator.validate(self.bid, results=errors))
        for val in [None, '', '  ', '1.5', '-18', '18¥', '2015/11/02']:
            errors.clear()
            self.bid.price = val
            self.assertFalse(Bid.validator.validate(self.bid, results=errors))
            self.assertTrue(
                invalid_msgs.get('positive_int') in errors.get('price')
            )

    def test_status_validation(self):
        errors = dict()
        self.assertTrue(Bid.validator.validate(self.bid, results=errors))
        for val in [None, '', '  ', '1.5', '-18', '0', '18¥', '2015/11/02', 19, 20]:
            errors.clear()
            self.bid.status = val
            self.assertFalse(Bid.validator.validate(self.bid, results=errors))
            self.assertTrue(
                invalid_msgs.get('one_of') in errors.get('status')
            )

    def test_create(self):
        bid = Bid.create(self.item, self.profile, self.bid)
        self.assertIsInstance(bid.id, int)
        self.assertEqual(bid.price, self.bid.price)
        self.assertEqual(bid.status, self.bid.status)
        self.assertIsInstance(bid.created_at, datetime) 
        self.assertIsNone(bid.updated_at)
        self.assertIsNone(bid.deleted_at)

    def test_get(self):
        created = Bid.create(self.item, self.profile, self.bid)
        bid = Bid.get(created.id)
        self.assertEqual(created.id, bid.id)
        self.assertEqual(created, bid)

    def test_filter_by_item(self):
        created = Bid.create(self.item, self.profile, self.bid)
        bids = Bid.filter_by_item(self.item, 1)
        self.assertEqual(len(bids), 1)
        self.assertEqual(bids[0].id, created.id) 

    def test_filter_by_status(self):
        created = Bid.create(self.item, self.profile, self.bid)
        bids = Bid.filter_by_status(2)
        self.assertEqual(len(bids), 1)
        self.assertEqual(bids[0].id, created.id)
        bids = Bid.filter_by_status(1)
        self.assertEqual(len(bids), 0)


    def test_update_status(self):
        created = Bid.create(self.item, self.profile, self.bid)
        self.assertEqual(created.status, 2)
        Bid.update_status(created, 1)
        bid = Bid.get(created.id)
        self.assertEqual(bid.status, 1)
        self.assertIsInstance(bid.updated_at, datetime) 

    def test_filter_max_bid_by_item(self):
        bid = Bid.create(self.item, self.profile, self.bid)
        bid1 = Bid.create(
            self.item, self.profile, Storage(price=159, status=1)
        )
        max_bid = Bid.filter_max_bid_by_item(self.item)
        self.assertEqual(max_bid.price, bid1.price)

    def test_filter_max_bid_by_item_without_bids(self):
        max_bid = Bid.filter_max_bid_by_item(self.item)
        self.assertIsNone(max_bid)

    def test_update_status_from2to3_by_itemids(self):
        bid = Bid.create(self.item, self.profile, self.bid)
        artist1 = Storage(
            name='artist1',
            intro='artist1 is a famous'*10,
            img='artist1.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        artist1 = Artist.create(artist1)
        item1 = Item.create(
            artist1,
            Storage(
                kind='1',
                name='财兴业茂',
                description='财源滚滚，事业兴旺',
                length='150',
                width='60',
                usage='客厅 办公室',
                produced_at='2017/11/10',
                starting_price='109',
                raising_price='10',
                deposit='8',
                starting_bid='2018/08/12/09:00',
                closing_bid='2018/08/13/09:00',
                img='hdzz.jpg',
                img_meta=dict(
                    size=1024,
                    filetype='image/jpeg',
                    vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
                )
            )
        )
        bid1 = Bid.create(item1, self.profile, Storage(price=129, status=2))
        num = Bid.update_status_from2to3_by_itemids([self.item.id, item1.id])
        self.assertEqual(num, 2)
        self.assertEqual(Bid.get(bid.id).status, 3)
        self.assertEqual(Bid.get(bid1.id).status, 3)


if __name__ == '__main__':
    unittest.main()
