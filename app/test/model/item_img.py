# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime, timedelta
from ... model.artist import Artist
from ... model.item import Item
from ...model.item_img import ItemImg
from ...model.role import Role
from ...model.assignment import Assignment
from ...box import utils
from ..helper import truncate_db, signup


class ItemImgTest(unittest.TestCase):

    def setUp(self): 
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)
        self.item = Item.create(
            self.artist,
            Storage(
                kind='1',
                name='厚德载物',
                description='君子的品德应如大地般厚实可以载养万物。' +
                            '旧指道德高尚者能承担重大任务',
                length='150',
                width='60',
                usage='客厅 办公室',
                produced_at='2017/11/10',
                starting_price='99',
                raising_price='10',
                deposit='8',
                starting_bid='2017/11/12/09:00',
                closing_bid='2017/11/13/09:00',
                img='hdzw.jpg',
                img_meta=dict(
                    size=1024,
                    filetype='image/jpeg',
                    vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
                )
            )
        )
        self.item_img = Storage(
            description='The img contains artist.',
            img='img_artist.jpg',
            img_meta=dict(
                size=1024 * 2,
                filetype='image/jpeg'
            )
        )

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(ItemImg.table, 'item_imgs')

    def test_versions(self):
        self.assertEqual(ItemImg.versions, {'detail': 425, 'thumb': 235}) 

    def test_description_validation(self):
        errors = dict()
        self.assertTrue(
            ItemImg.validator.validate(self.item_img, results={})
        )
        for val in ['', None]:
            errors.clear()
            self.item_img.description = val
            self.assertFalse(
                ItemImg.validator.validate(self.item_img, results=errors)
            )
            self.assertTrue(utils.invalid_msgs['required'] in errors['description'])
        for val in ['the img contains artist'*4]:
            errors.clear()
            self.item_img.description = val
            self.assertFalse(
                ItemImg.validator.validate(self.item_img, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['length'].format(1, 50) in errors['description']
            )

    def test_create(self):
        created = ItemImg.create(self.item, self.item_img)
        self.assertIsInstance(created.id, int)
        self.assertEqual(created.item_id, self.item.id)
        self.assertTrue(created.img)
        self.assertEqual(created.description, self.item_img.description)
        self.assertEqual(created.img_meta, self.item_img.img_meta)
        self.assertIsInstance(created.created_at, datetime)
        self.assertIsNone(created.updated_at)
        self.assertIsNone(created.deleted_at)

    def test_get(self):
        created = ItemImg.create(self.item, self.item_img)
        itemimg = ItemImg.get(created.id)
        self.assertEqual(itemimg, created)

    def test_filter_by_item(self):
        created = ItemImg.create(self.item, self.item_img)
        itemimgs = ItemImg.filter_by_item(self.item)
        self.assertIsInstance(itemimgs, list)
        self.assertEqual(len(itemimgs), 1)
        self.assertEqual(itemimgs[0], created)


if __name__ == '__main__':
    unittest.main()
