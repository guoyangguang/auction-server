# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ...model.role import Role
from ..helper import truncate_db
from ...box.utils import invalid_msgs


class RoleTest(unittest.TestCase):

    def setUp(self):
        self.financial_officer = Storage(name='financial_officer')
        self.customer_service = Role.create(
            Storage(name='customer_service')
        )
        self.seller = Role.create(Storage(name='seller'))

    def tearDown(self):
        truncate_db()

    def test_validation_name(self):
        self.assertTrue(
            Role.validator.validate(
                self.financial_officer, results=dict()
            )
        )
        errors = dict()
        for val in ['@', '!', '$']:
            self.financial_officer.name = val
            errors.clear()
            self.assertFalse(
                Role.validator.validate(
                    self.financial_officer, results=errors
                )
            )
            self.assertTrue(invalid_msgs.get('slug') in errors.get('name'))
        self.financial_officer.name = 'superadmin' 
        errors.clear()
        self.assertFalse(
            Role.validator.validate(
                self.financial_officer, results=errors
            )
        )
        self.assertTrue(invalid_msgs.get('one_of') in errors.get('name'))
        Role.create(Storage(name='financial_officer'))
        self.financial_officer.name = 'financial_officer'
        errors.clear()
        self.assertFalse(
            Role.validator.validate(self.financial_officer, results=errors)
        )
        msg = invalid_msgs.get('unique').format('role')
        self.assertTrue(msg in errors.get('name'))
   
    def test_all(self):
        roles = Role.all()
        self.assertEqual(len(roles), 2)
        self.assertTrue(self.customer_service in roles)
        self.assertTrue(self.seller in roles)
        
    def test_filter_by_ids(self):
        ids = [self.seller.id, self.customer_service.id]
        roles = Role.filter_by_ids(ids)
        self.assertEqual(len(roles), 2)
        self.assertTrue(self.customer_service in roles)
        self.assertTrue(self.seller in roles)

    def test_filter_by_name(self):
        role = Role.filter_by_name('financial_officer')
        self.assertIsNone(role)
        role = Role.filter_by_name('customer_service')
        self.assertEqual(role.id, self.customer_service.id)

    def test_create(self): 
        financial_officer = Role.create(self.financial_officer)
        self.assertIsInstance(financial_officer.id, int)
        self.assertIsInstance(financial_officer.created_at, datetime)

    def test_delete(self):
        self.assertIsInstance(
            Role.filter_by_name('customer_service'),
            Storage
        )
        customer_service = Role.delete(self.customer_service) 
        self.assertIsInstance(customer_service.deleted_at, datetime)
        self.assertIsNone(
            Role.filter_by_name('customer_service')
        )

if __name__ == '__main__':
    unittest.main()
