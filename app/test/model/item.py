# -*- coding: utf-8 -*-

from web import Storage
from datetime import datetime, timedelta
import unittest
from ...model.artist import Artist
from ...model.item import Item
from ...model.role import Role
from ...model.assignment import Assignment
from ..helper import truncate_db, signup
from ...box.utils import invalid_msgs


class ItemTest(unittest.TestCase):

    def setUp(self):
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)
        self.item = Storage(
            kind='1',
            name='厚德载物',
            description='君子的品德应如大地般厚实可以载养万物。' +
                        '旧指道德高尚者能承担重大任务',
            length='150',
            width='60',
            usage='客厅 办公室',
            produced_at='2017/11/10',
            starting_price='99',
            raising_price='10',
            deposit='8',
            starting_bid='2017/11/12/09:00',
            closing_bid=(datetime.now() + timedelta(1)).strftime('%Y/%m/%d/%H:%M'),
            img='hdzw.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.item1 = Storage(
            kind='1',
            name='春华秋实',
            description='春华秋实'*10,
            length='100',
            width='50',
            usage='客厅 办公室',
            produced_at='2017/11/10',
            starting_price='98',
            raising_price='10',
            deposit='8',
            starting_bid='2017/11/12/09:00',
            closing_bid='2017/11/13/09:00',
            img='hdzw.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Item.table, 'items')

    def test_per_page(self):
        self.assertEqual(Item.per_page, 15)

    def test_versions(self):
        self.assertEqual(Item.versions, {'detail': 425, 'thumb': 235}) 

    def test_kind_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in [None, '', '  ', 1.5, -18, 0, '18¥', '2015/11/02', 19, 20]:
            errors.clear()
            self.item.kind = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('one_of') in errors.get('kind')
            )

    def test_name_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in ['', None]:
            errors.clear()
            self.item.name = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('required') in errors.get('name')
            )
        for val in ['book'*8]:
            errors.clear()
            self.item.name = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('length').format(1, 30) in errors.get('name')
            )

    def test_description_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in [None, '']:
            errors.clear()
            self.item.description = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('required') in errors.get('description')
            )
        for val in ['the cover', 'the cover'*60]:
            errors.clear()
            self.item.description = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('length').format(10, 500) in errors.get(
                    'description'
                )
            )

    def test_length_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in [None, '', '  ', '1.5', '-18', '0', '18¥', '2015/11/02']:
            errors.clear()
            self.item.length = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('positive_int') in errors.get('length')
            )

    def test_width_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in [None, '', '  ', '1.5', '-18', '0', '18¥', '2015/11/02']:
            errors.clear()
            self.item.width = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('positive_int') in errors.get('width')
            )

    def test_usage_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in [None, '']:
            errors.clear()
            self.item.usage = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('required') in errors.get('usage')
            )
        for val in ['the cover'*6]:
            errors.clear()
            self.item.usage = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('length').format(1, 50) in errors.get(
                    'usage'
                )
            )

    def test_produced_at_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['', '  ', '1', '二零壹伍']:
            errors.clear()
            self.item.produced_at = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('date') in errors.get('produced_at')
            )

    def test_starting_price_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in [None, '', '  ', '1.5', '-18', '18¥', '2015/11/02']:
            errors.clear()
            self.item.starting_price = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('positive_int_or_zero') in errors.get(
                    'starting_price'
                )
            )

    def test_raising_price_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in [None, '', '  ', '1.5', '-18', '0', '18¥', '2015/11/02']:
            errors.clear()
            self.item.raising_price = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('positive_int') in errors.get('raising_price')
            )

    def test_deposit_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in [None, '', '  ', '1.5', '-18', '18¥', '2015/11/02']:
            errors.clear()
            self.item.deposit = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('positive_int_or_zero') in errors.get(
                    'deposit'
                )
            )

    def test_starting_bid_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in [None, True, '', '  ', 1, '二零壹伍']:
            errors.clear()
            self.item.starting_bid = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('datetime') in errors.get('starting_bid')
            )

    def test_closing_bid_validation(self):
        errors = dict()
        self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in [None, True, '', '  ', 1, '二零壹伍']:
            errors.clear()
            self.item.closing_bid = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('datetime') in errors.get('closing_bid')
            )

    def test_create(self):
        item = Item.create(self.artist, self.item)
        self.assertIsInstance(item.id, int)
        self.assertEqual(item.artist_id, self.artist.id)
        self.assertEqual(
            item.size, [int(self.item.length), int(self.item.width)]
        )
        self.assertEqual(
            item.produced_at,
            datetime.strptime(self.item.produced_at, '%Y/%m/%d')
        )
        self.assertEqual(
            item.starting_price, int(self.item.starting_price) * 100
        )
        self.assertEqual(
            item.raising_price, int(self.item.raising_price) * 100
        )
        self.assertEqual(item.deposit, int(self.item.deposit) * 100)
        self.assertEqual(
            item.starting_bid,
            datetime.strptime(self.item.starting_bid, '%Y/%m/%d/%H:%M')
        ),
        self.assertEqual(
            item.closing_bid,
            datetime.strptime(self.item.closing_bid, '%Y/%m/%d/%H:%M')
        ),
        self.assertEqual(item.img_meta, self.item.img_meta),
        self.assertIsInstance(item.created_at, datetime)

    def test_get(self):
        created = Item.create(self.artist, self.item)
        item = Item.get(created.id)
        self.assertIsInstance(item, Storage)
        self.assertEqual(item, created)

    def test_all(self):
        created = Item.create(self.artist, self.item)
        created1 = Item.create(self.artist, self.item1)
        items = Item.all(1)
        self.assertEqual(len(items), 2)
        self.assertTrue(created in items)
        self.assertTrue(created1 in items)

    def test_filter_by_artist(self):
        created = Item.create(self.artist, self.item)
        items = Item.filter_by_artist(self.artist, 1)
        self.assertIsInstance(items, list)
        self.assertEqual(items[0].artist_id, self.artist.id)

    def test_filter_closed_items(self):
        created = Item.create(self.artist, self.item)
        created1 = Item.create(self.artist, self.item1)
        closed_items = Item.filter_closed_items(datetime.now())
        self.assertEqual(len(closed_items), 1)
        self.assertEqual(closed_items[0].id, created1.id)


if __name__ == '__main__':
    unittest.main()
