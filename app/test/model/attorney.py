# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ...model.artist import Artist
from ...model.attorney import Attorney
from ...model.role import Role
from ...model.assignment import Assignment
from ...box.utils import invalid_msgs
from ..helper import truncate_db, signup

class AttorneyTest(unittest.TestCase):

    def setUp(self): 
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)
        self.attorney = Storage(
            img='attorney.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )

        )

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Attorney.table, 'attorneys')

    def test_versions(self):
        self.assertEqual(Attorney.versions, {'detail': 425, 'thumb': 235})

    def test_create(self):  
        attorney = Attorney.create(self.artist, self.attorney)
        self.assertIsInstance(attorney.id, int)
        self.assertEqual(attorney.artist_id, self.artist.id)
        self.assertEqual(attorney.img, self.attorney.img)
        self.assertIsInstance(attorney.created_at, datetime)
        self.assertIsNone(attorney.updated_at)
        self.assertIsNone(attorney.deleted_at)

    def test_get(self):
        created_attorney = Attorney.create(self.artist, self.attorney)
        attorney = Attorney.get(created_attorney.id)
        self.assertIsInstance(attorney, Storage)
        self.assertEqual(attorney.id, created_attorney.id)
        self.assertEqual(attorney.img, created_attorney.img)

    def test_filter_by_artist(self):
        attorney1 = Storage(
            img='attorney1.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )

        )
        attorney = Attorney.create(self.artist, self.attorney)
        attorney1 = Attorney.create(self.artist, attorney1)
        attorneys = Attorney.filter_by_artist(self.artist)
        self.assertIsInstance(attorneys, list)
        self.assertEqual(len(attorneys), 2)
        self.assertTrue(attorney in attorneys)
        self.assertTrue(attorney1 in attorneys)


if __name__ == '__main__':
    unittest.main()
