#-*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ...box import utils
from ...model.role import Role
from ...model.assignment import Assignment
from ...model.artist import Artist
from ...model.item import Item
from ...model.order import Order
from ..helper import truncate_db, signup

class OrderTest(unittest.TestCase):

    def setUp(self): 
        user, self.profile, account = signup('artist@auction.com', '18909512216')
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        artist = Artist.create(artist)
        item = Storage(
            kind='1',
            name='厚德载物',
            description='君子的品德应如大地般厚实可以载养万物。\
旧指道德高尚者能承担重大任务',
            length='150',
            width='60',
            usage='客厅 办公室',
            produced_at='2017/11/10',
            starting_price='99',
            raising_price='10',
            deposit='8',
            starting_bid='2018/07/04/09:00',
            closing_bid='2018/07/05/09:00',
            img='hdzw.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.item = Item.create(artist, item)
        self.order = Storage(amount='100')

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Order.table, 'orders')

    def test_per_page(self):
        self.assertEqual(Order.per_page, 15)

    def test_amount_validation(self):
        errors = {}
        self.assertTrue(
            Order.validator.validate(self.order, results=errors)
        )
        for val in [None, '', '  ', '1.5', '-18', '0', '18¥', '2015/11/02']:
            errors.clear()
            self.order.amount = val
            self.assertFalse(
                Order.validator.validate(self.order, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs.get('positive_int') in errors.get('amount')
            )

    def test_get(self):
        self.order.amount = int(self.order.amount) * 100
        created = Order.create(self.profile, self.item, self.order)
        order = Order.get(created.id)
        self.assertEqual(order, created)
        self.assertIsInstance(order, Storage)

    def test_create(self):
        self.order.amount = int(self.order.amount) * 100
        created = Order.create(self.profile, self.item, self.order)
        self.assertIsInstance(created.id, int)
        self.assertEqual(created.profile_id, self.profile.id)
        self.assertEqual(created.item_id, self.item.id)
        self.assertEqual(created.amount, self.order.amount)
        self.assertEqual(created.status, 1)
        self.assertIsNone(created.pay_method)
        self.assertIsNone(created.paid_at)
        self.assertIsNone(created.refunded_at)
        self.assertIsInstance(created.created_at, datetime)
        self.assertIsNone(created.updated_at)
        self.assertIsNone(created.deleted_at)

    def test_paid(self):
        self.order.amount = int(self.order.amount) * 100
        created = Order.create(self.profile, self.item, self.order)
        self.assertEqual(created.status, 1)
        self.assertIsNone(created.pay_method)
        self.assertIsNone(created.paid_at)
        self.assertIsNone(created.updated_at)

        created.pay_method = 1
        created.paid_at= datetime.now() 
        paid = Order.paid(created)
        self.assertEqual(paid.status, 2)
        self.assertEqual(paid.pay_method, created.pay_method)
        self.assertEqual(paid.paid_at, created.paid_at)
        self.assertIsInstance(paid.updated_at, datetime)

    def test_refund(self):
        self.order.amount = int(self.order.amount) * 100
        created = Order.create(self.profile, self.item, self.order)
        created.pay_method = 1
        created.paid_at= datetime.now() 

        paid = Order.paid(created)
        self.assertEqual(paid.status, 2)
        self.assertEqual(paid.pay_method, created.pay_method)
        self.assertEqual(paid.paid_at, created.paid_at)
        self.assertIsInstance(paid.updated_at, datetime)

        paid.refunded_at = datetime.now() 
        refunded = Order.refund(paid)
        self.assertEqual(refunded.status, 3)
        self.assertEqual(refunded.refunded_at, paid.refunded_at)
        self.assertIsInstance(refunded.updated_at, datetime)

    def test_filter_by_profile(self):
        self.order.amount = int(self.order.amount) * 100
        created = Order.create(self.profile, self.item, self.order)
        orders = Order.filter_by_profile(self.profile, 1)
        self.assertEqual(len(orders), 1)
        self.assertEqual(orders[0].profile_id, self.profile.id)
        self.assertEqual(orders[0], created)


if __name__ == '__main__':
    unittest.main()
