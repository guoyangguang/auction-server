# -*- coding: utf-8 -*-

import unittest
from web import storage
from datetime import datetime
from ...model.user import User
from ...model.profile import Profile
from ...model.role import Role
from ...model.assignment import Assignment
from ..helper import truncate_db


class AssignmentTest(unittest.TestCase):

    def setUp(self):
        user = storage(
            email='user1@book.com',
            phone='18612345678',
            password_digest='123pw4'
        )
        self.user = User.signup(user)
        self.profile = Profile.create_default_profile(self.user)
        self.customer_service = Role.create(storage(name='customer service'))

    def tearDown(self):
        truncate_db()

    def test_get(self):
        Assignment.has_role(self.profile, self.customer_service)
        assignment = Assignment.get(self.profile, self.customer_service)
        self.assertEqual(assignment.profile_id, self.profile.id)
        self.assertEqual(assignment.role_id, self.customer_service.id)

    def test_roles_of(self):
        Assignment.has_role(self.profile, self.customer_service)
        seller = Role.create(storage(name='publisher'))
        Assignment.has_role(self.profile, seller)
        roles = Assignment.roles_of(self.profile)
        self.assertEqual(len(roles), 2)
        self.assertTrue(self.customer_service in roles)
        self.assertTrue(seller in roles)

    def test_is_role(self):
        admin = Role.create(storage(name='admin'))
        self.assertFalse(Assignment.is_role(self.profile, ['customer service']))
        assigned_customer_service= Assignment.has_role(
            self.profile, self.customer_service
        )
        self.assertTrue(Assignment.is_role(self.profile, ['customer service']))
        #test multiple roles
        assigned_admin = Assignment.has_role(self.profile, admin) 
        self.assertTrue(
            Assignment.is_role(self.profile, ['admin', 'customer service'])
        )
        Assignment.has_no_role(assigned_customer_service)
        self.assertTrue(
            Assignment.is_role(self.profile, ['admin', 'customer service'])
        )
        Assignment.has_no_role(assigned_admin)
        self.assertFalse(
            Assignment.is_role(self.profile, ['admin', 'customer service'])
        )

    def test_has_role(self):
        assignment = Assignment.has_role(self.profile, self.customer_service)
        self.assertEqual(assignment.profile_id, self.profile.id)
        self.assertEqual(assignment.role_id, self.customer_service.id)
        self.assertIsInstance(assignment.created_at, datetime)
        self.assertIsNone(assignment.deleted_at)

    def test_has_no_role(self):
        assignment = Assignment.has_role(self.profile, self.customer_service)
        self.assertTrue(Assignment.is_role(self.profile, ['customer service']))

        assignment = Assignment.has_no_role(assignment)
        self.assertFalse(Assignment.is_role(self.profile, ['customer service']))
        self.assertIsInstance(assignment.deleted_at, datetime)

if __name__ == '__main__':
    unittest.main()
