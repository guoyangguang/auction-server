# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ..helper import truncate_db, signup
from ...model.artist import Artist
from ...model.item import Item
from ...model.like import Like 
from ...model.role import Role
from ...model.assignment import Assignment


class LikeTest(unittest.TestCase):

    def setUp(self):
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)
        self.user1, self.profile1, self.account1 = signup(
            'user1@book.com', '18612345679'
        )
        self.user2, self.profile2, self.account2 = signup(
            'user2@book.com', '18612345680'
        )
        item = Storage(
            kind='1',
            name='厚德载物',
            description='君子的品德应如大地般厚实可以载养万物。' +
                        '旧指道德高尚者能承担重大任务',
            length='150',
            width='60',
            usage='客厅 办公室',
            produced_at='2017/11/10',
            starting_price='99',
            raising_price='10',
            deposit='8',
            starting_bid='2017/11/12/09:00',
            closing_bid='2017/11/13/09:00',
            img='hdzw.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.item = Item.create(self.artist, item)

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Like.table, 'likes')

    def test_per_page(self):
        self.assertEqual(Like.per_page, 15)

    def test_like(self):
        like = Like.like(self.profile1, self.item)
        self.assertEqual(like.profile_id, self.profile1.id)
        self.assertEqual(like.item_id, self.item.id)
        self.assertIsInstance(like.created_at, datetime)

    def test_get(self):
        created = Like.like(self.profile1, self.item)
        like = Like.get(self.profile1, self.item)
        self.assertEqual(like, created)

    def test_cancel_like(self):
        created = Like.like(self.profile1, self.item)
        canceled = Like.cancel_like(created)
        self.assertIsInstance(canceled.deleted_at, datetime)
        self.assertIsNone(Like.get(self.profile1, self.item))

    def test_item_liked_count(self):
        Like.like(self.profile1, self.item)
        Like.like(self.profile2, self.item)
        self.assertEqual(Like.item_liked_count(self.item), 2)


if __name__ == '__main__':
    unittest.main()
