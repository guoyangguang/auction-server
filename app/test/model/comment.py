# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ..helper import truncate_db, signup
from ...model.artist import Artist
from ...model.item import Item
from ...model.comment import Comment
from ...model.role import Role
from ...model.assignment import Assignment
from ...box import utils


class CommentTest(unittest.TestCase):

    def setUp(self):
        self.user, self.profile, self.account = signup()
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        artist = Artist.create(artist)
        item = Storage(
            kind='1',
            name='厚德载物',
            description='君子的品德应如大地般厚实可以载养万物。' +
                        '旧指道德高尚者能承担重大任务',
            length='150',
            width='60',
            usage='客厅 办公室',
            produced_at='2017/11/10',
            starting_price='99',
            raising_price='10',
            deposit='8',
            starting_bid='2017/11/12/09:00',
            closing_bid='2017/11/13/09:00',
            img='hdzw.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.item = Item.create(artist, item)
        self.comment = Storage(body='I like the item')

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Comment.table, 'comments')

    def test_per_page(self):
        self.assertEqual(Comment.per_page, 15)

    def test_body_validation(self): 
        self.assertTrue(
            Comment.comment_validator.validate(self.comment, results={})
        )
        errors = dict()
        for val in ['']:
            errors.clear()
            self.comment.body = val
            self.assertFalse(
                Comment.comment_validator.validate(
                    self.comment, results=errors
                )
            )
            self.assertTrue(
                utils.invalid_msgs['required'] in errors['body']
            )
        for val in ['I like the item'*10]:
            errors.clear()
            self.comment.body = val
            self.assertFalse(
                Comment.comment_validator.validate(
                    self.comment, results=errors
                )
            )
            self.assertTrue(
                  utils.invalid_msgs['length'].format(1, 100) in \
                  errors['body']
            )

    def test_comment_id_validation(self):
        created = Comment.create(
            profile=self.profile, item=self.item, new=self.comment
        )
        reply = Storage(body='thanks', comment_id=created.id)
        self.assertTrue(
            Comment.reply_validator.validate(reply, results={})
        )
        errors = dict()
        for val in ['-1', '0', '1,2', '']:
            errors.clear()
            reply.comment_id = val
            self.assertFalse(
                Comment.reply_validator.validate(reply, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['positive_int'] in errors['comment_id']
            )

    def test_create(self):
        created = Comment.create(
            profile=self.profile, item=self.item, new=self.comment
        )
        self.assertIsInstance(created.id, int)
        self.assertEqual(created.profile_id, self.profile.id)
        self.assertEqual(created.item_id, self.item.id)
        self.assertEqual(created.body, self.comment.body)
        self.assertIsNone(created.comment_id)
        self.assertIsInstance(created.created_at, datetime)

        reply = Storage(body='would you like to buy it?')
        created1 = Comment.create(
            profile=self.profile, item=self.item, new=reply, comment=created
        )
        self.assertIsInstance(created1.id, int)
        self.assertEqual(created1.profile_id, self.profile.id)
        self.assertEqual(created1.item_id, self.item.id)
        self.assertEqual(created1.body, reply.body)
        self.assertEqual(created1.comment_id, created.id)
        self.assertIsInstance(created1.created_at, datetime)

    def test_get(self):
        created = Comment.create(
            profile=self.profile, item=self.item, new=self.comment
        )
        comment = Comment.get(created.id)
        self.assertEqual(comment, created)

    def test_filter_by_item(self):
        created = Comment.create(
            profile=self.profile, item=self.item, new=self.comment
        )
        reply = Storage(body='would you like to buy it?')
        reply = Comment.create(
            profile=self.profile, item=self.item, new=reply, comment=created
        )
        comments = Comment.filter_by_item(self.item, 1)
        self.assertEqual(len(comments), 2)
        for comment in comments:
            self.assertEqual(comment.profiles_id, self.profile.id)
            self.assertEqual(comment.profile_id, self.profile.id)
            self.assertEqual(comment.item_id, self.item.id)
            if comment.comment_id:
                self.assertEqual(comment.id, reply.id)
            else:
                self.assertEqual(comment.id, created.id)


if __name__ == '__main__':
    unittest.main()
