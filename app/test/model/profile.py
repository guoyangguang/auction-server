# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ...model.profile import Profile 
from ...model.user import User 
from ...config import conf 
from ..helper import truncate_db
from ...box.utils import invalid_msgs
from ...box.uploader import Uploader


class ProfileTest(unittest.TestCase):

    def setUp(self):
        user = Storage(
            email='user1@ing.com',
            phone='18612345678',
            password_digest='123password4',
            password_confirmation='123password4'
        )
        self.user = User.signup(user)
        self.profile = Storage(
            name='user1',
            gender='1',
            location='china, shanghai',
            job='1',
            about='me...',
            img='user1.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
    
    def tearDown(self):
        truncate_db()
   
    def test_table(self):
        self.assertEqual(Profile.table, 'profiles') 

    def test_versions(self):
        self.assertEqual(Profile.versions, {'detail': 160, 'thumb': 50}) 

    def test_name_validation(self):
        self.assertTrue(Profile.validator.validate(self.profile, results=dict())) 
        for val in ['']:
            self.profile.name = val
            errors = dict()
            self.assertFalse(
                Profile.validator.validate(self.profile, results=errors)
            )
            self.assertTrue(invalid_msgs['required'] in errors['name'])
        self.profile.name = 'user1' * 7
        errors = dict()
        self.assertFalse(Profile.validator.validate(self.profile, results=errors))
        self.assertTrue(invalid_msgs['length'].format(1, 30) in errors['name'])

    def test_gender_validation(self):
        self.assertTrue(Profile.validator.validate(self.profile, results=dict()))
        for val in ['', 'male']:
            self.profile.gender = val 
            errors = dict()
            self.assertFalse(Profile.validator.validate(self.profile, results=errors))
            self.assertTrue(invalid_msgs['one_of'] in errors['gender'])

    def test_location_validations(self):
        self.assertTrue(Profile.validator.validate(self.profile, results=dict())) 
        for val in ['']:
            self.profile.location = val
            errors = dict()
            self.assertFalse(Profile.validator.validate(self.profile, results=errors))
            self.assertTrue(invalid_msgs['required'] in errors['location'])
        for val in ['@']:
            self.profile.location = val
            errors = dict()
            self.assertFalse(Profile.validator.validate(self.profile, results=errors))
            self.assertTrue(
                invalid_msgs['length'].format(2, 100) in errors['location'])

    def test_job_validations(self):
        self.assertTrue(Profile.validator.validate(self.profile, results=dict())) 
        for val in ['', 'programmer']:
            self.profile.job = val
            errors = dict()
            self.assertFalse(Profile.validator.validate(self.profile, results=errors))
            self.assertTrue(invalid_msgs['one_of'] in errors['job'])
    
    def test_get(self):
        created = Profile.create_default_profile(self.user)
        profile = Profile.get(created.id)
        self.assertEqual(profile, created)
    
    def test_filter_by_ids(self):
        profile = Profile.create_default_profile(self.user)
        profiles = Profile.filter_by_ids([profile.id])
        self.assertIsInstance(profiles, list)
        self.assertEqual(profiles[0], profile)
     
    def test_filter_by_user(self):
        created = Profile.create_default_profile(self.user)
        profile = Profile.filter_by_user(self.user)
        self.assertEqual(profile, created)
    
    def test_update(self):
        created = Profile.create_default_profile(self.user)
        self.assertIsNone(created.updated_at)
        new_profile = Storage(
            name='booklover',
            gender='1',
            location='china, beijing',
            job='1',
            about='i am a book lover'
        )
        profile = Profile.update(created, new_profile)
        self.assertEqual(profile.name, new_profile.name)
        self.assertEqual(profile.gender, int(new_profile.gender))
        self.assertIsInstance(profile.updated_at, datetime)
    
    def test_update_img(self):
        created = Profile.create_default_profile(self.user)
        img_name = 'me.jpg'
        img_meta=dict(
            dimension=[500, 666],
            ratio=1.3,
            size=1024,
            filetype='image/jpeg',
            vdimension={'detail': [375, 400], 'thumb': [235, 300]}
        ),
        profile = Profile.update_img(created, img_name, img_meta)
        self.assertEqual(profile.img, img_name)
        self.assertEqual(profile.img_meta, img_meta)
        self.assertIsInstance(profile.uploaded_at, datetime)
   
    def test_create_default_profile(self):
        created = Profile.create_default_profile(self.user)
        self.assertTrue(created.id)
        self.assertEqual(created.user_id, self.user.id)
        self.assertEqual(created.img, conf.DEFAULT_UPLOAD)
        self.assertIsInstance(created.created_at, datetime)


if __name__ == '__main__':
    unittest.main()
