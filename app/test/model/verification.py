# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ...box import utils
from ...model.verification import Verification
from ..helper import truncate_db


class VerificationTest(unittest.TestCase):

    def setUp(self):
        self.verification = Storage(
            phone='18612345678',
            verification_code=100000
        )

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Verification.table, 'verifications')

    def test_phone_validation(self):
        self.assertTrue(
            Verification.validator.validate(self.verification, results={})
        )
        for val in ['', '  ', 'phone']:
            self.verification.phone = val
            errors = dict()
            self.assertFalse(
                Verification.validator.validate(self.verification, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['phone'] in errors['phone']
            )

    def test_create(self):
        verification = Verification.create(self.verification)
        self.assertIsInstance(verification.id, int)
        self.assertEqual(verification.phone, self.verification.phone)
        self.assertIsInstance(verification.created_at, datetime)

    def test_get(self):
        created = Verification.create(self.verification)
        verification = Verification.get(created.id)
        self.assertEqual(verification, created)

    def test_update_code(self):
        created = Verification.create(self.verification)
        self.assertEqual(
            created.verification_code,
            self.verification.verification_code 
        )
        created.verification_code = 200000
        updated = Verification.update_code(created)
        self.assertEqual(updated.verification_code, 200000)
        self.assertIsInstance(updated.updated_at, datetime)

    def test_filter_by_phone(self):
        created = Verification.create(self.verification)
        verifications = Verification.filter_by_phone(created.phone)
        self.assertEqual(len(verifications), 1)
        self.assertEqual(verifications[0], created)


if __name__ == '__main__':
    unittest.main()
