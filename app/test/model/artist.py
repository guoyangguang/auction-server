#-*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ...model.artist import Artist
from ...box.utils import invalid_msgs
from ...config import conf
from ..helper import truncate_db, signup

class ArtistTest(unittest.TestCase):

    def setUp(self): 
        self.artist1 = Storage(
            name='user1',
            intro='user1 is a famous'*10,
            img='user1.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )

        self.artist2 = Storage(
            name='user2',
            intro='user2 is a famous'*10,
            img='user2.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Artist.table, 'artists')

    def test_versions(self):
        self.assertEqual(Artist.versions, {'detail': 270, 'thumb': 160})

    def test_name_validation(self):
        self.assertTrue(Artist.validator.validate(self.artist1, results={}))
        errors = {}
        self.artist1.name = None 
        errors.clear()
        self.assertFalse(
            Artist.validator.validate(self.artist1, results=errors)
        )
        self.assertTrue(invalid_msgs['required'] in errors['name'])
        self.artist1.name = 'user1'*7
        errors.clear()
        self.assertFalse(
            Artist.validator.validate(self.artist1, results=errors)
        )
        self.assertTrue(invalid_msgs['length'].format(1, 30) in errors['name'])

    def test_intro_validation(self):
        self.assertTrue(Artist.validator.validate(self.artist1, results={}))
        errors = {}
        self.artist1.intro = None 
        errors.clear()
        self.assertFalse(
            Artist.validator.validate(self.artist1, results=errors)
        )
        self.assertTrue(invalid_msgs['required'] in errors['intro'])
        self.artist1.intro = 'user1 is a famous'*2
        errors.clear()
        self.assertFalse(
            Artist.validator.validate(self.artist1, results=errors)
        )
        self.assertTrue(
            invalid_msgs['length'].format(100, 500) in errors['intro']
        )

    def test_create(self):
        created = Artist.create(self.artist1)
        self.assertIsInstance(created.id, int)
        self.assertIsInstance(created.created_at, datetime)
        self.assertEqual(created.name, self.artist1.name)
        self.assertIsInstance(created.uploaded_at, datetime)

    def test_update(self):  
        created = Artist.create(self.artist1)
        updated = Artist.update(created, self.artist2)
        self.assertEqual(updated.id, created.id)
        self.assertEqual(updated.name, self.artist2.name)
        self.assertIsInstance(updated.updated_at, datetime)
        self.assertIsInstance(updated.uploaded_at, datetime)

    def test_get(self):
        created = Artist.create(self.artist1)
        artist = Artist.get(created.id)
        self.assertIsInstance(artist, Storage)
        self.assertEqual(artist.id, created.id)
        self.assertEqual(artist.name, created.name)

    def test_all(self):
        artist1 = Artist.create(self.artist1)
        artist2 = Artist.create(self.artist2)
        artists = Artist.all(1)
        self.assertEqual(len(artists), 2)
        self.assertTrue(artist1 in artists)
        self.assertTrue(artist2 in artists)

    def test_filter_by_ids(self):
        created = Artist.create(self.artist1)
        artists = Artist.filter_by_ids([created.id])
        self.assertEqual(len(artists), 1)
        self.assertTrue(created in artists)


if __name__ == '__main__':
    unittest.main()
