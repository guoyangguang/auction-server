# -*- coding: utf-8 -*-
'''
user API
'''

import unittest
from web import Storage
from datetime import datetime
from ...model.account import Account
from ...model.user import User
from ...model.profile import Profile 
from ...box import utils
from ..helper import truncate_db, signup


class AccountTest(unittest.TestCase):
  
    def setUp(self):
        user = User.signup(
            Storage(
                email='user1@auction.com',
                phone='18612345679',
                password_digest='123pw4'
            )
        )
        self.profile = Profile.create_default_profile(user)
        self.account = Storage(profile_id=self.profile.id, balance=0)

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Account.table, 'accounts')

    def test_balance_validation(self):
        errors = dict()
        self.assertTrue(Account.validator.validate(self.account, results=errors))
        for val in ['', '-1', '8.8']:
            errors.clear()
            self.account.balance = val
            self.assertFalse(
                Account.validator.validate(self.account, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['positive_int_or_zero'] in errors['balance']
            )

    def test_profile_id_validation(self):
        created = Account.create(self.profile, self.account)
        errors = dict()
        self.assertFalse(Account.validator.validate(self.account, results=errors))
        self.assertTrue(
           utils.invalid_msgs['unique'].format('account')  in errors['profile_id']
        )
       
    def test_create(self): 
        created = Account.create(self.profile, self.account)
        self.assertIsInstance(created.id, int)
        self.assertEqual(created.balance, int(self.account.balance))
        self.assertEqual(created.profile_id, self.profile.id)
        self.assertIsInstance(created.created_at, datetime)

    def test_get(self): 
        created = Account.create(self.profile, self.account)
        account = Account.get(created.id)
        self.assertEqual(account, created)

    def test_filter_by_profile(self): 
        created = Account.create(self.profile, self.account)
        account = Account.filter_by_profile(self.profile)
        self.assertEqual(account, created)


if __name__ == '__main__':
    unittest.main()
