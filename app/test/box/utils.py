#-*- coding: utf-8 -*-

import unittest
from ...box.utils import gen_uuid

class UtilsTest(unittest.TestCase):
    
    def test_gen_uuid(self):
        ls = list() 
        for i in range(1, 100):
            uid = gen_uuid()
            ls.append(uid)
        for uid  in ls:
            occur = ls.count(uid)
            self.assertEqual(occur, 1)

if __name__ == '__main__':
    # unittest.main()
    from wheezy.validation import Validator, rules
    validator = Validator({
        'email': [rules.required]
    })
    for val in ['']:
        errors = dict()
        person = dict(email=val)
        print(validator.validate(person, results=errors), errors)
