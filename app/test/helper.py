# -*- coding: utf-8 -*-

'''
helper functions used in the tests
'''

from web import storage
from ..model.user import User
from ..model.profile import Profile
from ..model.account import Account
from ..config import conf
from ..box import utils

TRUNCATED_TABLES = ', '.join(utils.tables)


def truncate_db():
    '''
    truncate database
    '''
    conf.db.query('truncate ' + TRUNCATED_TABLES)


def signup(email=None, phone=None):
    '''
    signup a test user.
    '''
    user = storage(
        email=email or 'user@auction.com',
        phone=phone or '18612345678',
        password_digest='123pw4'
    )
    user = User.signup(user)
    profile = Profile.create_default_profile(user)
    account = Account.create(profile, storage(balance=0))
    return (user, profile, account)
