# -*- coding: utf-8 -*-
'''
running all the tests from the module
'''

import unittest

# config tests
# box tests
from .box.utils import UtilsTest
from .box.exception import ExceptionTest

# model tests
from .model.access_token import AccessTokenTest
from .model.account import AccountTest
# from .model.account_order import AccountOrderTest
# from .model.address import AddressTest
from .model.assignment import AssignmentTest
from .model.attorney import AttorneyTest
from .model.artist import ArtistTest
from .model.bid import BidTest
from .model.comment import CommentTest
from .model.followship import FollowshipTest
from .model.item import ItemTest
from .model.item_img import ItemImgTest
from .model.like import LikeTest
# from .model.message import MessageTest
from .model.order import OrderTest
from .model.profile import ProfileTest
from .model.role import RoleTest
from .model.user import UserTest
from .model.verification import VerificationTest
# from .model.tag import TagTest

# api tests
from .api.access_token import AccessTokenAPITest
# from .api.account import AccountAPITest
# from .api.account_order import AccountOrderAPITest
# from .api.api.address import AddressAPITest
from .api.artist import ArtistAPITest
from .api.bid import BidAPITest
from .api.assignment import AssignmentAPITest
from .api.attorney import AttorneyAPITest
from .api.comment import CommentAPITest
from .api.followship import FollowshipAPITest
from .api.item import ItemAPITest
from .api.item_img import ItemImgAPITest
from .api.like import LikeAPITest
# from .api.api.message import MessageAPITest
from .api.order import OrderAPITest
from .api.profile import ProfileAPITest
from .api.role import RoleAPITest
from .api.user import UserAPITest
from .api.verification import VerificationAPITest
# from .api.tag import TagAPITest


if __name__ == '__main__':
    unittest.main()
