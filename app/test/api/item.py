# -*- coding: utf-8 -*-

from datetime import datetime
import json
import unittest
from web import Storage
from ..helper import truncate_db, signup
from ...app import app
from ...box import utils
from ...config import conf 
from ...model.artist import Artist
from ...model.followship import Followship
from ...model.item import Item
from ...model.role import Role
from ...model.assignment import Assignment


class ItemAPITest(unittest.TestCase):

    def setUp(self):
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)
        self.item = Storage(
            kind='1',
            name='厚德载物',
            description='君子的品德应如大地般厚实可以载养万物。\
旧指道德高尚者能承担重大任务',
            length='150',
            width='60',
            usage='客厅 办公室',
            produced_at='2017/11/10',
            starting_price='99',
            raising_price='10',
            deposit='8',
            starting_bid='2017/11/12/09:00',
            closing_bid='2017/11/13/09:00',
            img='hdzw.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )

    def tearDown(self):
        truncate_db()

    def test_ArtistItemsAPI_GET_missed_data_page(self):
        response = app.request(
            '/api/artists/{0}/items'.format(self.artist.id),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"page is required." in response_data['msg'])

    def test_ArtistItemsAPI_GET_401(self):
        response = app.request(
            '/api/artists/{0}/items?page={1}'.format(self.artist.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(u"请登录.", response_data['msg'])

    def test_ArtistItemsAPI_GET_404_current_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/artists/{0}/items?page={1}'.format(self.artist.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(u"The profile is not found.", response_data['msg'])

    def test_ArtistItemsAPI_GET_404_artist(self):
        user, profile, account = signup()
        response = app.request(
            '/api/artists/{0}/items?page={1}'.format(self.artist.id + 1, 1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(u"找不到艺术家.", response_data['msg'])

    def test_ArtistItemsAPI_GET_403(self):
        user, profile, account = signup()
        response = app.request(
            '/api/artists/{0}/items?page={1}'.format(self.artist.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(u"Not Allowed.", response_data['msg'])

    def test_ArtistItemsAPI_GET_200(self):
        user, profile, account = signup()
        seller_role = Role.create(Storage(name='seller'))
        Assignment.has_role(profile, seller_role)
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/artists/{0}/items?page={1}'.format(self.artist.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 1)
        self.assertEqual(response_data[0]['img'], self.item.img)

    def test_ArtistItemsAPI_POST_missed_data_name(self):
        self.item.pop('name')
        response = app.request(
            '/api/artists/{0}/items'.format(self.artist.id),
            method='POST',
            data=json.dumps(self.item),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u'name' in response_data.get('msg'))

    def test_ArtistItemsAPI_POST_401(self):
        response = app.request(
            '/api/artists/{0}/items'.format(self.artist.id),
            method='POST',
            data=self.item
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data.get('msg'), u'请登录.')

    def test_ArtistItemsAPI_POST_404_current_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/artists/{0}/items'.format(self.artist.id),
            method='POST',
            data=self.item
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data.get('msg'), u'The profile is not found.')

    def test_ArtistItemsAPI_POST_403(self):
        user, profile, account = signup()
        response = app.request(
            '/api/artists/{0}/items'.format(self.artist.id),
            method='POST',
            data=self.item
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data.get('msg'), u'Not allowed.')

    def test_ArtistItemsAPI_POST_404_artist(self):
        user, profile, account = signup()
        seller_role = Role.create(Storage(name='seller'))
        Assignment.has_role(profile, seller_role)
        response = app.request(
            '/api/artists/{0}/items'.format(self.artist.id + 1),
            method='POST',
            data=self.item
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data.get('msg'), u'找不到艺术家.')

    def test_FollowingItemsAPI_GET_missed_data_page(self):
        user, profile, account = signup('user@auction.com', '18909512217')
        response = app.request(
            '/api/followings/{0}/items'.format(self.artist.id),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"page is required." in response_data['msg'])

    def test_FollowingItemsAPI_GET_401(self):
        user, profile, account = signup('user1@auction.com', '18909512219')
        response = app.request(
            '/api/followings/{0}/items?page={1}'.format(self.artist.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"请登录." in response_data['msg'])

    def test_FollowingItemsAPI_GET_404_current_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/followings/{0}/items?page={1}'.format(self.artist.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            u"The profile is not found." in response_data['msg']
        )

    def test_FollowingItemsAPI_GET_404_artist(self):
        user, profile, account = signup()
        response = app.request(
            '/api/followings/{0}/items?page={1}'.format(
                self.artist.id + 1, 1
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            u"找不到艺术家." in response_data['msg']
        )

    def test_FollowingItemsAPI_GET_400_not_following(self):
        user, profile, account = signup()
        response = app.request(
            '/api/followings/{0}/items?page={1}'.format(
                self.artist.id, 1
            ),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            u"未关注." in response_data['msg']
        )

    def test_FollowingItemsAPI_GET_200(self):
        user, profile, account = signup()
        Followship.follow(profile, self.artist)
        response = app.request(
            '/api/followings/{0}/items?page={1}'.format(
                self.artist.id, 1
            ),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 0)

    def test_ItemsSearchAPI_GET_missed_data_page(self):
        response = app.request(
            '/api/items/search?query={}'.format('mudanhuakai'),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"page is required." in response_data['msg'])

    def test_ItemsSearchAPI_GET_401(self):
        response = app.request(
            '/api/items/search?query={0}&page={1}'.format('mudanhuakai', 1),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"请登录." in response_data['msg'])

    def test_ItemsSearchAPI_GET_404_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/items/search?query={0}&page={1}'.format('mudanhuakai', 1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"The profile is not found." in response_data['msg'])

    def test_ItemsSearchAPI_GET_400_query(self):
        user, profile, account = signup()
        response = app.request(
            '/api/items/search?query={0}&page={1}'.format('  ', 1),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"请输入作品主题, 搜索." in response_data['msg'])

    def test_ItemsSearchAPI_GET_200(self):
        user, profile, account = signup()
        response = app.request(
            '/api/items/search?query={0}&page={1}'.format('mudanhuakai', 1),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 0)

    def test_ArtistItemShowAPI_GET_401(self):
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/artists/{0}/items/{1}'.format(self.artist.id, item.id),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(u"请登录.", response_data['msg'])

    def test_ArtistItemShowAPI_GET_404_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/artists/{0}/items/{1}'.format(self.artist.id, item.id),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(u"The profile is not found.", response_data['msg'])

    def test_ArtistItemShowAPI_GET_403_forbidden(self):
        user, profile, account = signup()
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/artists/{0}/items/{1}'.format(self.artist.id, item.id),
            method='GET'
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(u"Not Allowed.", response_data['msg'])

    def test_ArtistItemShowAPI_GET_404_artist(self):
        user, profile, account = signup()
        seller_role = Role.create(Storage(name='seller'))
        Assignment.has_role(profile, seller_role)
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/artists/{0}/items/{1}'.format(self.artist.id + 1, item.id),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(u"找不到艺术家.", response_data['msg'])

    def test_ArtistItemShowAPI_GET_404_item(self):
        user, profile, account = signup()
        seller_role = Role.create(Storage(name='seller'))
        Assignment.has_role(profile, seller_role)
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/artists/{0}/items/{1}'.format(self.artist.id, item.id + 1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(u"找不到作品.", response_data['msg'])

    def test_ArtistItemShowAPI_GET_400(self):
        user, profile, account = signup()
        seller_role = Role.create(Storage(name='seller'))
        Assignment.has_role(profile, seller_role)
        artist1 = Storage(
            name='artist1',
            intro='artist1 is a famous'*10,
            img='artist1.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        artist1 = Artist.create(artist1)
        item = Item.create(artist1, self.item)
        response = app.request(
            '/api/artists/{0}/items/{1}'.format(self.artist.id, item.id),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(u"作品不属于该艺术家.", response_data['msg'])

    def test_ArtistItemShowAPI_GET_200(self):
        user, profile, account = signup()
        seller_role = Role.create(Storage(name='seller'))
        Assignment.has_role(profile, seller_role)
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/artists/{0}/items/{1}'.format(self.artist.id, item.id),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['id'], item.id)
        self.assertEqual(len(response_data['itemimgs']), 0)

    def test_ItemsAPI_GET_missed_data_page(self):
        response = app.request(
            '/api/items',
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"page is required." in response_data['msg'])

    def test_ItemsAPI_GET_200(self):
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/items?page={0}'.format('1'),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 1)
        self.assertEqual(response_data[0]['length'], int(self.item.length))
        self.assertEqual(response_data[0]['width'], int(self.item.width))

    def test_ItemShowAPI_GET_401(self):
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/items/{0}'.format(item.id),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_ItemShowAPI_GET_404_item(self):
        user, profile, account = signup()
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/items/{0}'.format(item.id + 1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'The item is not found.'
        )

    def test_ItemShowAPI_GET_404_current_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/items/{0}'.format(item.id),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'The profile is not found.'
        )

    def test_ItemShowAPI_GET_200(self):
        user, profile, account = signup()
        item = Item.create(self.artist, self.item)
        response = app.request(
            '/api/items/{0}'.format(item.id),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        for key in [
            'item', 'artist', 'is_followed', 'is_liked', 'like_count',
            'comments', 'itemimgs'
        ]:
            self.assertTrue(key in response_data)


if __name__ == '__main__':
    unittest.main()
