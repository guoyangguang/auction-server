# -*- coding: utf-8 -*-

from datetime import datetime
import json
import unittest
from web import Storage
from ...app import app
from ...model.role import Role
from ...model.assignment import Assignment
from ...model.artist import Artist
from ...model.bid import Bid
from ...model.item import Item
from ...config import conf
from ...box import utils
from ..helper import truncate_db, signup 

class OrderAPITest(unittest.TestCase):

    def setUp(self):
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        artist = Artist.create(artist)
        item = Storage(
            kind='1',
            name='厚德载物',
            description='君子的品德应如大地般厚实可以载养万物。\
旧指道德高尚者能承担重大任务',
            length='150',
            width='60',
            usage='客厅 办公室',
            produced_at='2017/11/10',
            starting_price='99',
            raising_price='10',
            deposit='8',
            starting_bid='2018/07/04/09:00',
            closing_bid='2018/07/05/09:00',
            img='hdzw.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.item = Item.create(artist, item)

    def tearDown(self):
        truncate_db()


    def test_ItemOrderAPI_401(self):
        response = app.request(
            '/api/items/{0}/order'.format(self.item.id),
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_ItemOrderAPI_404_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=profile.id)
        )
        response = app.request(
            '/api/items/{0}/order'.format(self.item.id),
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'The profile is not found.'
        )

    def test_ItemOrderAPI_404_item(self):
        user, profile, account = signup()
        response = app.request(
            '/api/items/{0}/order'.format(self.item.id + 1),
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'The item is not found.'
        )

    def test_ItemOrderAPI_404_no_bids_for_item(self):
        user, profile, account = signup()
        response = app.request(
            '/api/items/{0}/order'.format(self.item.id),
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'No bids for the item.'
        )

    def test_ItemOrderAPI_403_pay_by_highest_bidder(self):
        user1, profile1, account1 = signup('user1@auction.com', '18612345679')
        Bid.create(self.item, profile1, Storage(price=109*100, status=1))
        user, profile, account = signup()
        Bid.create(self.item, profile, Storage(price=99*100, status=1))
        response = app.request(
            '/api/items/{0}/order'.format(self.item.id),
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'),
            u'The item should be paid by the highest bidder.'
        )

    def test_ItemOrderAPI_400_order(self):
        user1, profile1, account1 = signup('user1@auction.com', '18612345679')
        Bid.create(self.item, profile1, Storage(price=-109*100, status=1))
        user, profile, account = signup()
        Bid.create(self.item, profile, Storage(price=-99*100, status=1))
        response = app.request(
            '/api/items/{0}/order'.format(self.item.id),
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            utils.invalid_msgs['positive_int'] in response_data.get('msg')[0]
        )

    def test_ItemOrderAPI_201(self):
        user1, profile1, account1 = signup('user1@auction.com', '18612345679')
        Bid.create(self.item, profile1, Storage(price=99*100, status=1))
        user, profile, account = signup()
        Bid.create(self.item, profile, Storage(price=109*100, status=1))
        response = app.request(
            '/api/items/{0}/order'.format(self.item.id),
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertIsInstance(response_data['id'], int)
        self.assertEqual(response_data['item_id'], self.item.id)
        self.assertEqual(response_data['profile_id'], profile.id)
        self.assertEqual(response_data['amount'], 10900)
        self.assertEqual(response_data['status'], 1)



if __name__ == '__main__':
    unittest.main()
