# -*- coding: utf-8 -*-

import unittest
import json
from web import Storage
from datetime import datetime
from ..helper import truncate_db, signup
from ...app import app
from ...model.artist import Artist
from ...model.item import Item
from ...model.item_img import ItemImg
from ...model.role import Role
from ...model.assignment import Assignment 
from ...config import conf 


class ItemImgAPITest(unittest.TestCase):

    def setUp(self):
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)
        item = Storage(
            kind='1',
            name='厚德载物',
            description='君子的品德应如大地般厚实可以载养万物。\
旧指道德高尚者能承担重大任务',
            length='150',
            width='60',
            usage='客厅 办公室',
            produced_at='2017/11/10',
            starting_price='99',
            raising_price='10',
            deposit='8',
            starting_bid='2017/11/12/09:00',
            closing_bid='2017/11/13/09:00',
            img='hdzw.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg'
            )
        )
        self.item = Item.create(self.artist, item)
        self.itemimg = Storage(
            description='detail',
            img='detail.jpg',
            img_meta=dict(
                size=1024 * 2,
                filetype='image/jpeg'
            )
        )

    def tearDown(self):
        truncate_db()

    def test_ArtistItemItemImgsAPI_POST_missed_data_description(self):
        self.itemimg.pop('description')
        response = app.request(
            '/api/artist/items/{0}/itemimgs'.format(self.item.id),
            method='POST',
            data=self.itemimg
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'),
            u'description, img is required.'
        )

    def test_ArtistItemItemImgsAPI_POST_401(self):
        response = app.request(
            '/api/artist/items/{0}/itemimgs'.format(self.item.id),
            method='POST',
            data=self.itemimg
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_ArtistItemItemImgsAPI_POST_404_current_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/artist/items/{0}/itemimgs'.format(self.item.id),
            method='POST',
            data=self.itemimg
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'The profile is not found.')

    def test_ArtistItemItemImgsAPI_POST_404_item(self):
        user, profile, account = signup()
        response = app.request(
            '/api/artist/items/{0}/itemimgs'.format(self.item.id + 1),
            method='POST',
            data=self.itemimg
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'The item is not found.')

    def test_ArtistItemItemImgsAPI_POST_403_forbidden(self):
        user, profile, account = signup()
        response = app.request(
            '/api/artist/items/{0}/itemimgs'.format(self.item.id),
            method='POST',
            data=self.itemimg
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'Not allowed.')


if __name__ == '__main__':
    unittest.main()
