# -*- coding: utf-8 -*-

import unittest
import json
from datetime import datetime
from web import Storage
from ..helper import truncate_db, signup
from ...app import app
from ...model.artist import Artist
from ...model.attorney import Attorney
from ...model.role import Role
from ...model.assignment import Assignment 
from ...config import conf


class AttorneyAPITest(unittest.TestCase):

    def setUp(self):
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)
        self.attorney = Storage(
            img='detail.jpg',
            img_meta=dict(
                size=1024 * 2,
                filetype='image/jpeg'
            )
        )

    def tearDown(self):
        truncate_db()

    def test_test_ArtistAttorneysAPI_POST_400_missed_data(self):
        response = app.request(
            '/api/artists/{0}/attorneys'.format(self.artist.id),
            method='POST',
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'img is required.')

    def test_test_ArtistAttorneysAPI_POST_401(self):
        response = app.request(
            '/api/artists/{0}/attorneys'.format(self.artist.id),
            method='POST',
            data=self.attorney
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_test_ArtistAttorneysAPI_POST_404_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/artists/{0}/attorneys'.format(self.artist.id),
            method='POST',
            data=self.attorney
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'The profile is not found.'
        )

    def test_test_ArtistAttorneysAPI_POST_404_artist(self):
        user, profile, account = signup()
        response = app.request(
            '/api/artists/{0}/attorneys'.format(self.artist.id + 1),
            method='POST',
            data=self.attorney
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'The artist is not found.'
        )
    def test_test_ArtistAttorneysAPI_POST_403(self):
        user, profile, account = signup()
        response = app.request(
            '/api/artists/{0}/attorneys'.format(self.artist.id),
            method='POST',
            data=self.attorney
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'Not allowed.'
        )


if __name__ == '__main__':
    unittest.main()
