# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
import json
from ...app import app
from ...model.artist import Artist
from ...model.item import Item
from ...model.comment import Comment
from ...model.role import Role
from ...model.assignment import Assignment 
from ...box import utils 
from ...config import conf
from ..helper import truncate_db, signup


class CommentAPITest(unittest.TestCase):

    def setUp(self):
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)
        item = Storage(
            kind='1',
            name='厚德载物',
            description='君子的品德应如大地般厚实可以载养万物。' +
                        '旧指道德高尚者能承担重大任务',
            length='150',
            width='60',
            usage='客厅 办公室',
            produced_at='2017/11/10',
            starting_price='99',
            raising_price='10',
            deposit='8',
            starting_bid='2017/11/12/09:00',
            closing_bid='2017/11/13/09:00',
            img='hdzw.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.item = Item.create(self.artist, item)
        self.comment = Storage(body='it is nice')

    def tearDown(self):
        truncate_db()

    def test_ItemCommentsAPI_POST_missed_data(self):
        response = app.request(
            '/api/items/{0}/comments'.format(self.item.id),
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'),
            u'body, comment_id is required.'
        )

    def test_ItemCommentsAPI_POST_401_Unauthorized(self):
        response = app.request(
            '/api/items/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps({'body': self.comment.body}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'),
            u'请登录.'
        )

    def test_ItemCommentsAPI_POST_404_item_not_found(self):
        user, profile, account = signup()
        conf.db.update(
            'items',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': self.item.id}
        )
        response = app.request(
            '/api/items/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps({'body': self.comment.body}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data.get('msg'), u'The item is not found.')

    def test_ItemCommentsAPI_POST_404_profile_not_found(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=profile.id)
        )
        response = app.request(
            '/api/items/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps({'body': self.comment.body}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'The profile is not found.'
        )

    def test_ItemCommentsAPI_POST_400_invalid_comment(self):
        user, profile, account = signup()
        response = app.request(
            '/api/items/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps({'body': ''}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            utils.invalid_msgs['required'].lower() in \
            response_data.get('msg')[0]
        )

    def test_ItemCommentsAPI_POST_400_invalid_reply(self):
        user, profile, account = signup()
        data = dict(
            body=self.comment.body,
            comment_id='-1'
        )
        response = app.request(
            '/api/items/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            utils.invalid_msgs['positive_int'].lower() in \
            response_data.get('msg')[0]
        )

    def test_ItemCommentsAPI_POST_404_comment_not_found(self):
        user, profile, account = signup()
        profile_comment = Comment.create(
            profile, self.item, Storage(body='any one?')
        )
        data = dict(
            body=self.comment.body,
            comment_id=profile_comment.id + 1
        )
        response = app.request(
            '/api/items/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            response_data.get('msg'), 'The comment is not found.'
        )

    def test_ItemCommentsAPI_POST_comment(self):
        user, profile, account = signup()
        data = dict(
            body=self.comment.body
        )
        response = app.request(
            '/api/items/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data['profile']['name'], profile.name
        )
        self.assertEqual(response_data['body'], self.comment.body)
        self.assertTrue(response_data['created_at'])

    def test_ItemCommentsAPI_POST_reply(self):
        user, profile, account = signup()
        profile_comment = Comment.create(
            profile, self.item, Storage(body='any one?')
        )
        data = dict(
            body=self.comment.body,
            comment_id=profile_comment.id
        )
        response = app.request(
            '/api/items/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data['profile']['name'], profile.name
        )
        self.assertEqual(response_data['body'], self.comment.body)
        self.assertTrue(response_data['created_at'])
        self.assertEqual(
            response_data['comment']['id'], profile_comment.id
        )


if __name__ == '__main__':
    unittest.main()
