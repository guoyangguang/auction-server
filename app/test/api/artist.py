# -*- coding: utf-8 -*-

import unittest
from web import Storage
import json
from datetime import datetime
from ...model.role import Role
from ...model.assignment import Assignment
from ...model.artist import Artist
from ..helper import truncate_db, signup
from ...box.utils import invalid_msgs
from ...box.uploader import Uploader
from ...config import conf
from ...app import app


class ArtistAPITest(unittest.TestCase):

    def setUp(self):
        self.artist = Storage(
            name='user1',
            intro='user1 is a famous'*10,
            img='user1.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )

    def tearDown(self):
        truncate_db()

    def test_ArtistsAPI_GET_400_missed_data_page(self):
        response = app.request(
            '/api/artists',
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'page is required.')

    def test_ArtistsAPI_GET_401_unauthorized(self):
        response = app.request(
            '/api/artists?page={0}'.format(1),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_ArtistsAPI_GET_404_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(), 
            where='id=$_id',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/artists?page={0}'.format(1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'The profile is not found.')

    def test_ArtistsAPI_GET_403(self):
        user, profile, account = signup()
        response = app.request(
            '/api/artists?page={0}'.format(1),
            method='GET'
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'Not allowed.')

    def test_ArtistsAPI_GET_200(self):
        created = Artist.create(self.artist)
        user, profile, account = signup()
        role = Role.create(Storage(name='seller'))
        Assignment.has_role(profile, role)
        response = app.request(
            '/api/artists?page={0}'.format(1),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data[0]['id'], created.id)

    def test_ArtistsAPI_POST_400_missed_data(self):
        response = app.request(
            '/api/artists',
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'),
            'name, intro, img is required.'
        )

    def test_ArtistAPI_PUT_400_missed_data(self):
        created = Artist.create(self.artist)
        response = app.request(
            '/api/artists/{0}'.format(created.id),
            method='PUT',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'),
            'name, intro, img is required.'
        )


if __name__ == '__main__':
    unittest.main()
