# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
import json
import unittest
from web import Storage
from ...config import conf 
from ...box import utils
from ..helper import truncate_db, signup
from ...app import app
from ...model.artist import Artist
from ...model.bid import Bid
from ...model.item import Item
from ...model.role import Role
from ...model.assignment import Assignment


class BidAPITest(unittest.TestCase):

    def setUp(self):
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)
        item = Storage(
            kind='1',
            name='厚德载物',
            description='君子的品德应如大地般厚实可以载养万物。\
旧指道德高尚者能承担重大任务',
            length='150',
            width='60',
            usage='客厅 办公室',
            produced_at='2017/11/10',
            starting_price='99',
            raising_price='10',
            deposit='8',
            starting_bid=datetime.now().strftime('%Y/%m/%d/%H:%M'),
            closing_bid=(datetime.now() + timedelta(1)).strftime('%Y/%m/%d/%H:%M'),
            img='hdzw.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.item = Item.create(self.artist, item)

    def tearDown(self):
        truncate_db()

    def test_ItemBidsAPI_GET_missed_data_page(self):
        response = app.request(
            '/api/items/{0}/bids'.format(self.item.id),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"page is required." in response_data['msg'])

    def test_ItemBidsAPI_GET_401(self):
        response = app.request(
            '/api/items/{0}/bids?page={1}'.format(self.item.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"请登录." in response_data['msg'])

    def test_ItemBidsAPI_GET_404_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/items/{0}/bids?page={1}'.format(self.item.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            u"The profile is not found." in response_data['msg']
        )

    def test_ItemBidsAPI_GET_404_item(self):
        user, profile, account = signup()
        item_id = self.item.id
        conf.db.update(
            'items',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': item_id}
        )
        response = app.request(
            '/api/items/{0}/bids?page={1}'.format(item_id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            u"The item is not found." in response_data['msg']
        )

    def test_ItemBidsAPI_GET_200(self):
        user, profile, account = signup()
        response = app.request(
            '/api/items/{0}/bids?page={1}'.format(self.item.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 0)

    def test_ItemBidsAPI_POST_missed_data_price(self):
        response = app.request(
            '/api/items/{0}/bids'.format(self.item.id),
            method='POST'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"price is required." in response_data['msg'])

    def test_ItemBidsAPI_POST_401(self):
        response = app.request(
            '/api/items/{0}/bids'.format(self.item.id),
            method='POST',
            data=json.dumps({'price': 98}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"请登录." in response_data['msg'])

    def test_ItemBidsAPI_POST_404_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/items/{0}/bids'.format(self.item.id),
            method='POST',
            data=json.dumps({'price': 98}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"The profile is not found." in response_data['msg'])

    def test_ItemBidsAPI_POST_404_item(self):
        user, profile, account = signup()
        item_id = self.item.id
        conf.db.update(
            'items',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': item_id}
        )
        response = app.request(
            '/api/items/{0}/bids'.format(item_id),
            method='POST',
            data=json.dumps({'price': 98}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"The item is not found." in response_data['msg'])

    def test_ItemBidsAPI_POST_400(self):
        user, profile, account = signup()
        response = app.request(
            '/api/items/{0}/bids'.format(self.item.id),
            method='POST',
            data=json.dumps({'price': -98}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            utils.invalid_msgs['positive_int'] in response_data['msg'][0]
        )

    def test_ItemBidsAPI_POST_403(self):
        user, profile, account = signup()
        response = app.request(
            '/api/items/{0}/bids'.format(self.item.id),
            method='POST',
            data=json.dumps({'price': 69}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            u"出价应等于或高于起价." in response_data['msg']
        )

    def test_ItemBidsAPI_POST_200(self):
        user, profile, account = signup()
        prices = [119, 99, 109, 139, 129, 309, 139, 149, 189, 159, 169]
        for price in prices:
            response = app.request(
                '/api/items/{0}/bids'.format(self.item.id),
                method='POST',
                data=json.dumps({'price': price}),
                headers={'Content-Type': 'application/json'}
            )
            self.assertEqual(response.status, '200 OK')
            self.assertEqual(
                response.headers['Content-Type'], 'application/json'
            )
            response_data = json.loads(response.data, encoding='utf-8')
            self.assertTrue(response_data['price'], price)
        item_max_bid = Bid.filter_max_bid_by_item(self.item)
        self.assertEqual(item_max_bid.price, 309*100) 
        self.assertEqual(item_max_bid.status, 2) 

        item_bids = Bid.filter_by_item(self.item, 1)
        self.assertEqual(len(item_bids), 11)
        item_bids = sorted(item_bids, key=lambda bid: bid.price, reverse=True)
        pop_item = item_bids.pop(0)
        self.assertEqual(pop_item.status, 2)
        self.assertEqual(pop_item.price, 309*100)
        self.assertEqual(len(item_bids), 10)
        for bid in item_bids:
            self.assertEqual(bid.status, 1) 


if __name__ == '__main__':
    unittest.main()
