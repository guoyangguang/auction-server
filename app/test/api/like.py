# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
import json
from ...app import app
from ...config import conf
from ...box import utils
from ...model.artist import Artist
from ...model.item import Item
from ...model.like import Like
from ...model.role import Role
from ...model.assignment import Assignment 
from ..helper import truncate_db, signup
 

class LikeAPITest(unittest.TestCase):
    
    def setUp(self):
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)
        item = Storage(
            kind='1',
            name='厚德载物',
            description='君子的品德应如大地般厚实可以载养万物。\
旧指道德高尚者能承担重大任务',
            length='150',
            width='60',
            usage='客厅 办公室',
            produced_at='2017/11/10',
            starting_price='99',
            raising_price='10',
            deposit='8',
            starting_bid='2017/11/12/09:00',
            closing_bid='2017/11/13/09:00',
            img='hdzw.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.item = Item.create(self.artist, item)

    def tearDown(self):
        truncate_db()
    
    def test_LikeMineAPI_POST_missed_data_id(self):
        response = app.request(
            '/api/likes/mine',
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"id is required."
        )

    def test_LikeMineAPI_POST_401_unauthorized(self):
        data = dict(
            id=self.item.id
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"请登录."
        )

    def test_LikeMineAPI_POST_404_item_not_found(self):
        user, profile, account = signup()
        data = dict(
            id=self.item.id + 1
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The item is not found."
        )

    def test_LikeMineAPI_POST_404_profile_not_found(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=profile.id)
        )
        data = dict(
            id=self.item.id
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The profile is not found."
        )

    def test_LikeMineAPI_POST_400_item_already_liked(self):
        user, profile, account = signup()
        Like.like(profile, self.item)
        data = dict(
            id=self.item.id
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"You have already liked the item."
        )

    def test_LikeMineAPI_POST_201(self):
        user, profile, account = signup()
        data = dict(
            id=self.item.id
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data['created_at'])
        self.assertIsNone(response_data['deleted_at'])
        self.assertEqual(response_data['item_liked_count'], 1)

    def test_LikeMineAPI_PUT_401_Unauthorized(self):
        response = app.request(
            '/api/likes/mine/{0}'.format(self.item.id),
            method='PUT'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"请登录."
        )

    def test_LikeMineAPI_PUT_404_item_not_found(self):
        user, profile, account = signup()
        Like.like(profile, self.item)
        response = app.request(
            '/api/likes/mine/{0}'.format(self.item.id + 1),
            method='PUT',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The item is not found."
        )

    def test_LikeMineAPI_PUT_404_profile_not_found(self):
        user, profile, account = signup()
        Like.like(profile, self.item)
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=profile.id)
        )
        response = app.request(
            '/api/likes/mine/{0}'.format(self.item.id),
            method='PUT',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The profile is not found."
        )

    def test_LikeMineAPI_PUT_404_like_not_found(self):
        user, profile, account = signup()
        response = app.request(
            '/api/likes/mine/{0}'.format(self.item.id),
            method='PUT',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"You did not ever liked the item."
        )

    def test_LikeMineAPI_PUT(self):
        user, profile, account = signup()
        Like.like(profile, self.item)
        self.assertEqual(Like.item_liked_count(self.item), 1)
        response = app.request(
            '/api/likes/mine/{0}'.format(self.item.id),
            method='PUT',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data['deleted_at'])
        self.assertIsNone(response_data['updated_at'])
        self.assertEqual(response_data['item_liked_count'], 0)


if __name__ == '__main__':
    unittest.main()
