# -*- coding: utf-8 -*-

import unittest
import json
import urllib
import os
from datetime import datetime
from ..helper import truncate_db, signup
from ...box.utils import invalid_msgs
from ...box.uploader import Uploader
from ...config.conf import db
from ...model.user import User
from ...app import app


class ProfileAPITest(unittest.TestCase):

    def setUp(self):
        self.user, self.profile, self.account = signup()
        self.data = dict(
            name='booklover',
            gender='1',
            location='china, beijing',
            job='1',
            about='i am a book lover'
        )

    def tearDown(self):
        truncate_db()

    def test_ProfileAPI_GET(self):
        response = app.request(
            '/api/profile',
            method='GET'
        )
        user = User.filter_by_email('user@auction.com')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        profile = json.loads(response.data, encoding='utf-8')
        for key in [
            'id', 'user_id', 'name', 'gender', 'location',
            'job', 'img', 'about', 'uploaded_at', 'created_at',
            'updated_at', 'deleted_at'
        ]:
            self.assertTrue(key in profile.keys())
    
    def test_ProfileAPI_GET_unauthorized(self):
        db.update(
            'users', deleted_at=datetime.now(),
            where='email=$_email', vars=dict(_email=self.user.email)
        )
        response = app.request(
            '/api/profile',
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_ProfileAPI_GET_not_found(self):
        db.update(
            'profiles', deleted_at=datetime.now(),
            where='id=$_id', vars=dict(_id=self.profile.id)
        )
        response = app.request(
            '/api/profile',
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'),
            u'The profile is not found.'
        )

    def test_ProfileAPI_PUT(self):
        json_data = json.dumps(self.data)
        response = app.request(
            '/api/profile',
            method='PUT',
            data=json_data,
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        profile = json.loads(response.data, encoding='utf-8')
        self.assertEqual(profile['gender'], int(self.data['gender']))
        self.assertEqual(profile['job'], int(self.data['job']))
        self.assertEqual(
            profile.get('detail_file'),
            Uploader.default_filepath('detail')
        )
        self.assertEqual(
            profile.get('thumb_file'),
            Uploader.default_filepath('thumb')
        )

    def test_ProfileAPI_PUT_missed_data(self):
        self.data.pop('name')
        response = app.request(
            '/api/profile',
            method='PUT',
            data=json.dumps(self.data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        msg = u'name is required.'
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), msg)

    def test_ProfileAPI_PUT_unauthorized(self):
        db.update(
            'users', deleted_at=datetime.now(),
            where='email=$_email', vars=dict(_email=self.user.email)
        )
        response = app.request(
            '/api/profile',
            method='PUT',
            data=json.dumps(self.data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_ProfileAPI_PUT_bad_request(self):
        self.data['gender'] = 'male'
        response = app.request(
            '/api/profile',
            method='PUT',
            data=json.dumps(self.data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        msgs = response_data.get('msg')
        self.assertEqual(len(msgs), 1)
        self.assertTrue(
            invalid_msgs.get('one_of').lower() in msgs[0]
        )

    def test_ProfileAPI_PUT_not_found(self):
        db.update(
            'profiles', deleted_at=datetime.now(),
            where='id=$_id', vars=dict(_id=self.profile.id)
        )
        response = app.request(
            '/api/profile',
            method='PUT',
            data=json.dumps(self.data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        msg = u'The profile is not found.'
        self.assertEqual(response_data.get('msg'), msg)

    def test_ProfileFileAPI_PUT_missed_data(self):
        response = app.request(
            '/api/profile/file',
            method='PUT'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        msg = u"profile_upload is required."
        self.assertEqual(response_data.get('msg'), msg)

    #@unittest.skip('fix test later')
    #def test_ProfileFileAPI_PUT_unauthorized(self):
    #    request_data = urllib.urlencode(dict(
    #        access_token=gen_uuid(),
    #        upload=''
    #    ))
    #    response = app.request('/api/profile/file', method='PUT', data=request_data)
    #    self.assertEqual(response.status, '401 Unauthorized')
    #    self.assertEqual(response.headers['Content-Type'], 'application/json')
    #    response_data = json.loads(response.data, encoding='utf-8')
    #    msg = u'请登录.'
    #    self.assertEqual(response_data.get('msg'), msg)

    #@unittest.skip('fix test later')
    #def test_ProfileFileAPI_PUT_not_found(self):
    #    db.update('profiles', deleted_at=datetime.now(),
    #        where='id=$_id', vars=dict(_id=self.profile.id))
    #    request_data = urllib.urlencode(dict(
    #        access_token=self.user.access_token,
    #        upload=''
    #    ))
    #    response = app.request('/api/profile/file', method='PUT', data=request_data)
    #    self.assertEqual(response.status, '404 Not Found')
    #    self.assertEqual(response.headers['Content-Type'], 'application/json')
    #    response_data = json.loads(response.data, encoding='utf-8')
    #    msg = u'The profile is not found.'
    #    self.assertEqual(response_data.get('msg'), msg)
    #
    #@unittest.skip('fix test later')
    #def test_ProfileFileAPI_PUT_bad_request(self):
    #    request_data = dict(
    #        access_token=self.user.access_token,
    #        upload=''
    #    )
    #    response = app.request('/api/profile/file', method='PUT', data=request_data)
    #    self.assertEqual(response.status, '400 Bad Request')
    #    self.assertEqual(response.headers['Content-Type'], 'application/json')
    #    response_data = json.loads(response.data, encoding='utf-8')
    #    msg = u'The png or jpg or gif file is required.'
    #    self.assertEqual(response_data.get('msg'), msg)

if __name__ == '__main__':
    unittest.main()
