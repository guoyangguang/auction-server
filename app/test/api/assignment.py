# -*-coding: utf-8-*-

'''
AssignmentAPITest module
'''

from datetime import datetime
import unittest
import json
from web import Storage
from ...model.role import Role
from ...model.assignment import Assignment
from ...box.utils import invalid_msgs
from ..helper import truncate_db, signup
from ...app import app
from ...config.conf import db


class AssignmentAPITest(unittest.TestCase):
    '''
    AssignmentAPITest class
    '''

    def setUp(self):
        self.admin_role = Role.create(Storage(name='admin'))
        self.user, self.profile, self.account = signup(
            'user1@auction.com', '18612345679'
        )

    def tearDown(self):
        truncate_db()

    def test_AssignmentsAPI_POST_missing_data(self):
        response = app.request(
            '/api/assignments',
            method='POST',
            data=json.dumps({'role_name': 'seller'}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'user_id is required.'
        )

    def test_AssignmentsAPI_POST_401(self):
        response = app.request(
            '/api/assignments',
            method='POST',
            data=json.dumps(
                {'user_id': self.user.id, 'role_name': 'seller'}
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'请登录.'
        )
 
    def test_AssignmentsAPI_POST_404_current_profile(self):
        current_user, current_profile, current_account = signup()
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': current_profile.id}
        )
        response = app.request(
            '/api/assignments',
            method='POST',
            data=json.dumps(
                {'user_id': self.user.id, 'role_name': 'seller'}
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'),
            u"The current user's profile is not found."
        )

    def test_AssignmentsAPI_POST_403_forbidden(self):
        current_user, current_profile, current_account = signup()
        response = app.request(
            '/api/assignments',
            method='POST',
            data=json.dumps(
                {'user_id': self.user.id, 'role_name': 'seller'}
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'Not allowed.'
        )

    def test_AssignmentsAPI_POST_404_user(self):
        current_user, current_profile, current_account = signup()
        Assignment.has_role(current_profile, self.admin_role)
        user_id = self.user.id
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': self.profile.id}
        )
        db.update(
            'accounts',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': self.account.id}
        )
        db.update(
            'users',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': self.user.id}
        )
        response = app.request(
            '/api/assignments',
            method='POST',
            data=json.dumps(
                {'user_id': user_id, 'role_name': 'seller'}
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'),
            u"The user is not found."
        )

    def test_AssignmentsAPI_POST_404_profile(self):
        current_user, current_profile, current_account = signup()
        Assignment.has_role(current_profile, self.admin_role)
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': self.profile.id}
        )
        response = app.request(
            '/api/assignments',
            method='POST',
            data=json.dumps(
                {'user_id': self.user.id, 'role_name': 'seller'}
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'),
            u"The profile is not found."
        )

    def test_AssignmentsAPI_POST_404_role(self):
        current_user, current_profile, current_account = signup()
        Assignment.has_role(current_profile, self.admin_role)
        response = app.request(
            '/api/assignments',
            method='POST',
            data=json.dumps(
                {'user_id': self.user.id, 'role_name': 'seller'}
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'),
            u"The role is not found."
        )

    def test_AssignmentsAPI_POST_201(self):
        current_user, current_profile, current_account = signup()
        Assignment.has_role(current_profile, self.admin_role)
        seller_role = Role.create(Storage(name='seller'))
        response = app.request(
            '/api/assignments',
            method='POST',
            data=json.dumps(
                {'user_id': self.user.id, 'role_name': 'seller'}
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        for key in ['profile_id', 'role_id', 'created_at']:
            self.assertTrue(
                key in response_data
            )


if __name__ == '__main__':
    unittest.main()
