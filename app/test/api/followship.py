# -*- coding: utf-8 -*-

import unittest
from web import Storage
import json
from datetime import datetime
from ..helper import truncate_db, signup
from ...app import app
from ...model.artist import Artist
from ...model.followship import Followship
from ...model.role import Role
from ...model.assignment import Assignment
from ...config import conf


class FollowshipAPITest(unittest.TestCase):

    def setUp(self):
        artist = Storage(
            name='artist',
            intro='artist is a famous'*10,
            img='artist.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            )
        )
        self.artist = Artist.create(artist)

    def tearDown(self):
        truncate_db()
 
    def test_MyFollowingsAPI_GET_unauthorized(self):
        response = app.request(
            '/api/followings/mine?page={0}'.format(1),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u"请登录.")

    def test_MyFollowingsAPI_GET_404_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id and deleted_at is null',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/followings/mine?page={0}'.format(1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'),
            'The profile is not found.'
        )

    def test_MyFollowingsAPI_GET(self):
        user, profile, account = signup()
        Followship.follow(profile, self.artist)
        response = app.request(
            '/api/followings/mine?page={0}'.format(1),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 1)
        self.assertEqual(response_data[0]['id'], self.artist.id) 

    def test_MyFollowingsAPI_POST_missed_data(self):
        response = app.request(
            '/api/followings/mine',
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'artist_id is required.')

    def test_MyFollowingsAPI_POST_unauthorized(self):
        response = app.request(
            '/api/followings/mine',
            method='POST',
            data=json.dumps({'artist_id': self.artist.id}), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_MyFollowingsAPI_POST_404_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id and deleted_at is null',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/followings/mine',
            method='POST',
            data=json.dumps({'artist_id': self.artist.id}), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'),
            u'The current profile is not found.'
        )

    def test_MyFollowingsAPI_POST_followed_user_not_found(self):
        user, profile, account = signup()
        response = app.request(
            '/api/followings/mine',
            method='POST',
            data=json.dumps({'artist_id': self.artist.id + 1}), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'),
            u'The artist is not found.'
        )

    def test_MyFollowingsAPI_POST_already_followed(self):
        user, profile, account = signup()
        Followship.follow(profile, self.artist)
        response = app.request(
            '/api/followings/mine',
            method='POST',
            data=json.dumps({'artist_id': self.artist.id}), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'),
            u'The artist is already followed.'
        )

    def test_MyFollowingsAPI_POST(self):
        user, profile, account = signup()
        response = app.request(
            '/api/followings/mine',
            method='POST',
            data=json.dumps({'artist_id': self.artist.id}), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(
            response.headers['Content-Type'],
            'application/json'
        )
        followship = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            followship.get('followed_id'),
            self.artist.id
        )
        self.assertTrue(followship.get('created_at'))

    def test_MyFollowingsAPI_PUT_unartistized(self):
        response = app.request(
            '/api/followings/{0}/mine'.format(self.artist.id),
            method='PUT',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        self.assertEqual(
            json.loads(response.data, encoding='utf-8').get('msg'),
            u'请登录.'
        )

    def test_MyFollowingsAPI_PUT_404_profile(self):
        user, profile, account = signup()
        # Followship.follow(profile, self.artist)
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id and deleted_at is null',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/followings/{0}/mine'.format(self.artist.id),
            method='PUT',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'],
            'application/json'
        )
        self.assertEqual(
            json.loads(response.data, encoding='utf-8').get('msg'),
            u'The current profile is not found.'
        )

    def test_MyFollowingsAPI_PUT_404_artist(self):
        user, profile, account = signup()
        Followship.follow(profile, self.artist)
        response = app.request(
            '/api/followings/{0}/mine'.format(self.artist.id + 1),
            method='PUT',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'],
            'application/json'
        )
        self.assertEqual(
            json.loads(response.data, encoding='utf-8').get('msg'),
            u'The artist is not found.'
        )

    def test_MyFollowingsAPI_PUT_404_user_not_followed(self):
        user, profile, account = signup()
        response = app.request(
            '/api/followings/{0}/mine'.format(self.artist.id),
            method='PUT',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        self.assertEqual(
            json.loads(response.data, encoding='utf-8').get('msg'),
            u'The artist is not followed.'
        )
   
    def test_MyFollowingsAPI_PUT(self):
        user, profile, account = signup()
        Followship.follow(profile, self.artist)
        response = app.request(
            '/api/followings/{0}/mine'.format(self.artist.id),
            method='PUT',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['followed_id'], self.artist.id)
        self.assertTrue(response_data['deleted_at'])

    def test_ArtistFollowedsAPI_GET_400_missed_page(self):
        response = app.request(
            '/api/artists/{0}/followeds'.format(self.artist.id),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u"page is required.")

    def test_ArtistFollowedsAPI_GET_401(self):
        response = app.request(
            '/api/artists/{0}/followeds?page={1}'.format(self.artist.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u"请登录.")
 
    def test_ArtistFollowedsAPI_GET_404_profile(self):
        user, profile, account = signup()
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id and deleted_at is null',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/artists/{0}/followeds?page={1}'.format(self.artist.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The profile is not found."
        )
 
    def test_ArtistFollowedsAPI_GET_404_artist(self):
        user, profile, account = signup()
        response = app.request(
            '/api/artists/{0}/followeds?page={1}'.format(self.artist.id + 1, 1),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The artist is not found."
        )
 
    def test_ArtistFollowedsAPI_GET_403(self):
        user, profile, account = signup()
        response = app.request(
            '/api/artists/{0}/followeds?page={1}'.format(self.artist.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"Not Allowed."
        )
 
    def test_ArtistFollowedsAPI_GET_200(self):
        user, profile, account = signup()
        seller_role = Role.create(Storage(name='seller'))
        Assignment.has_role(profile, seller_role)
        response = app.request(
            '/api/artists/{0}/followeds?page={1}'.format(self.artist.id, 1),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 0)


if __name__ == '__main__':
    unittest.main()
