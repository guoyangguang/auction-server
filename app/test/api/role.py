# -*-coding: utf-8-*-

'''
RoleAPITest module
'''

from datetime import datetime
import unittest
import json
from web import Storage
from ...model.role import Role
from ...model.assignment import Assignment
from ...box.utils import invalid_msgs
from ..helper import truncate_db, signup
from ...app import app
from ...config.conf import db

class RoleAPITest(unittest.TestCase):
    '''
    RoleAPITest class 
    '''

    def setUp(self):
        self.role = Storage(name='admin')

    def tearDown(self):
        truncate_db()

    def test_RolesAPI_POST_missing_data(self):
        response = app.request(
            '/api/roles',
            method='POST',
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'name is required.'
        )

    def test_RolesAPI_POST_401(self):
        response = app.request(
            '/api/roles',
            method='POST',
            data=json.dumps({'name': 'seller'}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'请登录.'
        )

    def test_RolesAPI_POST_404_profile(self):
        user, profile, account = signup()
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/roles',
            method='POST',
            data=json.dumps({'name': 'seller'}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'The profile is not found.'
        )

    def test_RolesAPI_POST_403_forbidden(self):
        user, profile, account = signup()
        response = app.request(
            '/api/roles',
            method='POST',
            data=json.dumps({'name': 'seller'}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'Not allowed.'
        )

    def test_RolesAPI_POST_400_invalid_data(self):
        user, profile, account = signup()
        role = Role.create(self.role)
        Assignment.has_role(profile, role)
        response = app.request(
            '/api/roles',
            method='POST',
            data=json.dumps({'name': '@'}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertTrue(
            invalid_msgs['slug'] in response_data.get('msg')[0]
        )

    def test_RolesAPI_POST_201(self):
        user, profile, account = signup()
        role = Role.create(self.role)
        Assignment.has_role(profile, role)
        name = 'seller'
        response = app.request(
            '/api/roles',
            method='POST',
            data=json.dumps({'name': name}),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        for key in ['id', 'name', 'created_at', 'updated_at', 'deleted_at']:
            self.assertTrue(
                key in response_data
            )

    def test_RolesAPI_GET_401(self):
        response = app.request(
            '/api/roles',
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'请登录.'
        )

    def test_RolesAPI_GET_404_current_profile(self):
        user, profile, account = signup()
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars={'_id': profile.id}
        )
        response = app.request(
            '/api/roles',
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'The profile is not found.'
        )

    def test_RolesAPI_GET_403_forbidden(self):
        user, profile, account = signup()
        response = app.request(
            '/api/roles',
            method='GET'
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'Not allowed.'
        )

    def test_RolesAPI_GET_200(self):
        user, profile, account = signup()
        role = Role.create(self.role)
        Assignment.has_role(profile, role)
        self.customer_service = Role.create(
            Storage(name='customer_service')
        )
        self.seller = Role.create(Storage(name='seller'))

        response = app.request(
            '/api/roles',
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(
            response.headers['Content-Type'], 'application/json'
        )
        response_data = json.loads(response.data)
        self.assertEqual(len(response_data), 3)


if __name__ == '__main__':
    unittest.main()
