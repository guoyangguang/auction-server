# -*- coding: utf-8 -*-

import smtplib
from email.mime.text import MIMEText
from ..config.conf import MAIL_SERVER_HOST, MAIL_SERVER_EMAIL, MAIL_SERVER_PASSWORD


def send_mail(subject, body, tos):
    message = MIMEText(body, _subtype='plain', _charset='utf-8')
    message['Subject'] = subject
    message['From'] = MAIL_SERVER_EMAIL
    message['To'] = ';'.join(tos)
    server = smtplib.SMTP()
    server.connect(MAIL_SERVER_HOST)
    server.login(MAIL_SERVER_EMAIL, MAIL_SERVER_PASSWORD)
    server.sendmail(MAIL_SERVER_EMAIL, tos, message.as_string())
    server.close()
    return message 
