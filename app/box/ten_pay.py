# -*- coding: utf-8 -*-

'''
tenpay including computer web, mobile web, app
'''

from weixin.pay import WeixinPay, WeixinPayError
from ..config import conf

pay = WeixinPay(
    conf.TENPAY_APPID, conf.TENPAY_MCHID, conf.TENPAY_MCHKEY,
    conf.TENPAY_NOTIFY_URL, conf.TENPAY_PUBLIC_KEY_PATH, '/path/to/cert.pem'
) # 后两个参数可选


def unified_order():
    '''
    注意: 默认spbill_create_ip的值为request.remote_addr，
    如果没有安装flask，请在参数后面带上spbill_create_ip
    '''
    try:
        out_trade_no = pay.nonce_str
        raw = pay.unified_order(
            trade_type="NATIVE", product_id="productid", body=u"测试",
            out_trade_no=out_trade_no, total_fee=1, attach="other info"
        )
        return raw
    except WeixinPayError as e:
        print e.message


def pay_notify():
    """
    微信异步通知
    """
    # TODO request.data
    data = pay.to_dict(request.data)
    if not pay.check(data):
        return pay.reply("签名验证失败", False)
    # 处理业务逻辑
    return pay.reply("OK", True)


if __name__ == '__main__':
    pass
