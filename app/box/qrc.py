# -*-coding: utf-8-*-

import qrcode
from ..tasks import box_logger_log


class QRCODE(object):

    # dimension, the value is between 1-40
    VERSION = 1
    # the error correction used for the QR Code
    ERROR_CORRECTION = qrcode.constants.ERROR_CORRECT_L
    # pixels each “box” of the QR code is
    BOX_SIZE = 10
    # border thick
    BORDER = 4

    def __init__(self):
        self.qrcode = qrcode.QRCode(
            version=QRCODE.VERSION,
            error_correction=QRCODE.ERROR_CORRECTION,
            box_size=QRCODE.BOX_SIZE,
            border=QRCODE.BORDER
        )

    def make_qrcode(self, data, path):
        '''
        Generate one qrcode from file path.

        :param data: data to be compiled
        :type data: string
        :param path: the path of qrcode image
        :type path: string
        :return: the generated qrcode image
        '''
        # add data
        self.qrcode.add_data(data)
        # compile data into qrcode array
        self.qrcode.make(fit=True)
        # make image from qrcode data
        img = self.qrcode.make_image()
        # save image
        try:
            f = open(path, mode='w')
        except IOError as e:
            box_logger_log.delay(40, '{0}'.format(str(e)))
        else:
            img.save(f)
            f.close()
        return img

if __name__ == '__main__':
    qrc = QRCODE()
    img = qrc.make_qrcode(
        data='http://www.ningxia.com',
        path='/Users/gyg/Desktop/10.jpg'
    )
    print(img)
