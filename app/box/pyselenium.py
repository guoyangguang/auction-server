# -*- coding: utf-8 -*-

"""
Selenium related base class.
"""

from selenium import webdriver
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait

from ..config import conf


class Browser(object):
    """
    Browser integrates methods of Selenium to
    make interaction with browser easier.
    """
    def __init__(self):
        self.browser = Browser.web_driver()

    def open(self):
        """
        quit the current running browser,
        and open a new one.
        """
        if self.browser:
            self.quit()
        self.browser = Browser.web_driver()

    def quit(self):
        """
        quit the current running browser.
        """
        if self.browser:
            self.browser.quit()
            self.browser = None

    def get(self, url):
        """
        make a get request
        """
        self.browser.get(url)

    def current_url(self):
        '''
        current url
        '''
        return self.browser.current_url

    def wait_for_css_selector(self, selector):
        """
        wait until webdriver loads a HTML element with
        specified CSS selector

        :param webdriver: Selenium webdriver
        :param selector: Specified CSS selector
        """
        WebDriverWait(self.browser, conf.SELENIUM_WAIT_TIMEOUT).until(
            lambda driver: len(driver.find_elements_by_css_selector(selector)),
            "URL: %s | Waiting for %s, but didn't show up in time" % (
                self.browser.current_url, selector
            )
        )

    def wait_until(self, func):
        """
        wait until func returns True

        :param func: a lambda or function
        """
        WebDriverWait(self.browser, conf.SELENIUM_WAIT_TIMEOUT).until(
            func,
            "URL: %s | fails to wait." % (self.browser.current_url)
        )

    def find_element(self, selector):
        """
        find single element and return it.
        """
        return self.browser.find_element_by_css_selector(selector)

    def find_elements(self, selector):
        """
        find a collection of elements and return them.
        """
        return self.browser.find_elements_by_css_selector(selector)

    def fill_field(self, field_selector, value):
        """
        find a field on the page to input value.

        :field_selector: css selector to locate the field
        :value: value to be filled into the field
        """

        field = self.browser.find_element_by_css_selector(field_selector)
        field.clear()
        field.send_keys(value)
        return field

    def fill_ckeditor_field(self, selector, value):
        """
        find an iframe in ckeditor to input value.

        :selector: css selector to locate the element containing iframe.
        :value: value to be filled into the iframe.
        """
        # TODO fill iframe instead of ckeditor
        selector = selector + ' iframe'
        iframe = self.browser.find_element_by_css_selector(selector)
        self.browser.switch_to.frame(iframe)
        body = self.browser.find_element_by_css_selector('body')
        body.send_keys(value)
        self.browser.switch_to.default_content()
        return iframe

    def click(self, selector):
        """
        find a field on the page to click, like link, button,

        text input, check box, radio input.
        :selector: css selector to locate the field
        """
        field = self.browser.find_element_by_css_selector(selector)
        field.click()
        return field

    def select_by_visible_text(self, select_tag_selector, text):
        """
        select an option that has the text from a select tag.

        :select_tag_selector: css selector to locate the select tag
        :text: text to locate the option
        """
        select_tag = self.browser.find_element_by_css_selector(
            select_tag_selector
        )
        Select(select_tag).select_by_visible_text(text)

    def select_by_index(self, select_tag_selector, index):
        """
        select an option that has the index from a select tag.

        :select_tag_selector: css selector to locate the select tag
        :index: index to locate the option
        """
        select_tag = self.browser.find_element_by_css_selector(
            select_tag_selector
        )
        Select(select_tag).select_by_index(index)

    def select_by_value(self, select_tag_selector, value):
        """
        select an option that has the value from a select tag.

        :select_tag_selector: css selector to locate the select tag
        :value: value to locate the option
        """
        select_tag = self.browser.find_element_by_css_selector(
            select_tag_selector
        )
        Select(select_tag).select_by_value(value)

    def accept_alert(self):
        """
        wait for an alert to pop up, and accept the alert.

        :return: the text on the alert panel.
        """
        WebDriverWait(self.browser, conf.SELENIUM_WAIT_TIMEOUT).until(
            lambda b: expected_conditions.alert_is_present(),
            'fails to wait for alert'
        )
        alert = self.browser.switch_to.alert
        text = alert.text
        alert.accept()
        return text

    def switch_to_window(self, window_name):
        """
        switches focus to the specified window.

        :param window_name: The name or window handle of
        the window to switch to.
        """
        self.browser.switch_to.window(window_name)

    def is_displayed(self, selector):
        '''
        check if element is displayed
        '''
        element = self.browser.find_element_by_css_selector(selector)
        return element.is_displayed()

    def filter_displayed_and_enabled(self, selector):
        '''
        filter elements which are displayed and enabled
        '''
        elements = self.browser.find_elements_by_css_selector(selector)
        elements = filter(
            lambda element: element.is_displayed() and
            element.is_enabled(),
            elements
        )
        return elements

    @classmethod
    def web_driver(cls):
        '''
        config profile, and return a driver instance
        '''

        profile = webdriver.FirefoxProfile()
        profile.set_preference(
            "general.useragent.override",
            conf.USER_AGENT
        )
        profile.set_preference('browser.cache.memory.enable', True)
        profile.set_preference('browser.cache.disk.enable', True)
        profile.set_preference('browser.cache.offline.enable', True)
        profile.set_preference('network.http.use-cache', True)
        profile.set_preference(
            'browser.cache.disk.parent_directory', '/tmp'
        )
        driver = webdriver.Firefox(profile)
        driver.desired_capabilities["applicationCacheEnabled"] = True
        driver.set_window_size(1024, 768)
        return driver
