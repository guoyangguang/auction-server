# -*- coding: utf-8 -*-

import requests
import time
import hashlib
import json

IOS_ACCESS_ID = ""
IOS_SECRET_KEY = ""
ANDROID_ACCESS_ID = ""
ANDROID_SECRET_KEY = ""
ANDROID_TOKEN = ""
IOS_TOKEN = ""


def push_to_single_device(device_token, message, message_type=1, environment=0): 
    '''
    push message to single device

    :param message_type: 1：通知 2：透传消息。iOS平台请填0
    :param environment: 向iOS设备推送时必填，1表示推送生产环境；2表示推送开发环境。
                        推送Android平台不填或填0
    {
       “ret_code”:0,
       “err_msg”:”ok”,
       “result”:{“status”:0}
    }
    '''
    url = 'http://openapi.xg.qq.com/v2/push/single_device'
    access_id = IOS_ACCESS_ID if message_type==0 else ANDROID_ACCESS_ID
    secret_key = IOS_SECRET_KEY if message_type==0 else ANDROID_SECRET_KEY
    data = dict(
        access_id=access_id.encode('utf-8'),
        timestamp=str(int(time.time())).encode('utf-8'),
        # valid_time
        device_token=device_token.encode('utf-8'),
        message_type=str(message_type).encode('utf-8'),
        message=message.encode('utf-8'),
        # expire_time
        # send_time
        # multi_pkg
        # 向iOS设备推送时必填，1表示推送生产环境；2表示推送开发环境。
        # 推送Android平台不填或填0
        environment=str(environment).encode('utf-8')
    )
    sign = gen_sign(data, secret_key) 
    data.update({'sign': sign}) 
    try:
        response = requests.post(
            url, 
            data=data,
            headers={'content-type': 'application/x-www-form-urlencoded'}
        )
        return response
    except requests.exceptions.RequestException as e:
        # here log exception
        return None


def gen_sign(data, secret_key):
    str_for_md5 = "POSTopenapi.xg.qq.com/v2/push/single_device" +\
        ''.join(['{0}={1}'.format(key, data[key]) for key in sorted(data)]) + secret_key
    return (hashlib.md5(str_for_md5).hexdigest()).encode('utf-8')


def notification_of_android(title, content, **args):
    dic = dict(
       title=title, 
       content=content, 
       builder_id=0,
       ring=args.get('ring') if args.get('ring') else 1,
       vibrate=args.get('vibrate') if args.get('vibrate') else 0,
       clearable=args.get('clearable') if args.get('clearable') else 1 
    )
    return json.dumps(dic)


def message_for_ios(body, action_loc_key="PLAY", badge=5, category="INVITE_CATEGORY", **args):
    dic = {
        #apns规定的key-value
        "aps": {   
            "alert": {
                "body": body,
                "action-loc-key": action_loc_key
            },
            "badge": badge,
            "category": category,
        }
    }
    dic.update(args)
    return json.dumps(dic)
