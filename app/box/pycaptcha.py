# -*-coding: utf-8-*-

'''
captcha image and data
'''
import os
import random
import string
from captcha.image import ImageCaptcha

from .exception import BadRequest


def set_captcha(session):
    '''
    set new captcha in the session
    '''
    session.pop('captcha_data', None)
    stringio, data = gen_captcha()
    session.captcha_data = data
    print('captcha is: ', session.captcha_data)
    return stringio


def check_captcha(captcha_data, session):
    '''
    check captcha
    '''
    captcha_data = captcha_data.upper()
    if not captcha_data == session.captcha_data:
        raise BadRequest(message='请输入正确的图片文字.')


def gen_captcha():
    '''
    generate captcha image and data
    '''
    font_path = [os.path.abspath(
        os.path.join(__file__, '../', 'fonts/DroidSansMono.ttf')
    )]
    seq = string.uppercase + string.digits
    data = ''.join([str(random.SystemRandom().choice(seq)) for _ in range(4)])
    # TODO handle exceptions
    image = ImageCaptcha(fonts=font_path)
    stringio = image.generate(data)
    return (stringio, data)


if __name__ == '__main__':
    font_path = ['/Users/gyg/app/auction/server/apps/box/fonts/DroidSansMono.ttf']
    image = ImageCaptcha(fonts=font_path)
    image.generate('8295')
    image.write('8295', 'out.jpg')
