# -*- coding: utf-8 -*-

'''
alipay including computer web, mobile web, app
'''
from alipay import AliPay
from ..config import conf
from ..box import utils

rsa_private_key = utils.read_file(conf.RSA_PRIVATE_KEY_PATH)
alipay_public_key = utils.read_file(conf.ALIPAY_PUBLIC_KEY_PATH)

alipay = AliPay(
    appid=conf.ALIPAY_APPID,
    app_notify_url=None, # 支付宝服务器主动通知商户服务器里指定的页面http/https路径。use the default notify path
    app_private_key_string=rsa_private_key,
    alipay_public_key_string=alipay_public_key, # alipay public key, do not use your public key!
    sign_type="RSA2", # RSA or RSA2
    debug=False  # False by default
)

def ready_to_pay(order):
    '''
    电脑网站支付，跳转到 + order_string
    '''
    order_string = alipay.api_alipay_trade_page_pay(
        out_trade_no=order.id,
        total_amount=float(order.amount)/float(100),
        subject=u"测试订单".encode("utf8"),
        return_url="https://example.com", # 同步返回地址，HTTP/HTTPS开头字符串
        notify_url="http://127.0.0.1:80/api/alipay/notify" # 可选, 不填则使用默认notify url
    )
    return conf.ALIPAY_COMPUTER_WEB_PAY_URL + "?" + order_string


def trade_query(order):
    '''
    调用alipay工具查询支付结果
    10000 TRADE_SUCCESS
    40004 表示支付宝接口调用暂时失败
    10000, trade_status == "WAIT_BUYER_PAY", 等待用户支付
    '''
    return alipay.api_alipay_trade_query(order.id)


if __name__ == '__main__':
    from web import Storage
    from ..model.order import Order
    order = Storage(id=10983289, amount=1)
    url = ready_to_pay(order)
    print(url, '-------------------------', check_pay(order))
