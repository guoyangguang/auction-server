# -*-coding: utf-8-*-

'''
elasticsearch client integration.
basic concept, https://www.elastic.co/guide/en/elasticsearch/reference/current/_basic_concepts.html
query and filter context, https://www.elastic.co/guide/en/elasticsearch/reference/current/query-filter-context.html
'''
from elasticsearch import ElasticsearchException
from ..config import conf
from .log import box_logger


class Esearch(object):
    '''
    elasticsearch client
    NOTE: keeps elasticsearch data same as db data
    '''
    def __init__(self, index):
        self.index = index

    def create_index(self):
        '''
        use elasticsearch.client.IndicesClient to create index;
        an index is a collection of docs.
        bound method IndicesClient.create of <elasticsearch.client.indices.IndicesClient
        object at 0x10269b990
        '''
        try:
            res = conf.es_cli.indices.create(index=self.index)
        except ElasticsearchException as e:
        # Base class for all exceptions raised by this package’s operations
        # (doesn’t apply to ImproperlyConfigured).
            box_logger.log(40, u'@Elastic client: ' + str(e))
        else:
            return res

    def drop_index(self):
        '''
        use elasticsearch.client.IndicesClient to delete index
        bound method IndicesClient.delete of <elasticsearch.client.indices.IndicesClient
        object at 0x10269b990
        '''
        try:
            res = conf.es_cli.indices.delete(index=self.index)
        except ElasticsearchException as e:
            box_logger.log(40, u'@Elastic client: ' + str(e))
        else:
            return res

    def index_exists(self):
        '''
        use elasticsearch.client.IndicesClient to check index exist
        bound method IndicesClient.exists of <elasticsearch.client.indices.IndicesClient
        object at 0x10269b990
        '''
        try:
            res = conf.es_cli.indices.exists(index=self.index)
        except ElasticsearchException as e:
            box_logger.log(40, u'@Elastic client: ' + str(e))
        else:
            return res

    def cat_index(self):
        '''
        use elasticsearch.client.CatClient to cat indices
        bound method CatClient.indices of <elasticsearch.client.cat.CatClient
        object at 0x10269ba50
        '''
        try:
            res = conf.es_cli.cat.indices(index=self.index, v=True)
        except ElasticsearchException as e:
            box_logger.log(40, u'@Elastic client: ' + str(e))
        else:
            return res

    def get(self, _id):
        '''
        use elasticsearch.Elasticsearch to get doc from the index
        '''
        try:
            res = conf.es_cli.get(
                index=self.index, doc_type=self.index, id=_id
            )['_source']
        except ElasticsearchException as e:
            box_logger.log(40, u'@Elastic client: ' + str(e))
        else:
            return res

    def insert(self, doc):
        '''
        use elasticsearch.Elasticsearch to add doc in the index;
        '''
        try:
            res = conf.es_cli.create(
                index=self.index, doc_type=self.index, id=doc['id'], body=doc
            )
        except ElasticsearchException as e:
            box_logger.log(40, u'@Elastic client: ' + str(e))
        else:
            _id = res.get('_id')
            if _id:
                return self.get(int(_id))

    def update(self, _id, doc):
        '''
        use elasticsearch.Elasticsearch to update doc for the id;
        :param doc: not containing id
        '''
        try:
            res = conf.es_cli.index(
                index=self.index, doc_type=self.index, id=_id, body=doc
            )
        except ElasticsearchException as e:
            box_logger.log(40, u'@Elastic client: ' + str(e))
        else:
            _id = res.get('_id')
            if _id:
                return self.get(int(_id))

    def delete(self, _id):
        '''
        use elasticsearch.Elasticsearch to delete doc from the index
        '''
        # TODO refresh
        try:
            res = conf.es_cli.delete(
                index=self.index, doc_type=self.index, id=_id
            )
        except ElasticsearchException as e:
            box_logger.log(40, u'@Elastic client: ' + str(e))
        else:
            return res.get('found')

    def search_about(self, query):
        '''
        use elasticsearch.Elasticsearch to search docs in the index with query
        TODO chinese search; pagination
        [doc['id'] for doc in result['docs']]
        '''
        try:
            res = conf.es_cli.search(
                index=self.index, doc_type=self.index, body=query
            )
        except ElasticsearchException as e:
            box_logger.log(40, u'@Elastic client: ' + str(e))
            # box_logger.log u'@Elastic client: {0}'.format(e)
        else:
            result = {'count': res['hits']['total'], 'ids': [], 'docs': []}
            for hit in res['hits']['hits']:
                result['docs'].append(hit['_source'])
                result['ids'].append(hit['_id'])
            return result


if __name__ == '__main__':
    pass
    # from pdb import set_trace;set_trace()
    # from datetime import datetime
    # es = Esearch(index='items')
    # # es.create_index()
    # print es.index_exists()
    # print es.cat_index()
    # query context is how well, it is related with score, there are
    # best_fields, most_fields, cross_fields in one query string;
    # filter context is yes or no, it is used to filter the query context.
    # query = {
    #     "query": {
    #         "bool": {
    #             "must": [
    #                 {"match": {"name": query}},
    #                 {"match": {"description": query}}
    #             ],
    #             "filter": [
    #                 {"term": {"deleted_at": None}},
    #             ]
    #         }
    #     },
    #     "from": 0,
    #     "size": 10
    # }
    # print es.search_about(query)
