# -*- coding: utf-8 -*-
'''
image uploader
'''

import os
import shutil
import cgi
from PIL import Image
from ..config.conf import UPLOADED_IMG_DIR, DEFAULT_UPLOAD, UPLOADED_IMG_DIR_PREFIX
from .log import box_logger


class Uploader(object):
    '''
    one img one model
    img is instance of CGI FIELDSTOREAGE
    '''
    @classmethod
    def insert(cls, table, model, versions, parsed_upload):
        '''
        Actually, we need to open the fout img object in mode "wb"
        (in windows), ie.
        write binary mode, otherwise the image uploaded is broken.
        '''
        store_dir = cls.store_dir(table, model)
        os.makedirs(store_dir)
        img_path = os.path.join(store_dir, model.img)
        _file = parsed_upload['file']
        splitted_img_name = model.img.rsplit('.', 1)
        vdimensions = cls.vdimensions(versions, parsed_upload['ratio'])
        try:
            written_img = open(img_path, 'w')
            written_img.write(_file.read())
            # create different versions for theimg
            for key in vdimensions:
                img = Image.open(_file)
                version_img_path = os.path.join(
                    store_dir,
                    splitted_img_name[0] + "_{version}." + splitted_img_name[1]
                ).format(version=key)
                img.thumbnail(
                    (vdimensions[key][0], vdimensions[key][1]),
                    Image.ANTIALIAS
                )
                img.save(version_img_path)
        except IOError:
            box_logger.log(40, "@uploader cannot write img and its versions for " + store_dir)
        else:
            return store_dir
        finally:
            written_img.close()
            img.close()

    @classmethod
    def filepath(cls, table, model):
        '''
        the path
        '''
        return os.path.join(
            UPLOADED_IMG_DIR,
            '{table_dir}/{model_dir}'.format(
                table_dir=table,
                model_dir=model.id
            ),
            model.img
        )

    @classmethod
    def filepath_of(cls, table, model, version):
        '''
        the path for the version
        '''
        splitted_img_name = model.img.rsplit('.', 1)
        return os.path.join(
            UPLOADED_IMG_DIR,
            '{table_dir}/{model_dir}'.format(
                table_dir=table,
                model_dir=model.id
            ),
            (splitted_img_name[0] + "_{version}." + splitted_img_name[1]).format(
                version=version
            )
        )

    @classmethod
    def default_filepath(cls, version):
        '''
        the default path for the version if users not upload
        '''
        return os.path.join(
            UPLOADED_IMG_DIR,
            'default',
            (DEFAULT_UPLOAD + '_{version}' + '.jpg').format(version=version)
        )

    @classmethod
    def remove(cls, table, model):
        '''
        delete store dir
        '''
        path = cls.store_dir(table, model)
        if os.path.exists(path):
            shutil.rmtree(path)

    @classmethod
    def store_dir(cls, table, model):
        '''
        the dir in which imgs is
        '''
        dir_template = os.path.join(
            UPLOADED_IMG_DIR_PREFIX,  # prefix is used to write data
            UPLOADED_IMG_DIR.lstrip('/'),
            '{table_dir}/{model_dir}'
        )
        return dir_template.format(
            table_dir=table,
            model_dir=model.id
        )

    @classmethod
    def parse_uploaded(cls, upload):
        '''
        parse upload
        :param upload: upload is data in web.input,
        make sure the upload is a file
        '''
        if isinstance(upload, cgi.FieldStorage):
            file_path = upload.filename.replace('\\', '/')
            file_name = file_path.split('/')[-1]
            _file = upload.file
            try:
                img = Image.open(_file)
            except(IOError):
                return None
            ratio = float(img.size[1])/float(img.size[0])
            return { 
                'filename': file_name,
                'file': _file,
                'filesize': len(upload.value),
                'filetype': upload.type,
                'ratio': ratio
            } 

    @classmethod
    def validate_img(cls, filesize, filetype):
        '''
        validate image size and type
        '''
        errors = list()
        if filesize > 2 * 1024 * 1024:
            errors.append('The image size must be smaller than 2MB.')
        if filetype not in ['image/jpg', 'image/jpeg', 'image/gif', 'image/png']:
            errors.append('The image type does not belong to jpg, gif, png.')
        return errors

    @classmethod
    def vdimensions(cls, versions, ratio):
        vdimensions = {}
        for key in versions:
            width = versions.get(key)
            height = int(versions.get(key)*ratio)
            vdimensions[key] = [width, height]
        return vdimensions
