# -*-coding: utf-8-*-

'''
short message module
'''
from qcloudsms_py import SmsSingleSender
from qcloudsms_py.httpclient import HTTPError
from ..config import conf


ssender = SmsSingleSender(conf.APPID, conf.APPKEY)

def send_single(phone_num, msg, sms_type=0):
    '''
    Enum{0: 普通短信, 1: 营销短信}
    '''
    try:
        result = ssender.send(sms_type, 86, phone_num, msg, extend="", ext="")
    except HTTPError as e:
        print(e)
    except Exception as e:
        print(e)
    # print(result)

def send_single_with_template(phone_num, params):
    '''
    params = ["5678"]  # 当模板没有参数时，`params = []`
    签名参数未提供或者为空时，会使用默认签名发送短信
    '''
    try:
        result = ssender.send_with_param(
            86,
            phone_num,
            conf.TEMPLATE_ID,
            params,
            sign=conf.SIGN,
            extend="",
            ext=""
        )
    except HTTPError as e:
        print(e)
    except Exception as e:
        print(e)
    # print(result)


if __name__ == '__main__':
    phone_numbers = ["18909512215", "18909539628"]
    short_message = u"【腾讯云】您的验证码是: 5678"
    send_single(phone_numbers[0], short_message)
