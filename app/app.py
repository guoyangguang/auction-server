# -*- coding: utf-8 -*-

'''
api app
'''
from .config import conf
from .box import utils

from .api.access_token import (
    AccessTokenMineAPI, AccessTokenMinePubStatusAPI
)
# from .api.address import AddressMineAPI
from .api.artist import ArtistsAPI, ArtistAPI
from .api.assignment import AssignmentAPI
from .api.attorney import ArtistAttorneysAPI
from .api.bid import ItemBidsAPI
from .api.captcha import CaptchaAPI
from .api.comment import ItemCommentsAPI
from .api.followship import MyFollowingsAPI, ArtistFollowedsAPI 
from .api.item import (
    ArtistItemsAPI, ArtistItemShowAPI,
    ItemsAPI, FollowingItemsAPI, ItemsSearchAPI, ItemShowAPI
)
from .api.item_img import ArtistItemItemImgsAPI
from .api.like import LikeMineAPI
# from .api.message import (
#    MessagesContactsAPI, MessagesUnreadMineAPI, MessagesBetweenAPI
# )
from .api.order import ItemOrderAPI
from .api.profile import ProfileAPI, ProfileFileAPI
from .api.role import RolesAPI
from .api.user import (
    AuthSignupAPI, AuthSigninAPI, AuthSignoutAPI,
    AuthResetPasswordAPI, UsersAPI
)
from .api.verification import VerificationAPI

urls = (
    '/api/alipay/notify', 'AliPayAPI',
    '/api/access_tokens/mine', 'AccessTokenMineAPI',
    '/api/access_tokens/mine/pubstatus', 'AccessTokenMinePubStatusAPI',
    '/api/artists/(\d+)/attorneys', 'ArtistAttorneysAPI',
    '/api/artists/(\d+)/followeds', 'ArtistFollowedsAPI',
    '/api/artists/(\d+)/items', 'ArtistItemsAPI',
    '/api/artists/(\d+)/items/(\d+)', 'ArtistItemShowAPI',
    '/api/artist/items/(\d+)/itemimgs', 'ArtistItemItemImgsAPI',
    '/api/assignments', 'AssignmentAPI',
    '/api/captcha', 'CaptchaAPI',
    '/api/followings/(\d+)/items', 'FollowingItemsAPI',
    '/api/followings/mine', 'MyFollowingsAPI',
    '/api/followings/(\d+)/mine', 'MyFollowingsAPI',
    '/api/items', 'ItemsAPI',
    '/api/items/search', 'ItemsSearchAPI',
    '/api/items/(\d+)', 'ItemShowAPI',
    '/api/items/(\d+)/bids', 'ItemBidsAPI',
    '/api/items/(\d+)/comments', 'ItemCommentsAPI',
    '/api/items/(\d+)/order', 'ItemOrderAPI',
    '/api/likes/mine', 'LikeMineAPI',
    '/api/likes/mine/(\d+)', 'LikeMineAPI',
    '/api/profile', 'ProfileAPI',
    '/api/profile/file', 'ProfileFileAPI',
    '/api/artists', 'ArtistsAPI',
    '/api/artists/(\d+)', 'ArtistAPI',
    '/api/roles', 'RolesAPI',
    '/api/reset_password', 'AuthResetPasswordAPI',
    '/api/signup', 'AuthSignupAPI',
    '/api/signin', 'AuthSigninAPI',
    '/api/signout', 'AuthSignoutAPI',
    '/api/users', 'UsersAPI',
    '/api/verifications', 'VerificationAPI',
)

app = conf.web.application(mapping=urls, fvars=locals(), autoreload=None)

wsgi_app = app.wsgifunc()

if not conf.web.config.debug:
    session = conf.web.session.Session(
        app,
        conf.web.session.DBStore(conf.db, 'sessions')
    )

    def session_hook():
        '''
        session hook
        '''
        conf.web.ctx.session = session
        conf.web.template.Template.globals['session'] = session
    app.add_processor(conf.web.loadhook(session_hook))


def after_request():
    '''
    add content type header for api request
    '''
    # if conf.web.ctx['path'] == '/api/captcha':
    #     conf.web.header('Content-Type', 'image/jpeg')
    # else:
    conf.web.header('Content-Type', 'application/json')
    if not conf.web.config.debug:
        csrf_token = utils.set_csrf_token(conf.web.ctx.session)
        utils.setcookie('auctionf', csrf_token)
    # In debug mode any changes to code and templates are
    # automatically reloaded and error messages will have
    # more helpful information.
    # print('is in debug mode? ', conf.web.config.debug)
    # print('header is :', conf.web.ctx.status, conf.web.ctx.headers)
app.add_processor(conf.web.unloadhook(after_request))

# keys in web.ctx,
# ['status', 'realhome', 'homedomain', 'protocol', 'app_stack',
#  'ip', 'fullpath',
# 'headers', 'host', 'environ', 'env', 'home', 'homepath', 'output', 'path',
# 'query', 'method']
# for example,
# web.ctx.env.get('HTTP_USER_AGENT')
