# -*- coding: utf-8 -*-

'''
dump database
'''

import subprocess
from datetime import datetime

now = datetime.now()
tofile = '/Users/gyg/app/auction_db_dump/dump-{0}.sql'.format(now)
subprocess.run(
    ['/usr/local/pgsql/bin/pg_dump', '-h', 'localhost', '-U', 'pgsqldata', '-f', tofile, 'auction_dev'],
    check=True
)


if __name__ == '__main__':
    pass
