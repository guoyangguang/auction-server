#-*- coding: utf-8 -*-

'''
update the status from 2 to 3 of the bids of the closed items
env EDITOR=VI crontab -e, edit cron
env EDITOR=VI crontab -l, list crons 
'''

import datetime
from ..model.item import Item
from ..model.bid import Bid


closed_items = Item.filter_closed_items(datetime.datetime.now())
if len(closed_items):
    bids_num = Bid.update_status_from2to3_by_itemids([item.id for item in closed_items])


if __name__ == '__main__':
    pass
    # import sched, time
    # s = sched.scheduler(time.time, time.sleep)
    # 
    # def say_hello(name):
    #     print("how are you? ", name)
    #     s.enter(3, 2, say_hello, kwargs={'name': 'yangguang.guo'})
    #     s.run()
