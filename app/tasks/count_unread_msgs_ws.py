# -*-coding: utf-8 -*-
'''
count unread msgs web socket,
run as __main__ module
'''
from .model.user import User
from .model.profile import Profile
from .model.message import Message
from autobahn.twisted.websocket import (
    WebSocketServerProtocol, WebSocketServerFactory
)
import sys
from twisted.python import log
from twisted.internet import reactor


class CountUnreadMsgsProto(WebSocketServerProtocol):
    '''
    count unread msgs protocol
    '''
    def onConnect(self, request):
        '''
        when client try connecting, the method will be called
        '''
        self.token = request.params['token'][0]
        print "CountUnreadMsgsWs is connecting: {0}".format(request.peer)

    def onOpen(self):
        '''
        when handshake is successful and connection is build, it will be triggered
        '''
        def send_msgs():
            self.sendMessage(self.count_unread_msgs(self.token))
            self.factory.reactor.callLater(5, send_msgs)
        send_msgs()

    def onMessage(self, payload, isBinary):
        '''
        when msg received, it will be triggered
        '''
        print "CountUnreadMsgsWs recieved message: #{0}".format(
            payload.decode('utf8')
        )

    def onClose(self, wasClean, code, reason):
        '''
        when ws connection is closed, it will be triggered
        '''
        print "CountUnreadMsgsWs connection closed for #{0}".format(self.token)

    def count_unread_msgs(self, token):
        '''
        count unread msgs
        :param token: the access token for the signed user
        :return: the number of unread msgs
        '''
        user = User.filter_by_access_token(token)
        profile = Profile.filter_by_user(user)
        num = Message.unread_count(profile)
        return u'{num}'.format(num=num).encode('utf8')


if __name__ == '__main__':
    log.startLogging(sys.stdout)
    factory = WebSocketServerFactory()  # ("ws://127.0.0.1:8080", debug = False)
    factory.protocol = CountUnreadMsgsProto

    reactor.listenTCP(8080, factory)
    reactor.run()
