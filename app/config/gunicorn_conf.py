# -*- coding: utf-8 -*-

'''
gunicorn configure for api
'''
import os


bind = '%s:%s' % ('127.0.0.1', 3000)
debug = False
daemon = False
loglevel = 'error'
accesslog = os.path.abspath(
    os.path.join(__file__, '../../log', 'gunicorn/api_access.log')
)
errorlog = os.path.abspath(
    os.path.join(__file__, '../../log', 'gunicorn/api_error.log')
)
workers = 1
