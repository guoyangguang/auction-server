# -*- coding: utf-8 -*-

'''
item presenter
'''

from .base import Base
from ..box.uploader import Uploader


class ItemImgPresenter(Base):

    @classmethod
    def created(cls, itemimg):
        itemimg_dict = cls._itemimg(itemimg)
        cls.data = itemimg_dict
        return cls

    @classmethod
    def _itemimg(cls, itemimg):
        itemimg_storage = cls.strf_timestamp(
            itemimg,
            'uploaded_at',
            'created_at',
            'updated_at',
            'deleted_at'
        )
        itemimg_storage.thumb_img = Uploader.filepath_of(
            'item_imgs', itemimg_storage, 'thumb'
        )
        itemimg_storage.detail_img = Uploader.filepath_of(
            'item_imgs', itemimg_storage, 'detail'
        )
        return dict(itemimg_storage)
