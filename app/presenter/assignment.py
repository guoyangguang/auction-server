# -*- coding: utf-8 -*-

'''
assignment presenter module
'''

from .base import Base


class AssignmentPresenter(Base):
    '''
    assignment presenter class
    '''

    @classmethod
    def assignment(cls, assignment):
        assignment_dict = cls._assignment(assignment)
        cls.data = assignment_dict
        return cls

    @classmethod
    def assignments(cls, assignments):
        assignment_list = [
            cls._assignment(assignment) for assignment in assignments
        ]
        cls.data = assignment_list
        return cls

    @classmethod
    def _assignment(cls, assignment):
        assignment_storage = cls.strf_timestamp(
            assignment, 'created_at', 'updated_at', 'deleted_at'
        )
        return dict(assignment_storage)
