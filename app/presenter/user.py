# -*- coding: utf-8 -*-
'''
user presenter
'''

from .base import Base
from .profile import ProfilePresenter


class UserPresenter(Base):

    @classmethod
    def user(cls, user):
        cls.data = cls._user(user)
        return cls

    @classmethod
    def signin(cls, user, profile):
        cls.data = cls._user(user)
        cls.data['profile'] = ProfilePresenter._profile(profile)
        return cls

    @classmethod
    def users(cls, users):
        cls.data = [cls._user(user) for user in users]
        return cls

    @classmethod
    def _user(cls, user):
        user_storage = cls.strf_timestamp(
            user,
            'current_sign_in_at',
            'last_sign_in_at',
            'created_at',
            'updated_at',
            'deleted_at'
        )
        return dict(
            id=user_storage.id,
            email=user_storage.email,
            phone=user_storage.phone,
            sign_in_count=user_storage.sign_in_count,
            current_sign_in_at=user_storage.current_sign_in_at,
            last_sign_in_at=user_storage.last_sign_in_at,
            current_sign_in_ip=user_storage.current_sign_in_ip,
            last_sign_in_ip=user_storage.last_sign_in_ip,
            created_at=user_storage.created_at,
            updated_at=user_storage.updated_at,
            deleted_at=user_storage.deleted_at
        )
