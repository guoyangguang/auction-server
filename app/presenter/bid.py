# -*-coding: utf-8 -*-

'''
bid presenter module
'''

from .base import Base
from .profile import ProfilePresenter

class BidPresenter(Base):
    '''
    bid presenter class
    '''
    @classmethod
    def item_bids(cls, bids):
        bid_list = list()
        for bid in cls._joineds_with_profile(bids):
            profile = bid.pop('profile')
            bid_list.append(
                cls._bid(bid, profile)
            )
        cls.data = bid_list
        return cls

    @classmethod
    def created(cls, profile, bid):
        cls.data = cls._bid(bid, profile)
        return cls

    @classmethod
    def _bid(cls, bid, profile):
        bid_dic = dict(
            cls.strf_timestamp(bid, 'created_at', 'updated_at', 'deleted_at')
        )
        bid_dic['profile'] = ProfilePresenter._profile(profile)
        return bid_dic
