# -*- coding: utf-8 -*-

import json
from datetime import datetime
from web import Storage


class Base(object):   
   
    data = None 
   
    @classmethod
    def as_json(cls):
        return json.dumps(cls.data)

    @classmethod
    def strf_timestamp(cls, model_ins, *timestamps): 
        '''
        strftime the timestamps in storage instance
        '''
        for key in timestamps:
            val = model_ins.get(key)
            if isinstance(val, datetime):
                model_ins[key] = val.strftime('%Y/%m/%d, %H:%M:%S')
        return model_ins

    @classmethod
    def _joineds_with_profile(cls, joineds):
        '''
        move profile fields into a seperated profile key
        '''
        ls = list()
        profile_cols = [
            'id', 'user_id', 'name', 'gender', 'location',
            'job', 'img', 'about', 'uploaded_at', 'created_at',
            'updated_at', 'deleted_at'
        ]
        for joined in joineds:
            assembled_profile = Storage()
            for col in profile_cols:
                assembled_profile[col] = joined.pop('profiles_' + col)
            joined['profile'] = assembled_profile
            ls.append(joined)
        return ls
