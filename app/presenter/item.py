#  -*- coding: utf-8 -*-

'''
item presenter
'''

from .base import Base
from .artist import ArtistPresenter
from .comment import CommentPresenter
from .item_img import ItemImgPresenter
from ..box.uploader import Uploader


class ItemPresenter(Base):
    '''
    item presenter
    '''
    @classmethod
    def created(cls, item):
        item_dict = cls._item(item)
        cls.data = item_dict
        return cls

    @classmethod
    def item(cls, item, itemimgs):
        item_dict = cls._item(item)
        item_dict['itemimgs'] = [
            ItemImgPresenter._itemimg(itemimg) for itemimg in itemimgs
        ]
        cls.data = item_dict
        return cls

    @classmethod
    def items(cls, items):
        item_list = [cls._item(item) for item in items]
        cls.data = item_list
        return cls

    @classmethod
    def show(cls, item, artist, is_followed, is_liked,
             like_count, comments, itemimgs):
        item_dic = {'item': cls._item(item)}
        item_dic['artist'] = ArtistPresenter._artist(artist)
        item_dic['is_followed'] = is_followed
        item_dic['is_liked'] = is_liked
        item_dic['like_count'] = like_count
        item_dic['comments'] = CommentPresenter._joined_with_profile(
            comments
        )
        item_dic['itemimgs'] = [
            ItemImgPresenter._itemimg(itemimg) for itemimg in itemimgs
        ]
        cls.data = item_dic
        return cls

    @classmethod
    def _item(cls, item):
        item_storage = cls.strf_timestamp(
            item,
            'produced_at',
            'starting_bid',
            'closing_bid',
            'uploaded_at',
            'created_at',
            'updated_at',
            'deleted_at'
        )
        item_storage.length = item_storage.size[0]
        item_storage.width = item_storage.size[1]
        item_storage.pop('size')
        item_storage.thumb_img = Uploader.filepath_of(
            'items', item_storage, 'thumb'
        )
        item_storage.detail_img = Uploader.filepath_of(
            'items', item_storage, 'detail'
        )
        item_storage.starting_price = float(item_storage.starting_price) / float(100)
        item_storage.raising_price = float(item_storage.raising_price) / float(100)
        item_storage.deposit = float(item_storage.deposit) / float(100)
        return dict(item_storage)
