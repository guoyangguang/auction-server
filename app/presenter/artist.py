
# -*- coding: utf-8 -*-

'''
artist presenter module
'''
from .base import Base
from ..box.uploader import Uploader


class ArtistPresenter(Base):
    '''
    artist presenter class
    '''

    @classmethod
    def all(cls, artists):
        artist_list = [cls._artist(artist) for artist in artists]
        cls.data = artist_list
        return cls

    @classmethod
    def followings(cls, artists):
        artist_list = [cls._artist(artist) for artist in artists]
        cls.data = artist_list
        return cls

    @classmethod
    def artist(cls, artist):
        cls.data = cls._artist(artist)
        return cls

    @classmethod
    def _artist(cls, artist):
        artist_storage = cls.strf_timestamp(
            artist, 'uploaded_at', 'created_at', 'updated_at', 'deleted_at'
        )
        if artist_storage.uploaded_at:
            detail_file = Uploader.filepath_of(
                'artists',
                artist_storage,
                'detail'
            )
            thumb_file = Uploader.filepath_of(
                'artists',
                artist_storage,
                'thumb'
            )
        else:
            detail_file = Uploader.default_filepath('detail')
            thumb_file = Uploader.default_filepath('thumb')
        artist_dict = dict(artist_storage)
        artist_dict['detail_file'] = detail_file
        artist_dict['thumb_file'] = thumb_file
        return artist_dict
