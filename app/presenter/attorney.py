# -*- coding: utf-8 -*-

'''
attorney presenter
'''

from .base import Base
from ..box.uploader import Uploader


class AttorneyPresenter(Base):

    @classmethod
    def created(cls, attorney):
        cls.data = cls._attorney(attorney)
        return cls

    @classmethod
    def _attorney(cls, attorney):
        attorney_storage = cls.strf_timestamp(
            attorney,
            'uploaded_at',
            'created_at',
            'updated_at',
            'deleted_at'
        )
        attorney_storage.thumb_img = Uploader.filepath_of(
            'attorneys', attorney_storage, 'thumb'
        )
        attorney_storage.detail_img = Uploader.filepath_of(
            'attorneys', attorney_storage, 'detail'
        )
        return dict(attorney_storage)
