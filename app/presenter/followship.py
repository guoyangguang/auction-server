# -*- coding: utf-8 -*-

from .base import Base


class FollowshipPresenter(Base):

    @classmethod
    def followship(cls, followship):
        followship_storage = cls.strf_timestamp(
            followship, 'created_at', 'updated_at', 'deleted_at'
        )
        cls.data = dict(followship_storage)
        return cls
