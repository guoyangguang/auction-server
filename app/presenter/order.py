# -*-coding: utf-8 -*-

'''
order presenter module
'''

from .base import Base

class OrderPresenter(Base):
    '''
    order presenter class
    '''
    @classmethod
    def created(cls, order, ready_to_pay_url):
        data = cls._order(order)
        data['ready_to_pay_url'] = ready_to_pay_url
        cls.data = data 
        return cls

    @classmethod
    def _order(cls, order):
        order_dic = dict(
            cls.strf_timestamp(
                order, 'paid_at', 'refunded_at', 'created_at',
                'updated_at', 'deleted_at'
            )
        )
        return order_dic
