# -*- coding: utf-8 -*-
'''
comment presenter
'''

from .base import Base
from .profile import ProfilePresenter


class CommentPresenter(Base):
    '''
    comment presenter
    '''
    @classmethod
    def comment(cls, profile, comment):
        data = cls._comment(profile, comment)
        cls.data = data
        return cls

    @classmethod
    def reply(cls, profile, reply, comment_profile, comment):
        '''
        :param comment: comment found in the api
        '''
        data = cls._comment(profile, reply)
        # FIXME comment_profile
        data['comment'] = cls._comment(comment_profile, comment)
        cls.data = data
        return cls

    @classmethod
    def _comment(cls, profile, comment):
        comment_dic = dict(
            cls.strf_timestamp(
                comment, 'created_at', 'updated_at', 'deleted_at'
            )
        )
        comment_dic['profile'] = ProfilePresenter._profile(profile)
        return comment_dic

    @classmethod
    def _joined_with_profile(cls, comments):
        _comments = Base._joineds_with_profile(comments)
        comment_list = list()
        assembled_comments = list()
        for comment in _comments:
            profile = comment.pop('profile')
            comment_list.append(cls._comment(profile, comment))
        for comment in comment_list:
            if comment['comment_id']:
                comment['comment'] = filter(
                    lambda c: c['id'] == comment['comment_id'],
                    comment_list
                )[0]
            assembled_comments.append(comment)
        return assembled_comments
