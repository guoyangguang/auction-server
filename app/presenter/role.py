# -*- coding: utf-8 -*-

'''
role presenter module
'''
from .base import Base


class RolePresenter(Base):
    '''
    role presenter class
    '''

    @classmethod
    def role(cls, role):
        role_dict = cls._role(role)
        cls.data = role_dict
        return cls

    @classmethod
    def roles(cls, roles):
        role_list = [cls._role(role) for role in roles]
        cls.data = role_list
        return cls

    @classmethod
    def _role(cls, role):
        role_storage = cls.strf_timestamp(
            role, 'created_at', 'updated_at', 'deleted_at'
        )
        return dict(role_storage)
