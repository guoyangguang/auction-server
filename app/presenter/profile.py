# -*- coding: utf-8 -*-

from .base import Base
from ..box.uploader import Uploader
from ..box import utils


class ProfilePresenter(Base): 

    @classmethod
    def profile(cls, profile):
        profile_dict = cls._profile(profile)
        cls.data = profile_dict
        return cls 
   
    @classmethod
    def profiles(cls, profiles):
        profile_list = [cls._profile(profile) for profile in profiles]
        cls.data = profile_list
        return cls 

    @classmethod
    def _profile(cls, profile):
        profile_storage = cls.strf_timestamp(
            profile, 'uploaded_at', 'created_at', 'updated_at', 'deleted_at'
        ) 
        if profile_storage.uploaded_at:
            detail_file = Uploader.filepath_of(
                'profiles', profile_storage, 'detail'
            )
            thumb_file = Uploader.filepath_of('profiles', profile_storage, 'thumb')
        else:
            detail_file = Uploader.default_filepath('detail')
            thumb_file = Uploader.default_filepath('thumb')
        profile_dict = dict(profile_storage)
        profile_dict['detail_file'] = detail_file
        profile_dict['thumb_file'] = thumb_file   
        return profile_dict
