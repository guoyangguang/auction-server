# -*- coding: utf-8 -*-

'''
AttorneysArtistAPI module
'''

from web import Storage
from .base import Base
from ..box.exception import Forbidden, NotFound, BadRequest
from ..model.profile import Profile
from ..model.assignment import Assignment
from ..model.artist import Artist
from ..model.attorney import Attorney
from ..presenter.attorney import AttorneyPresenter
from ..box.uploader import Uploader
from ..config import conf


class ArtistAttorneysAPI(Base):
    '''
    ArtistAttorneysAPI class
    '''
    def POST(self, artist_id):
        '''
        As a team role , i want to create an attorney
        for an artist, so that there will be one more attorney.
        '''
        # img={} to get cgi.FieldStorage
        data = self.input('img', img={})
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        artist = Artist.get(artist_id)
        if not artist:
            raise NotFound('The artist is not found.')
        if not Assignment.is_role(
            current_user_profile,
            ['admin', 'customer_service', 'financial_officer', 'seller']
        ):
            raise Forbidden('Not allowed.')
        parsed_upload = Uploader.parse_uploaded(data.img)
        if parsed_upload:
            errors = Uploader.validate_img(
                parsed_upload['filesize'], parsed_upload['filetype']
            )
            if errors:
                raise BadRequest(errors)
        else:
            raise BadRequest(['Please upload an image.'])
        new_attorney = Storage(
            img=parsed_upload['filename'],
            img_meta=dict(
                size=parsed_upload['filesize'],
                filetype=parsed_upload['filetype']
            )
        )
        attorney = Attorney.create(artist, new_attorney)
        Uploader.remove('attorneys', attorney)
        Uploader.insert(
            'attorneys', attorney, Attorney.versions, parsed_upload
        )
        conf.web.ctx['status'] = '201 Created'
        return AttorneyPresenter.created(attorney).as_json()
