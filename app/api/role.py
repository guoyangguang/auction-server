# -*-coding: utf-8-*-
'''
role api module
'''

from web import Storage
from .base import Base
from ..model.role import Role
from ..model.profile import Profile
from ..model.assignment import Assignment
from ..presenter.role import RolePresenter
from ..box.exception import NotFound, BadRequest, Forbidden
from ..config import conf


class RolesAPI(Base):
    '''
    roles api class
    '''
    def POST(self):
        '''
        As an admin, I want to add a role, so that
        there is a new role definition in the system.
        '''
        data = self.data('name')
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        if not Assignment.is_role(current_user_profile, ['admin']):
            raise Forbidden('Not allowed.')
        role = Storage(name=data.name)
        errors = {}
        if not Role.validator.validate(role, results=errors):
            raise BadRequest(errors)
        role = Role.create(role)
        conf.web.ctx['status'] = '201 Created'
        return RolePresenter.role(role).as_json()

    def GET(self):
        '''
        As an team member, I want to look over roles, so that
        i could know all the roles in the system.
        '''
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        if not Assignment.is_role(
            current_user_profile,
            ['admin', 'customer_service', 'financial_officer', 'seller']
        ):
            raise Forbidden('Not allowed.')
        roles = Role.all()
        return RolePresenter.roles(roles).as_json()
