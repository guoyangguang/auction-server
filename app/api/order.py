# -*- coding: utf-8 -*-
'''
order api module
'''

from datetime import datetime
from web import Storage
from .base import Base
from ..model.bid import Bid
from ..model.item import Item
from ..model.order import Order
from ..model.profile import Profile
from ..box import ali_pay
from ..box.exception import NotFound, BadRequest, Forbidden
from ..config import conf
from ..presenter.order import OrderPresenter


class ItemOrderAPI(Base):
    '''
    order api class
    '''
    def POST(self, item_id):
        self.csrf_protected()
        current_user = self.signin_required()
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        item = Item.get(item_id)
        if not item:
            raise NotFound('The item is not found.')
        now = datetime.now()
        if now < item.starting_bid: 
            raise Forbidden('仍未开拍, 欢迎您届时前来竞拍.')
        if now < item.closing_bid: 
            raise Forbidden('拍卖未结束.')
        # TODO deal with the same price
        max_bid = Bid.filter_max_bid_by_item(item)
        if not max_bid:
            raise NotFound('No bids for the item.')
        if max_bid.profile_id != profile.id:
            raise Forbidden('The item should be paid by the highest bidder.')
        new_order = Storage(amount=max_bid.price)
        errors = {}
        if not Order.validator.validate(new_order, results=errors):
            raise BadRequest(errors)
        order = Order.create(profile, item, new_order)
        conf.web.ctx['status'] = '201 Created'
        ready_to_pay_url = ali_pay.ready_to_pay(order)
        return OrderPresenter.created(order, ready_to_pay_url).as_json()
