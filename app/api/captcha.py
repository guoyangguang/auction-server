#  -*- coding: utf-8 -*-

'''
captcha apis
'''

import json
from .base import Base
import base64

from ..box import pycaptcha 
from ..config import conf


class CaptchaAPI(Base): 
    '''
    as a general user, i want to get the captcha img path,
    so that i can input words according to the img. 
    '''
    def GET(self):
        # from pdb import set_trace;set_trace()
        if not conf.web.config.debug:
            # TODO self.csrf_protected()
            stringio = pycaptcha.set_captcha(conf.web.ctx.session)
            img = stringio.getvalue()
            stringio.close()
            return json.dumps({'img': base64.b64encode(img)})
