# -*- coding: utf-8 -*-

'''
artists apis module
'''
from web import Storage
from .base import Base
from ..model.artist import Artist
from ..model.assignment import Assignment
from ..model.profile import Profile
from ..box.exception import NotFound, BadRequest, Forbidden
from ..box.uploader import Uploader
from ..presenter.artist import ArtistPresenter
from ..config import conf


class ArtistsAPI(Base):
    '''
    artists API
    '''
    def GET(self): 
        '''
        As a team role, i want to look over all artists, so that
        i could create, edit etc...
        '''
        data = self.input('page')
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        if not Assignment.is_role(
            current_user_profile,
            ['admin', 'customer_service', 'financial_officer', 'seller']
        ):
            raise Forbidden('Not allowed.')
        artists = Artist.all(int(data.page))
        return ArtistPresenter.all(artists).as_json()

    def POST(self):
        '''
        As a team role, i want to create an artist,
        so that the general users could follow him or her, etc....
        '''
        data = self.input(
            'name', 'intro', 'img', img={}
        )
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        if not Assignment.is_role(
            current_user_profile,
            ['admin', 'customer_service', 'financial_officer', 'seller']
        ):
            raise Forbidden('Not allowed.')
        parsed_upload = Uploader.parse_uploaded(data.img)
        if parsed_upload:
            img_errors = Uploader.validate_img(
                parsed_upload['filesize'], parsed_upload['filetype']
            )
            if img_errors:
                raise BadRequest(img_errors)
            new_artist = Storage(
                name=data.name,
                intro=data.intro,
                img=parsed_upload['filename'],
                img_meta=dict(
                    size=parsed_upload['filesize'],
                    filetype=parsed_upload['filetype'],
                    ratio=parsed_upload['ratio'],
                    vdimensions=Uploader.vdimensions(
                        Artist.versions, parsed_upload['ratio']
                    )
                )
            )
            errors = {}
            if not Artist.validator.validate(new_artist, results=errors):
                raise BadRequest(errors)
        else:
            raise BadRequest(['请上传图片.'])
        artist = Artist.create(new_artist)
        Uploader.remove('artists', artist)
        Uploader.insert('artists', artist, Artist.versions, parsed_upload)
        conf.web.ctx['status'] = '201 Created'
        return ArtistPresenter.artist(artist).as_json()


class ArtistAPI(Base):
    '''
    artist API
    '''
    def PUT(self, artist_id):
        '''
        As a team role, i want to update the artist's info, so that
        the general users could know him or her better.
        '''
        data = self.input(
            'name', 'intro', 'img', img={}
        )
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        artist = Artist.get(artist_id)
        if not artist:
            raise NotFound('The artist is not found.')
        if not Assignment.is_role(
            current_user_profile,
            ['admin', 'customer_service', 'financial_officer', 'seller']
        ):
            raise Forbidden('Not allowed.')
        parsed_upload = Uploader.parse_uploaded(data.img)
        if parsed_upload:
            img_errors = Uploader.validate_img(
                parsed_upload['filesize'], parsed_upload['filetype']
            )
            if img_errors:
                raise BadRequest(img_errors)
            new_artist = Storage(
                name=data.name,
                intro=data.intro,
                img=parsed_upload['filename'],
                img_meta=dict(
                    size=parsed_upload['filesize'],
                    filetype=parsed_upload['filetype'],
                    ratio=parsed_upload['ratio'],
                    vdimensions=Uploader.vdimensions(
                        Artist.versions, parsed_upload['ratio']
                    )
                )
            )
            errors = {}
            if not Artist.validator.validate(new_artist, results=errors):
                raise BadRequest(errors)
        else:
            raise BadRequest(['请上传图片.'])
        updated = Artist.update(artist, new_artist)
        Uploader.remove('artists', updated)
        Uploader.insert('artists', updated, Artist.versions, parsed_upload)
        return ArtistPresenter.artist(updated).as_json()
