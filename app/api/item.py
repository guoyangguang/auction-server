#  -*- coding: utf-8 -*-

'''
item apis module
'''

from web import Storage
from .base import Base
from ..model.assignment import Assignment
from ..model.profile import Profile
from ..model.artist import Artist
from ..model.followship import Followship
from ..model.item import Item
from ..model.item_img import ItemImg
from ..model.like import Like
from ..model.comment import Comment
from ..presenter.item import ItemPresenter
from ..box.exception import Forbidden, NotFound, BadRequest, format_errors
from ..box.uploader import Uploader
from ..box import utils
from ..box import esearch
from ..config import conf


es = esearch.Esearch(index='items')

class ArtistItemsAPI(Base):
    '''
    ArtistItemsAPI class
    '''
    def GET(self, artist_id):
        '''
        As an team role, i want to glance over the artist's items,
        so that i could know what items i has on the platform. 
        '''
        data = self.input('page')
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        artist = Artist.get(artist_id)
        if not artist:
            raise NotFound('找不到艺术家.')
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        if not Assignment.is_role(
            current_user_profile,
            ['admin', 'customer_service', 'financial_officer', 'seller']
        ):
            raise Forbidden('Not Allowed.')
        items = Item.filter_by_artist(artist, int(data.page))
        return ItemPresenter.items(items).as_json()

    def POST(self, artist_id):
        '''
        As a team role, i want to create an item,
        so that the general users can buy the item.
        '''
        data = self.input(
            'kind', 'name', 'description', 'length', 'width',
            'usage', 'produced_at', 'starting_price', 'raising_price',
            'deposit', 'starting_bid', 'closing_bid', 'img', img={}
        )
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        if not Assignment.is_role(
            current_user_profile,
            ['admin', 'customer_service', 'financial_officer', 'seller']
        ):
            raise Forbidden('Not allowed.')
        artist = Artist.get(artist_id)
        if not artist:
            raise NotFound('找不到艺术家.')
        parsed_upload = Uploader.parse_uploaded(data.img)
        if parsed_upload:
            img_errors = Uploader.validate_img(
                parsed_upload['filesize'], parsed_upload['filetype']
            )
            if img_errors:
                raise BadRequest(img_errors)
            new_item = Storage(
                kind=data.kind,
                name=data.name,
                description=data.description,
                length=data.length,
                width=data.width,
                usage=data.usage,
                produced_at=data.produced_at,
                starting_price=data.starting_price,
                raising_price=data.raising_price,
                deposit=data.deposit,
                starting_bid=data.starting_bid,
                closing_bid=data.closing_bid,
                img=parsed_upload['filename'],
                img_meta=dict(
                    size=parsed_upload['filesize'],
                    filetype=parsed_upload['filetype'],
                    ratio=parsed_upload['ratio'],
                    vdimensions=Uploader.vdimensions(
                        Item.versions, parsed_upload['ratio']
                    )
                )
            )
            errors = {}
            if not Item.validator.validate(new_item, results=errors):
                raise BadRequest(errors)
        else:
            raise BadRequest(['请上传图片.'])
        item = Item.create(artist, new_item)
        if not conf.web.config.debug:
            es.insert(dict(item))
        Uploader.remove('items', item)
        Uploader.insert('items', item, Item.versions, parsed_upload)
        conf.web.ctx['status'] = '201 Created'
        return ItemPresenter.created(item).as_json()


class ArtistItemShowAPI(Base):
    '''
    ArtistItemShowAPI
    '''
    def GET(self, artist_id, item_id):
        '''
        As a team role, i want to look one of the artist's items,
        so that i could know it, and upload itemimgs. 
        '''
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        artist = Artist.get(artist_id)
        if not artist:
            raise NotFound('找不到艺术家.')
        item = Item.get(item_id)
        if not item:
            raise NotFound('找不到作品.')
        if not Assignment.is_role(
            current_user_profile,
            ['admin', 'customer_service', 'financial_officer', 'seller']
        ):
            raise Forbidden('Not Allowed.')
        if item.artist_id != artist.id:
            raise BadRequest('作品不属于该艺术家.')
        itemimgs = ItemImg.filter_by_item(item)
        return ItemPresenter.item(item, itemimgs).as_json()


class ItemsAPI(Base):
    '''
    ItemsAPI class
    '''
    def GET(self):
        '''
        As a general user, i want to glance over all the items,
        so that i can discover my favorites.
        '''
        data = self.input('page')
        self.csrf_protected()
#        current_user = self.signin_required()
#        current_user_profile = Profile.filter_by_user(current_user)
#        if not current_user_profile:
#            raise NotFound('用户不存在.')
        items = Item.all(int(data.page))
        return ItemPresenter.items(items).as_json()


class FollowingItemsAPI(Base):
    '''
    FollowingItemsAPI class
    '''
    def GET(self, following_id):
        '''
        As an general user, i want to glance over one _of my following's
        items, so that i could see what items he has on the platform. 
        '''
        data = self.input('page')
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        artist = Artist.get(following_id)
        if not artist:
            raise NotFound('找不到艺术家.')
        if not Followship.is_following(current_user_profile, artist):
            raise BadRequest('未关注.')
        items = Item.filter_by_artist(artist, int(data.page))
        return ItemPresenter.items(items).as_json()


class ItemsSearchAPI(Base):

    def GET(self):
        '''
        As a general user, i want to search items by query,
        so that i could get the items i want quickly.
        '''
        data = self.input('query', 'page')
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        query = data.query.strip()
        if not query:
            raise BadRequest('请输入作品主题, 搜索.')
        if not conf.web.config.debug:
            val = es.search_about(
                utils.items_query(
                    query,
                    (int(data.page)-1)*Item.per_page,
                    Item.per_page
                )
            )
            items = [Storage(doc) for doc in val['docs']]
        else:
            items = []
        # TODO DO we need to get data from postgresql?
        # items = Item.filter_by_ids(val['ids'])
        return ItemPresenter.items(items).as_json()


class ItemShowAPI(Base):
    '''
    ItemShowAPI class
    '''
    def GET(self, item_id):
        '''
        As a general user, i want to look one of the items,
        so that i can know more about the item and bid it.
        '''
        self.csrf_protected()
        current_user = self.signin_required()
        item = Item.get(item_id)
        if not item:
            raise NotFound('The item is not found.')
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('The profile is not found.')
        artist = Artist.get(item.artist_id)
        is_followed = bool(Followship.get(current_profile.id, artist.id))
        is_liked = bool(Like.get(current_profile, item))
        like_count = Like.item_liked_count(item)
        comments = Comment.filter_by_item(item, 1)
        itemimgs = ItemImg.filter_by_item(item)
        return ItemPresenter.show(
            item, artist, is_followed, is_liked, like_count,
            comments, itemimgs
        ).as_json()
