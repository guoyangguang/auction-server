# -*- coding: utf-8 -*-

import web
import json
from ..box.pycaptcha import check_captcha
from ..box.utils import prevent_csrf
from ..box.exception import Unauthorized, MissedData
from ..config import conf
from ..model.user import User


class Base(object):

    def signin_required(self):
        '''
        if debug==True or in test, the user pretends to signin
        with the one created in the test.
        '''
        if conf.web.config.debug:
            user = User.filter_by_email('user@auction.com')
            if not user:
                raise Unauthorized('请登录.')
            return user
        if not conf.web.ctx.session.has_key('user_id'):
            raise Unauthorized('请登录.')
        user_id = conf.web.ctx.session.user_id
        if not user_id:
            raise Unauthorized('请登录.')
        user = User.get(user_id)
        if not user:
            raise Unauthorized('请登录.')
        return user

    def is_logged_in(self):
        if not conf.web.config.debug:
            if not conf.web.ctx.session.has_key('user_id'):
                return False
            user_id = conf.web.ctx.session.user_id
            if not user_id:
                return False
            return True

    def input(self, *requireds, **defaults):
        try:
            request_data = web.input(*requireds, **defaults)
            return request_data 
        except web.webapi.BadRequest:
            raise MissedData(', '.join(requireds) + ' is required.')

    def data(self, *requireds, **defaults):
        try:
            data = web.storage(json.loads(web.data()))
        except ValueError:
            raise MissedData(', '.join(requireds) + ' is required.')
        for required in requireds:
            if required not in data:
                try:
                    data[required] = defaults[required]
                except KeyError:
                    raise MissedData(required + ' is required.')
        return data

    def csrf_protected(self, checkcaptcha=False, captcha_data=None):
        '''
        check csrf and robot with check_captcha, prevent_csrf
        '''
        if not conf.web.config.debug:
            prevent_csrf(conf.web.ctx.session)
            if checkcaptcha:
                check_captcha(captcha_data, conf.web.ctx.session)
