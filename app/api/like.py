# -*- coding: utf-8 -*-

'''
like api
'''

from .base import Base
from ..box.exception import NotFound, BadRequest, Forbidden
from ..model.profile import Profile
from ..model.item import Item
from ..model.like import Like
from ..presenter.like import LikePresenter
from ..config import conf


class LikeMineAPI(Base):
    '''
    LikeMineAPI
    '''
    def POST(self):
        '''
        As an general user, i want to like an item,
        so that i can express my like
        '''
        data = self.data('id')
        self.csrf_protected()
        current_user = self.signin_required()
        item = Item.get(data.id)
        if not item:
            raise NotFound('The item is not found.')
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        if Like.get(profile, item):
            raise BadRequest('You have already liked the item.')
        like = Like.like(profile, item)
        item_liked_count = Like.item_liked_count(item)
        conf.web.ctx.status = '201 Created'
        return LikePresenter.created(like, item_liked_count).as_json()

    def PUT(self, item_id):
        '''
        As an general user, i want to cancel like an item,
        so that i can express my like
        '''
        self.csrf_protected()
        current_user = self.signin_required()
        item = Item.get(item_id)
        if not item:
            raise NotFound('The item is not found.')
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        like = Like.get(profile, item)
        if not like:
            raise NotFound('You did not ever liked the item.')
        like = Like.cancel_like(like)
        item_liked_count = Like.item_liked_count(item)
        return LikePresenter.cancel_like(like, item_liked_count).as_json()
