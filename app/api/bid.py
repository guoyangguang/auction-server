# -*-coding: utf-8-*-

'''
bid api module
1、拍卖必须有两个以上的买主：即凡拍卖表现为只有一个卖主（通常由拍卖机构充任）而有许多可能的买主，从而得以具备使后者相互之间能就其欲购的拍卖物品展开价格竞争的条件。
2、拍卖必须有不断变动的价格：即凡拍卖皆非卖主对拍卖物品固定标价待售或买卖双方就拍卖物品讨价还价成交，而是由买主以卖主当场公布的起始价为基准另行应报价，直至最后确定最高价金为止。
3、拍卖必须有公开竞争的行为：即凡拍卖都是不同的买主在公开场合针对同一拍卖物品竞相出价，争购以图，而倘若所有买主对任何拍卖物品均无意思表示，没有任何竞争行为发生，拍卖就将失去任何意义。
https://baike.baidu.com/item/%E6%8B%8D%E5%8D%96/81152
'''

from web import Storage
from datetime import datetime
from .base import Base
from ..box.exception import NotFound, BadRequest, Forbidden
from ..model.profile import Profile
from ..model.bid import Bid
from ..model.item import Item 
from ..presenter.bid import BidPresenter


class ItemBidsAPI(Base):
    '''
    ItemBids class
    '''
    def GET(self, item_id):
        '''
        As a general user, i want to see all the bids of the item,
        so that i could understand and decide my bid.
        '''
        data = self.input('page')
        self.csrf_protected()
        current_user = self.signin_required()
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        item = Item.get(item_id)
        if not item:
            raise NotFound('The item is not found.')
        bids = Bid.filter_by_item(item, int(data.page))
        return BidPresenter.item_bids(bids).as_json()

    def POST(self, item_id):
        '''
        As a general user, i want to bid the item,
        so that i could get it.
        '''
        data = self.data('price')
        self.csrf_protected()
        current_user = self.signin_required()
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        item = Item.get(item_id)
        if not item:
            raise NotFound('The item is not found.')
        new_bid = Storage(price=data.price, status=1)
        errors = {}
        if not Bid.validator.validate(new_bid, results=errors):
            raise BadRequest(errors)
        new_bid.price = int(new_bid.price) * 100
        # TODO deposit forbbidene; manual test the following forbiddens.
        now = datetime.now()
        if now < item.starting_bid: 
            raise Forbidden('仍未开拍, 欢迎您届时前来竞拍.')
        if now > item.closing_bid: 
            raise Forbidden('拍卖已结束.')
        if new_bid.price < item.starting_price:
            raise Forbidden('出价应等于或高于起价.')
        val = new_bid.price - item.starting_price
        if (val > 0 and val < item.raising_price) or val % item.raising_price:
            raise Forbidden('请遵守加价规则.')
        max_bid_of_item = Bid.filter_max_bid_by_item(item)
        if max_bid_of_item:
            if max_bid_of_item.price < new_bid.price:
                new_bid.status = 2 
                Bid.update_status(max_bid_of_item, 1)
            else:
                new_bid.status = 1 
        else:
            new_bid.status = 2
        bid = Bid.create(item, profile, new_bid)
        return BidPresenter.created(profile, bid).as_json()
