# -*- coding: utf-8 -*-

'''
access token
'''

from web import Storage
import re
from .base import Base
from ..model.access_token import AccessToken
from ..model.profile import Profile
from ..presenter.access_token import AccessTokenPresenter
from ..box.exception import NotFound, BadRequest
from ..box import oauth, utils
from ..config import conf


class AccessTokenMineAPI(Base):
    '''
    access token
    '''
    def POST(self):
        '''
        As a general user, i want to authorize my sites,
        so that i can share something there.
        '''
        data = self.data('code', 'site')
        self.csrf_protected()
        if int(data.site) == 1:
            conf.DOUBAN_TOKEN_DATA['authorization_code'] = data.code
            val = oauth.request_token(
                conf.TOKEN_URL_DOUBAN, conf.DOUBAN_TOKEN_DATA
            )
        elif int(data.site) == 2:
            conf.SINA_TOKEN_DATA['authorization_code'] = data.code
            val = oauth.request_token(
                conf.TOKEN_URL_SINA, conf.SINA_TOKEN_DATA
            )
        if not val['success']:
            raise BadRequest('授权失败.')
        current_user = self.signin_required()
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        access_token = AccessToken.filter_by_profile_and_site(
            profile, data.site
        )
        if access_token:
            access_token.access_token = val['data']['access_token']
            access_token.expires_in = val['data']['expires_in']
            access_token.user_id = utils.oauth_user_id(
                int(data.site), val['data']
            )
            access_token = AccessToken.update_access_token(access_token)
        else:
            access_token = Storage(
                site=data.site,
                access_token=val['data']['access_token'],
                expires_in=val['data']['expires_in'],
                user_id=utils.oauth_user_id(int(data.site), val['data'])
            )
            errors = dict()
            if not AccessToken.validator.validate(
                access_token, results=errors
            ):
                raise BadRequest(errors)
            access_token = AccessToken.create(profile, access_token)
            conf.web.ctx.status = '201 Created'
        return AccessTokenPresenter.access_token(access_token).as_json()


class AccessTokenMinePubStatusAPI(Base):
    '''
    As a general user, i want to share something from my sites,
    so that more people know it.
    '''
    def GET(self):
        data = self.input('share', sites=[])
        self.csrf_protected()
        pattern = re.compile(r"http:\/\/.+\/items\/[1-9][0-9]*")
        if not pattern.search(data.share):
            raise BadRequest('分享的文字不正确.')
        current_user = self.signin_required()
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('用户不存在.')
        msgs = list()
        for site in data.sites:
            access_token = AccessToken.filter_by_profile_and_site(
                profile, site
            )
            if not access_token:
                msgs.append('{0}未授权'.format(
                    utils.oauth_sites[int(site)])
                )
                # FIXME next or continue?
                continue
            val = oauth.pub_status(
                int(site), access_token.access_token, data.share
            )
            if not val['success']:
                msgs.append('分享到{0}失败'.format(
                    utils.oauth_sites[int(site)])
                )
        if msgs:
            raise BadRequest('; '.join(msgs))
        return AccessTokenPresenter.filter_by_profile_and_site(
            access_token
        ).as_json()
