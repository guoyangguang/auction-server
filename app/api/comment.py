# -*- coding: utf-8 -*-

'''
comment API module
'''

from web import Storage
from .base import Base
from ..model.profile import Profile
from ..model.item import Item
from ..model.comment import Comment
from ..presenter.comment import CommentPresenter
from ..box.exception import NotFound, Forbidden, BadRequest
from ..config import conf


class ItemCommentsAPI(Base):
    '''
    ItemCommentsAPI
    '''
    def POST(self, item_id):
        '''
        As a general user, i want to create a comment for an item,
        so that i can express myself.
        '''
        data = self.data(
            'body', 'comment_id', comment_id=None
        )
        self.csrf_protected()
        current_user = self.signin_required()
        item = Item.get(item_id)
        if not item:
            raise NotFound('The item is not found.')
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('The profile is not found.')
        errors = dict()
        if data.comment_id:
            errors.clear()
            new_reply = Storage(body=data.body, comment_id=data.comment_id)
            if not Comment.reply_validator.validate(
                new_reply, results=errors
            ):
                raise BadRequest(errors)
            comment = Comment.get(int(data.comment_id))
            if not comment:
                raise NotFound('The comment is not found.')
            comment_profile = Profile.get(comment.profile_id)
            created = Comment.create(
                current_profile, item, new_reply, comment
            )
            conf.web.ctx.status = '201 Created'
            return CommentPresenter.reply(
                current_profile, created, comment_profile, comment
            ).as_json()
        else:
            new_comment = Storage(body=data.body)
            errors.clear()
            if not Comment.comment_validator.validate(
                new_comment, results=errors
            ):
                raise BadRequest(errors)
            created = Comment.create(current_profile, item, new_comment)
            conf.web.ctx.status = '201 Created'
            return CommentPresenter.comment(
                current_profile, created
            ).as_json()
