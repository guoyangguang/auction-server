#  -*- coding: utf-8 -*-

from .base import Base
from ..model.followship import Followship
from ..model.profile import Profile
from ..model.artist import Artist
from ..model.item import Item
from ..model.assignment import Assignment
from ..presenter.artist import ArtistPresenter
from ..presenter.profile import ProfilePresenter
from ..presenter.followship import FollowshipPresenter
from ..box.exception import NotFound, Forbidden, BadRequest
from ..config import conf


class MyFollowingsAPI(Base):

    def GET(self):
        self.csrf_protected()
        current_user = self.signin_required()
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('The profile is not found.')
        artists = Followship.followings(current_profile)
        artists = sorted(
            artists, key=lambda artist: artist.created_at
        )
        return ArtistPresenter.followings(artists).as_json()

    def POST(self):
        data = self.data('artist_id')
        self.csrf_protected()
        current_user = self.signin_required()
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('The current profile is not found.')
        artist = Artist.get(data.artist_id)
        if not artist:
            raise NotFound('The artist is not found.')
        if Followship.get(current_profile.id, artist.id):
            raise BadRequest('The artist is already followed.')
        followship = Followship.follow(current_profile, artist)
        conf.web.ctx.status = '201 Created'
        return FollowshipPresenter.followship(followship).as_json()

    def PUT(self, artist_id):
        self.csrf_protected()
        current_user = self.signin_required()
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('The current profile is not found.')
        artist = Artist.get(artist_id)
        if not artist:
            raise NotFound('The artist is not found.')
        followship = Followship.get(current_profile.id, artist.id)
        if not followship:
            raise NotFound('The artist is not followed.')
        followship = Followship.unfollow(followship)
        return FollowshipPresenter.followship(followship).as_json()


class ArtistFollowedsAPI(Base):

    def GET(self, artist_id):
        data = self.input('page')
        self.csrf_protected()
        current_user = self.signin_required()
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('The profile is not found.')
        artist = Artist.get(artist_id)
        if not artist:
            raise NotFound('The artist is not found.')
        if not Assignment.is_role(
            current_profile,
            ['admin', 'customer_service', 'financial_officer', 'seller']
        ):
            raise Forbidden('Not Allowed.')
        profiles = Followship.followeds(artist, int(data.page))
        return ProfilePresenter.profiles(profiles).as_json()
