# -*- coding: utf-8 -*-

'''
ItemImg APIs module
'''

from web import Storage
from .base import Base
from ..box.exception import Forbidden, NotFound, BadRequest
from ..model.profile import Profile
from ..model.assignment import Assignment
from ..model.item import Item
from ..model.item_img import ItemImg
from ..presenter.item_img import ItemImgPresenter
from ..box.uploader import Uploader
from ..config import conf


class ArtistItemItemImgsAPI(Base):
    '''
    ArtistItemItemImgsAPI class
    '''
    def POST(self, item_id):
        '''
        As a team role, i want to create an item img
        for the item, so that there will be one more item img.
        '''
        data = self.input('description', 'img', img={})
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        item = Item.get(item_id)
        if not item:
            raise NotFound('The item is not found.')
        if not Assignment.is_role(
            current_user_profile,
            ['admin', 'customer_service', 'financial_officer', 'seller']
        ):
            raise Forbidden('Not allowed.')
        parsed_upload = Uploader.parse_uploaded(data.img)
        if parsed_upload:
            errors = Uploader.validate_img(
                parsed_upload['filesize'], parsed_upload['filetype']
            )
            if errors:
                raise BadRequest(errors)
            errors = {}
            new_itemimg = Storage(
                description=data.description,
                img=parsed_upload['filename'],
                img_meta=dict(
                    size=parsed_upload['filesize'],
                    filetype=parsed_upload['filetype'],
                    ratio=parsed_upload['ratio'],
                    vdimensions=Uploader.vdimensions(
                        ItemImg.versions, parsed_upload['ratio']
                    )
                )
            )
            if not ItemImg.validator.validate(new_itemimg, results=errors):
                raise BadRequest(errors)
        else:
            raise BadRequest(['Please upload an image.'])
        itemimg = ItemImg.create(item, new_itemimg)
        Uploader.remove('item_imgs', itemimg)
        Uploader.insert(
            'item_imgs', itemimg, ItemImg.versions, parsed_upload
        )
        conf.web.ctx['status'] = '201 Created'
        return ItemImgPresenter.created(itemimg).as_json()
