# -*- coding: utf-8 -*-

from web import Storage
from .base import Base
from ..model.user import User
from ..model.assignment import Assignment
from ..model.verification import Verification
from ..model.profile import Profile
from ..model.account import Account
from ..presenter.user import UserPresenter
from ..box.exception import Unauthorized, NotFound, Forbidden, BadRequest
from ..box import utils, pycaptcha
from ..config import conf


class AuthSignupAPI(Base):
    '''
    as a not registered user, i want to signup, so that i
    can do something for myself.
    '''
    def POST(self):
        data = self.data(
            'captcha_data', 'email', 'phone', 'verification_code',
            'password', 'password_confirmation', 'accept', accept='0'
        )
        self.csrf_protected(True, data.captcha_data)
        if self.is_logged_in():
            raise Forbidden('请点击退出后，再注册.')
        if int(data.accept) == 0:
            raise BadRequest(u'请阅读并接受协议条款.')
        errors = dict()
        phone_code = Storage(
            phone=data.phone, verification_code=data.verification_code
        )
        if not User.phone_code_validator.validate(phone_code, results=errors):
            raise BadRequest(errors)
        verifications = Verification.filter_by_phone(data.phone)
        if not verifications:
            raise NotFound('请点击获取验证码.')
        if not verifications[0].verification_code == int(data.verification_code):
            raise BadRequest('无法确认手机号码, 请输入正确的验证码.')
        user = Storage(
            email=data.email,
            phone=data.phone,
            password_digest=data.password,
            password_confirmation=data.password_confirmation
        )
        errors.clear()
        if not User.validator.validate(user, results=errors):
            raise BadRequest(message=errors)
        trans = conf.db.transaction()
        try:
            user = User.signup(user)
            profile = Profile.create_default_profile(user)
            account = Storage(balance=0)
            Account.create(profile, account)
        # TODO too general exception
        except Exception:
            trans.rollback()
        # TODO return 500?
            raise
        else:
            trans.commit()
            conf.web.ctx['status'] = '201 Created'
            return UserPresenter.user(user).as_json()


class AuthSigninAPI(Base):
    '''
    as a confirmed user,
    i want to signin with my account, so that i can generate my data.
    '''
    def POST(self):
        data = self.data('captcha_data', 'phone', 'password')
        self.csrf_protected(True, data.captcha_data)
        if self.is_logged_in():
            raise Forbidden('请点击退出后，再登录.')
        user = User.filter_by_phone(data.phone)
        # from pdb import set_trace;set_trace()
        if user and User.check_password(user, data.password):
            user = User.signin(user, conf.web.ctx.get('ip'))
            profile = Profile.filter_by_user(user)
            if not profile:
                raise NotFound('The profile is not found.')
            if not conf.web.config.debug:
                conf.web.ctx.session.user_id = user.id
            return UserPresenter.signin(user, profile).as_json()
        else:
            raise NotFound('用户不存在.')


class AuthSignoutAPI(Base):
    '''
    as a signin user,
    i want to signout, so that i must signin next time to use my account
    '''
    def POST(self):
        self.csrf_protected()
        current_user = self.signin_required()
        if not conf.web.config.debug:
            conf.web.ctx.session.pop('user_id', None)
        return UserPresenter.user(current_user).as_json()


class AuthResetPasswordAPI(Base):
    '''
    as a signup user,
    i want to input my new password, so that i can reset my password
    '''
    def POST(self):
        # TODO if user1 has signup our app, and currently user2
        # uses user1's phone number, user2 must not reset user1's password.
        data = self.data(
            'captcha_data', 'verification_code',
            'phone', 'password', 'password_confirmation'
        )
        self.csrf_protected(True, data.captcha_data)
        if self.is_logged_in():
            raise Forbidden('请点击退出后，再设置密码.')
        errors = dict()
        phone_code = Storage(
            phone=data.phone, verification_code=data.verification_code
        )
        if not User.phone_code_validator.validate(phone_code, results=errors):
            raise BadRequest(errors)
        user = User.filter_by_phone(data.phone)
        if not user:
            raise NotFound('用户不存在.')
        verifications = Verification.filter_by_phone(data.phone)
        if not verifications:
            raise NotFound('请点击获取验证码.')
        if not verifications[0].verification_code == int(data.verification_code):
            raise BadRequest('无法确认手机号码, 请输入正确的验证码.')
        user.password_digest = data.password
        user.password_confirmation = data.password_confirmation
        errors.clear()
        if not User.password_validator.validate(user, results=errors):
            raise BadRequest(errors)
        user = User.reset_password(user)
        return UserPresenter.user(user).as_json()


class UsersAPI(Base):

    def GET(self):
        '''
        As a customer service, i want to look up all the users,
        so that i can assign roles, etc
        '''
        data = self.input('page')
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound('The profile is not found.')
        if not Assignment.is_role(
            current_user_profile,
            ['admin', 'financial_officer, customer_service']
        ):
            raise Forbidden('Not allowed.')
        users = User.all(int(data.page))
        return UserPresenter.users(users).as_json()
