# -*- coding: utf-8 -*-

from web import Storage
from .base import Base
from ..model.verification import Verification
from ..presenter.verification import VerificationPresenter
from ..box.exception import NotFound, BadRequest
from ..config import conf
from ..box import utils


class VerificationAPI(Base):
    '''
    as a general user, i want to fetch the verification code, 
    so that i can verify my phone.
    '''
    def POST(self):
        data = self.data('phone')
        self.csrf_protected()
        errors = dict()
        if not Verification.validator.validate(
            Storage(phone=data.phone), results=errors
        ):
            raise BadRequest(errors)
        code = utils.gen_verification_code(xrange(100000, 999999), 1)
        verifications = Verification.filter_by_phone(data.phone)
        if len(verifications):
            verifications[0].verification_code = code
            verif = Verification.update_code(verifications[0])
        else:
            verification = Storage(
                phone=data.phone,
                verification_code=code
            )
            errors.clear()
            if Verification.validator.validate(verification, results=errors):
                verif = Verification.create(verification)
            else:
                raise BadRequest(errors)
        print('verification code is ', code)
        # send_sms.delay(data.phone, code)
        return VerificationPresenter.verification(verif).as_json()
