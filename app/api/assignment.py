# -*-coding: utf-8-*-
'''
assignment api module
'''

from .base import Base
from ..model.assignment import Assignment
from ..model.artist import Artist
from ..model.profile import Profile
from ..model.user import User 
from ..model.role import Role
from ..presenter.assignment import AssignmentPresenter
from ..box.exception import NotFound, BadRequest, Forbidden
from ..config import conf


class AssignmentAPI(Base):
    '''
    assignments api class
    '''
    def POST(self):
        '''
        As an admin, I want to add a assignment, so that
        the user has a specific role.
        '''
        data = self.data('user_id', 'role_name')
        self.csrf_protected()
        current_user = self.signin_required()
        current_user_profile = Profile.filter_by_user(current_user)
        if not current_user_profile:
            raise NotFound("The current user's profile is not found.")
        if not Assignment.is_role(current_user_profile, ['admin']):
            raise Forbidden('Not allowed.')
        user = User.get(data.user_id)
        if not user:
            raise NotFound("The user is not found.")
        profile = Profile.filter_by_user(user)
        if not profile:
            raise NotFound("The profile is not found.")
        if Assignment.is_role(profile, [data.role_name]):
            raise BadRequest(
                'The user has been already been assigned the role.'
            )
        role = Role.filter_by_name(data.role_name)
        if not role:
            raise NotFound("The role is not found.")
        assignment = Assignment.has_role(profile, role)
        conf.web.ctx['status'] = '201 Created'
        return AssignmentPresenter.assignment(assignment).as_json()
